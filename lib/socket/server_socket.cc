#include <arpa/inet.h>
#include <netdb.h>
#include "server_socket.h"

// -- definitions --

#define MAX_HOST_NAME	128	// max host name


// -- construct, destruct --

// constructor
server_socket::server_socket(unsigned short _port)
:   port(_port)	// port number
{
    // create socket
    if (!create())
    {
	close();
	return;
    }	
    
    // bind socket with the address
    if (!bind())
    {
	close();
	return;
    }
    
    // start listening socket
    if (!listen())
    {
	close();
	return;
    }    
}

// destructor
server_socket::~server_socket()
{
    close();
}


// -- properties --

/*
 * gets server socket address (ip, port number),
 * overridden from class socket.
 */
bool server_socket::get_addr(std::string& _ip, unsigned short& _port)
{
    if (!is_valid())
	return false;
    
    // get address data
    char name[MAX_HOST_NAME+1];
    if (gethostname(name, MAX_HOST_NAME) == -1)
	return false;
        
    struct hostent* host = gethostbyname(name);
    if (!host)
	return false;
    
    // get ip
    _ip = inet_ntoa(*(in_addr*)host->h_addr_list[0]);
    
    // get port number    
    _port = port;
    
    return true;
}


// -- operations --

/*
 * accepts a client connection.
 */
int server_socket::accept()
{
    if (!is_valid())
	return -1;
    
    // accept a client
    return ::accept(fd, NULL, NULL);    
}

/*
 * indicates whether server socket is valid.
 */
bool server_socket::is_valid()
{
    // call parent class' is_valid()
    return socket::is_valid();
}
     
/*
 * closes socket and clear data.
 */
void server_socket::close()
{
    // let parent class close
    socket::close();
}


// -- methods --

/*
 * binds server socket with the address.
 */
bool server_socket::bind()
{
//  if (!server socket::is_valid())
//	return false;
    
    struct sockaddr_in addr = {0,};
    addr.sin_family	    = AF_INET;
    addr.sin_addr.s_addr    = htonl(INADDR_ANY);
    addr.sin_port	    = htons(port);
    
    if (::bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	return false;
    
    return true;
}

/*
 * starts listening clients.
 */
bool server_socket::listen()
{
//  if (!server socket::is_valid())
//	return false;
    
    if (::listen(fd, SOMAXCONN) == -1)
	return false;
    
    return true;
}
