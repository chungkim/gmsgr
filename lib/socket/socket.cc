#include <sys/socket.h>
#include "socket.h"

// -- construct, destruct --

// constructor
socket::socket()
:   fd(-1)  // file descriptor
{
}

// destructor
socket::~socket()
{    
    close();
}


// -- operations --

/*
 * sets socket file descriptor.
 * returns false, if the file descriptor is already created.
 */
bool socket::set_fd(int _fd)
{
   if (socket::is_valid())
	return false; 

    // set file descriptor
    fd = _fd;
    
    return true;
}

/*
 * indicates whether socket is valid.
 */
bool socket::is_valid()
{
    return (fd != -1);
}

/*
 * creates socket.
 */
bool socket::create() 
{
    if (is_valid())
	return false;
    
    // create socket
    fd = ::socket(PF_INET, SOCK_STREAM, 0);
    if (fd == -1)
	return false;
    
    return true;
}

/*
 * closes socket, set file descriptor zero.
 */
void socket::close()
{
    // close file descriptor
    if (fd != -1)
    {
        ::close(fd);

	// set fd invalid
	fd = -1;
    }
}
