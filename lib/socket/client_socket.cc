#include <arpa/inet.h>
#include <string>
#include "client_socket.h"

// -- construct, destruct --

// constructor
client_socket::client_socket()
:   recv_stream(NULL),	// stream to receive
    send_stream(NULL)	// stream to send
{
}

// destructor
client_socket::~client_socket()
{
    close();
}


// -- properties --

/*
 * set socket file descriptor.
 * returns false, if the file descriptor is already created.
 */
bool client_socket::set_fd(int _fd)
{
    if (!socket::set_fd(_fd))
	return false;

    // open receive, send streams
    if (!open_streams())
    {
	close();
	return false;
    }

    return true;
}
    
/*
 * gets client socket address (ip, port number).
 */
bool client_socket::get_addr(std::string& _ip, unsigned short& _port)
{
    if (!is_valid())
	return false;
    
    struct sockaddr_in addr;
    socklen_t len = sizeof(addr);
   
    // get address from the file descriptor
    if (::getpeername(fd, (struct sockaddr*)&addr, &len) == -1)
	return false;
    
    // convert the endian type
    _ip = inet_ntoa(addr.sin_addr);
    if (_ip.empty())
	return false;
    
    _port = ntohs(addr.sin_port);
    
    return true;
}


// -- operations --

/*
 * creates client socket socket.
 */
bool client_socket::create()
{
    // let parent create socket
    if (!socket::create())
	return false;
    
    // open receive, send streams
    if (!open_streams())
    {
	close();
	return false;
    }
    
    return true;
}

/*
 * connects client socket to the entered address
 */
bool client_socket::connect(std::string _ip, unsigned short _port)
{
    if (!socket::is_valid())
	return false;
    
    struct sockaddr_in addr = {0,};
    addr.sin_family	    = AF_INET;
    addr.sin_addr.s_addr    = inet_addr(_ip.c_str());
    addr.sin_port	    = htons(_port);
   
    // connect to the address
    if (::connect(fd, (sockaddr*)&addr, sizeof(addr)) == -1)
    {
	close();
	return false;
    }
    
    return true;
}

/*
 * indicates whether client socket is valid.
 */
bool client_socket::is_valid()
{
    // is receive stream valid ?
    if (!recv_stream)
	return false;
    
    // is send stream valid ?
    if (!send_stream)
	return false;

    // call parent class' is_valid()
    return socket::is_valid();
}

/*
 * closes socket and clear data.
 */
void client_socket::close()
{
    // disable transmission
    shutdown(SHUT_BOTH);
    
    // close send stream
    if (send_stream)
    {
	::fclose(send_stream);
	send_stream = NULL;
    }
    
    // close receive stream
    if (recv_stream)
    {
	::fclose(recv_stream);
	recv_stream = NULL;
    }
    
    // let parent class close
    socket::close();
}

/*
 * disables receives or sends
 */
bool client_socket::shutdown(SHUT _how)
{
    if (!is_valid())
	return false;
    
    if (::shutdown(fd, _how) == -1)
	return false;
    
    return true;
}

/*
 * receives data, returns the length of data received.
 */
int client_socket::recv(void* _buf, int _len)
{
    if (!is_valid())
	return -1;
    
    return ::recv(fd, _buf, _len, 0);
}

/*
 * sends data, returns the length of data sent.
 */
int client_socket::send(void* _buf, int _len)
{
    if (!is_valid())
	return -1;
    
    return ::send(fd, _buf, _len, 0);
}

// -- methods --

/*
 * opens receive, send streams
 */
bool client_socket::open_streams() 
{
    if (!socket::is_valid() || recv_stream || send_stream)
	return false;
    
    // open receive stream
    recv_stream = ::fdopen(fd, "r");
    if (!recv_stream)
	return false;
    
    // duplicate the file descriptor
    int dup_fd = ::dup(fd);
    if (dup_fd == -1)
	return false;
    
    // open send stream
    send_stream = ::fdopen(dup_fd, "w");
    if (!send_stream)
	return false;
    
    return true;
}
