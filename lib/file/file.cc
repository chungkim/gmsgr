#include <unistd.h>
using namespace std;

#include "file.h"

// -- construct, destruct --

// constructor
file::file(string _path, char* _mode /* = "r+" */)
:   fp(NULL),	// file pointer
    path(_path)	// file path
{
    fp = fopen(path.c_str(), _mode);
}

// destructor
file::~file()
{
    if (fp)
	fclose(fp);
}


// -- operations --

/*
 * reads the file.
 */
int file::read(void* _buf, int _len)
{
    if (!fp)
	return 0;
    
    int len = fread(_buf, 1, _len, fp);
    if (!len)
	return len;
    
    fflush(fp);
    
    return len;
}

/*
 * writes the file.
 */
int file::write(void* _buf, int _len)
{
    if (!fp)
	return 0;
    
    int len = fwrite(_buf, 1, _len, fp);
	return len;
    
    fflush(fp);
    
    return len;
}

/*
 * truncates file.
 */
bool file::truncate(int _len)
{
    if (path.empty())
	return false;
    
    return (!::truncate(path.c_str(), _len));
}

/*
 * sets current position/
 */
bool file::set_position(int _pos, FILE_POS _from)
{
    if (!fp)
	return false;
 
    if (fseek(fp, _pos, _from))
	return false;
    
    return true;
}

/*
 * returns current position
 */
int file::get_position()
{
    if (!fp)
	return -1;
    
    return (ftell(fp));
}

/*
 * returns the file size.
 * returns -1, if it failes.
 */
int file::get_size()
{
    int cur_pos = get_position();
    if (cur_pos == -1)
	return -1;
    
    if (!set_position(0, POS_END))
	return -1;
    
    int size = get_position();    
    set_position(cur_pos, POS_BEGIN);
    
    return size;
}

/*
 * returns the file path.
 */
string file::get_path()
{
    return path;
}

/*
 * returns the file name.
 */
string file::get_name()
{
    int idx = path.find_last_of('/');
    if (idx == string::npos)
	return ("");
    
    return path.substr(idx + 1);
}
