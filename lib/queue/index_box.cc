#include "index_box.h"

// -- construct, destruct --

// constructor
index_box::index_box(unsigned int _max)
:   q(_max)	// circular queue
{
    // put all the indexes for the first time
    for (unsigned int idx = 0; idx < _max; idx++)
    {
	if (!in(idx))
	    return;
    }
}

// destructor
index_box::~index_box()
{
}


// -- operations --

/*
 * puts an index into the box.
 */
bool index_box::in(unsigned int _idx)
{
    return (q.put(_idx));
}

/*
 * returns an index out of the box.
 */
unsigned int index_box::out()
{
    return (q.get());
}

/*
 * returns the max amount of the box
 */
unsigned int index_box::max()
{
    return (q.max());
}

/*
 * returns the current number of indexes.
 */
unsigned int index_box::count()
{
    return (q.count());
}

/*
 * indicates whether the box is empty.
 */
bool index_box::is_empty()
{
    return (q.is_empty());
}

/*
 * indicates whether the box is full.
 */
bool index_box::is_full()
{
    return (q.is_full());
}
