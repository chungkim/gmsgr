/* 
 * File:   infinite_queue.h
 * Author: Willam Kim
 *
 * Created on May 8, 2007, 1:32 AM
 */

#ifndef _infinite_queue_H
#define	_infinite_queue_H

#include <pthread.h>

#include "queue.h"

/*
 * template infinite queue
 */
template <typename T>
class infinite_queue : public queue<T>
{
public: // private:    
    // -- inner type --
    
    // node structure
    struct node {
	T	key;	// key
	node*	next;	// link to next node
	node(T _key = 0, node* _next = 0) : key(_key), next(_next) {}
    };
    

    // -- fields --
    node* head;
    node* tail;
	
    pthread_mutex_t	mutx;	    // mutex object

    
public:
    // -- construct, destruct --
    
    // constructor
    infinite_queue()
    {
	if (pthread_mutex_init(&mutx, NULL))
	    return;
	
	head = new node;
	if (!head)
	    return;
	
	tail = new node;
	if (!tail)
	    return;
	
	head->next = tail;
	tail->next = tail;
    }

    // destructor
    ~infinite_queue()
    {
	if (tail)
	    delete tail;

	if (head)
	    delete head;
	
	pthread_mutex_destroy(&mutx);
    }
    
    
    // -- operations --
    
    /*
     * puts a key at the rear.
     */
    virtual bool put(T _key)
    {
	if (!head || !tail)
	    return false;
	
	node* s = new node;
	if (!s)
	    return false;
	
	pthread_mutex_lock(&mutx);	
	
	s->key = _key;
	s->next = head->next;
	head->next = s;
	
	pthread_mutex_unlock(&mutx);
	
	return true;
    }

    /*
     * returns a key at the front.
     */
    virtual T get()
    {
	if (!head || !tail)
	    return NULL;

	if (is_empty())
	    return NULL;
	
	node* p = head;
	node* s = p->next;
	while (s->next != tail) 
	{
	    p = p->next;
	    s = s->next;
	}
	
	pthread_mutex_lock(&mutx);
	
	T t_key = s->key;
	p->next = s->next;
	
	pthread_mutex_unlock(&mutx);
	
	delete s;
	
	return t_key;
    }
    
    /*
     * returns the current key count.
     */
    virtual unsigned int count() 
    {
	if (!head || !tail)
	    return 0;
	
	unsigned int cnt = 0;
	
	node* s = head->next;
	while (s != tail)
	{
	    s = s->next;
	    cnt++;
	}
	
	return cnt;
    }
    
    /*
     * removes all keys.
     */
    virtual void clear()
    {
	if (!head || !tail)
	    return;
		
	node* s = head->next;
	node* p;

	pthread_mutex_lock(&mutx);
	
	while (s != tail) 
	{
	    p = s;
	    s = s->next;
	    delete p;
	}
	
	pthread_mutex_unlock(&mutx);
	
    }
    
    /*
     * indicates whether the queue is empty.
     */
    virtual bool is_empty()
    {
	if (!head || !tail)
	    return true;
	
	return (head->next == tail);
    }    
};

#endif	/* _infinite_queue_H */

