// 
// File:   file.h
// Author: William Kim
//
// Created on May 9, 2007, 5:08 PM
//

#ifndef _file_H
#define	_file_H

#include <stdio.h>
#include <string>

/*
 * file
 */
class file 
{
public:
    // -- inner types --
    
    enum FILE_POS {
	POS_CURRENT = SEEK_CUR,
	POS_END	    = SEEK_END,
	POS_BEGIN   = SEEK_SET
    };    
    
    
    // -- fields --
    
    FILE*		fp;	    // file pointer   
    std::string		path;	    // file path
        
    
public:
    // -- construct, destruct --

    // constructor
    file(std::string _path, char* _mode = "r+");
    
    // destructor
    virtual ~file();
    
    
    // -- operations --
    
    /*
     * reads the file.
     */
    int read(void* _buf, int _len);
    
    /*
     * writes the file.
     */
    int write(void* _buf, int _len);
    
    /*
     * truncates file.
     */
    bool truncate(int _len);
    
    /*
     * sets current position.
     */
    bool set_position(int _pos, FILE_POS _from);

    /*
     * returns current position.
     * if it failes, it returns -1.
     */    
    int get_position();

    /*
     * returns the file size.
     * returns -1, if it failes.
     */
    int get_size();
    
    /*
     * returns the file path.
     */
    std::string get_path();

    /*
     * returns the file name.
     */
    std::string get_name();
};

typedef class file* p_file;

#endif	/* _file_H */

