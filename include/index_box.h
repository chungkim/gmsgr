// 
// File:   index_box.h
// Author: William Kim
//
// Created on April 18, 2007, 10:12 PM
//

#ifndef _index_box_H
#define	_index_box_H

#include "circular_queue.h"

/*
 * index box for a server
 */
class index_box
{
private:
    // -- fields --
    
    circular_queue<unsigned int>    q;  // circular queue
    
public:
    // -- construct, destruct --

    // constructor
    index_box(unsigned int _max);

    // destructor
    ~index_box();
	
    
    // -- operations --
    
    /*
     * puts an index into the box.
     */
    bool in(unsigned int _idx);
    
    /*
     * returns an index out of the box.
     */
    unsigned int out();
    
    /*
     * returns the max amount of the box.
     */
    unsigned int max();
    
    /*
     * returns the current number of indexes.
     */
    unsigned int count();
    
    /*
     * indicates whether the box is empty.
     */
    bool is_empty();
    
    /*
     * indicates whether the box is full.
     */
    bool is_full();    
};

typedef class index_box* p_index_box;

#endif	/* _index_box_H */

