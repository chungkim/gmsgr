// 
// File:   gmsgr_packet.h
// Author: William Kim
//
// Created on April 29, 2007, 11:10 PM
//

#ifndef _gmsgr_packet_H
#define	_gmsgr_packet_H

#include "gmsgr_protocol.h"
#include "gmsgr_packet_type.h"

/*
 * g messenger packet head
 */
typedef struct gmsgr_packet_head {
    int			size;	// packet size
    GMSGR_PACKET_TYPE	type;	// packet type
}* p_gmsgr_packet_head;

/*
 * g messenger packet
 */
typedef struct gmsgr_packet {
    gmsgr_packet_head   head;				// packet head
    char		body[GMSGR_MAX_PACKET_BODY];	// packet body
}* p_gmsgr_packet;

#endif	/* _gmsgr_packet_H */

