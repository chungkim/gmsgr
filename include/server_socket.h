// 
// File:   server_socket.h
// Author: William Kim
//
// Created on April 17, 2007, 7:28 PM
//

#ifndef _server_socket_H
#define	_server_socket_H

#include "socket.h"

class server_socket : public socket
{
protected:
    // -- fields --
    
    const unsigned short    port;	// port number
    
    
public:
    // -- construct, destruct --    
    
    // constructor
    server_socket(unsigned short _port);
    
    // destructor
    virtual ~server_socket();

    
    // -- properties --

    /*
     * gets server socket address (ip, port number), 
     * overridden from the class socket.
     */
    virtual bool get_addr(std::string& _ip, unsigned short& _port);	

    
    // -- operations --
    
    /*
     * accepts a client connection. 
     */
    int accept();
    
    /*
     * indicates whether server socket is valid.
     */
    virtual bool is_valid();
    
    /*
     * closes socket and clear data.
     */
    virtual void close();
    
private:    
    // -- methods --
    
    /*
     * binds server socket with the address.
     */
    bool bind();
    
    /*
     * starts listening clients.
     */
    bool listen();
};

typedef server_socket* p_server_socket;

#endif	/* _server_socket_H */

