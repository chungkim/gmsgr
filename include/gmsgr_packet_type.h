// 
// File:   gmsgr_packet_type.h
// Author: William Kim
//
// Created on April 29, 2007, 11:10 PM
//

#ifndef _gmsgr_packet_type_H
#define	_gmsgr_packet_type_H

/*
 * g messenger packet types
 * 
 * APP - apply
 * RSL - result
 * QST - question
 * ASW - answer
 * ACK - acknowledge
 * ESC - escape(cancel)
 */
enum GMSGR_PACKET_TYPE {
    PACKET_DUMMY,		// 0.
    PACKET_JOIN_APP,		// 1.	| ID(20+1) | PWD_HASH(20) |
    PACKET_JOIN_RSL,		// 2.	| JOIN_RSL(4) |
    PACKET_LEAVE_APP,		// 3.	| ID(20+1) |
    PACKET_LEAVE_RSL,		// 4.	| LEAVE_RSL(4) |
    PACKET_LOGIN_APP,		// 5.	| ID(20+1) | PWD_HASH(20) |
    PACKET_LOGIN_RSL,		// 6.	| LOGIN_RSL(4) |
    PACKET_BUDDY_LOGIN,		// 7.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_LOGOUT,	// 8.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_FIND_APP,	// 9.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_FIND_RSL,	// 10.	| BUDDY_FIND_RSL(4) |
    PACKET_BUDDY_ADD_APP,	// 11.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_ADD_RSL,	// 12.	| BUDY_ADD_RSL(4) |
    PACKET_BUDDY_ADD_QST,	// 13.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_ADD_ASW,	// 14.	| BUDDY_ADD_ASW(4) | BUDDY_ID(20+1) |
    PACKET_BUDDY_DEL_APP,	// 15.	| BUDDY_ID(20+1) |
    PACKET_BUDDY_DEL_RSL,	// 16.	| BUDDY_DEL_RSL(4) |
    PACKET_CHAT_MSG,		// 17.	| BUDDY_ID(20+1) | MSG(n+1) |
    PACKET_FILE_APP,		// 18.	| BUDDY_ID(20+1) | FILE_SIZE(4) | FILE_NAME(n+1) |
    PACKET_FILE_RSL,		// 19.	| FILE_RSL(4) | BUDDY_ID(20+1) |
    PACKET_FILE_QST,		// 20.	| BUDDY_ID(20+1) | FILE_SIZE(4) | FILE_NAME(n+1) |
    PACKET_FILE_ASW,		// 21.	| FILE_ASW(4) | BUDDY_ID(20+1) |
    PACKET_FILE_BIN,		// 22.	| BUDDY_ID(20+1) | WHOLE_SIZE(4) | CUR_SIZE(4) | BINARY(n) |
    PACKET_FILE_ACK,		// 23.	| BUDDY_ID(20+1) |
    PACKET_FILE_ESC		// 24.	| BUDDY_ID(20+1) |
};

/*
 * PACKET_JOIN_RSL
 */
typedef enum GMSGR_JOIN_RSL {
    JOIN_RSL_SUCCESS,
    JOIN_RSL_FAIL_ID_EXIST,
    JOIN_RSL_FAIL_UNKNWN
}* P_GMSGR_JOIN_RSL;

/*
 * PACKET_LEAVE_RSL
 */
typedef enum GMSGR_LEAVE_RSL {
    LEAVE_RSL_SUCCESS,
    LEAVE_RSL_FAIL_ID_INVLD,
    LEAVE_RSL_FAIL_UNKNWN
}* P_GMSGR_LEAVE_RSL;

/*
 * PACKET_LOGIN_RSLsensitive
 */
typedef enum GMSGR_LOGIN_RSL {
    LOGIN_RSL_SUCCESS,
    LOGIN_RSL_FAIL_ID_INVLD,
    LOGIN_RSL_FAIL_PWD_INVLD,
    LOGIN_RSL_FAIL_UNKNWN
}* P_GMSGR_LOGIN_RSL;

/*
 * PACKET_BUDDY_FIND_RSL
 */
typedef enum GMSGR_BUDDY_FIND_RSL {
    BUDDY_FIND_RSL_SUCCESS,
    BUDDY_FIND_RSL_FAIL_ID_INVLD,
    BUDDY_FIND_RSL_FAIL_ALREADY,
    BUDDY_FIND_RSL_FAIL_OFFLINE,
    BUDDY_FIND_RSL_FAIL_UNKNWN
}* P_GMSGR_BUDDY_FIND_RSL;

/*
 * PACKET_BUDDY_ADD_RSL
 */
typedef enum GMSGR_BUDDY_ADD_RSL {
    BUDDY_ADD_RSL_SUCCESS,
    BUDDY_ADD_RSL_FAIL_ID_INVLD,
    BUDDY_ADD_RSL_FAIL_USR_FULL,
    BUDDY_ADD_RSL_FAIL_BDY_FULL,
    BUDDY_ADD_RSL_FAIL_OFFLINE,
    BUDDY_ADD_RSL_FAIL_REFUSED,
    BUDDY_ADD_RSL_FAIL_UNKNWN
}* P_GMSGR_BUDDY_ADD_RSL;

/*
 * PACKET_BUDDY_ADD_ASW
 */
typedef enum GMSGR_BUDDY_ADD_ASW {
    BUDDY_ADD_ASW_YES,
    BUDDY_ADD_ASW_NO
}* P_GMSGR_BUDDY_ADD_ASW;

/*
 * PACKET_BUDDY_DEL_RSL
 */
typedef enum GMSGR_BUDDY_DEL_RSL {
    BUDDY_DEL_RSL_SUCCESS,
    BUDDY_DEL_RSL_FAIL_UNKNWN
}* P_GMSGR_BUDDY_DEL_RSL;

/*
 * PACKET_FILE_RSL
 */
typedef enum GMSGR_FILE_RSL {
    FILE_RSL_SUCCESS,
    FILE_RSL_FAIL_REFUSED,
    FILE_RSL_FAIL_UNKNWN
}* P_GMSGR_FILE_RSL;

/*
 * PACKET_FILE_ASW
 */
typedef enum GMSGR_FILE_ASW {
    FILE_ASW_YES,
    FILE_ASW_NO
}* P_GMSGR_FILE_ASW;

#endif	/* _gmsgr_packet_type_H */

