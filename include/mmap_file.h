// 
// File:   mmap_file.h
// Author: William Kim
//
// Created on May 9, 2007, 11:03 PM
//

#ifndef _mmap_file_H
#define	_mmap_file_H

#include <unistd.h>
#include <sys/mman.h>

#include "file.h"

/*
 * memory mapped file
 */
template <typename T>
class mmap_file : public file
{
public:
    // -- inner types
    
    enum MMAP_MODE {
	MODE_READ   = PROT_READ,
	MODE_WRITE  = PROT_WRITE,
	MODE_EXEC   = PROT_EXEC,
	MODE_NONE   = PROT_NONE
    };
    

public:
    // -- public field
    
    T*	    buf;    // mapped buffer
    int	    size;   // mapped size
        
    
    // -- construct, destruct --

    // constructor
    mmap_file(string _path, int _size = -1, MMAP_MODE _mmap_mode = (MMAP_MODE)(MODE_READ|MODE_WRITE), char* _file_mode = "r+");
    
    // destructor
    ~mmap_file();
    
    
    // -- operations --
    
    /*
     * synchronises the mapped file.
     */
#include "mmap_file.h"


    bool sync();     
    
    /*
     * remaps the mapped file.
     */
    bool remap(int _new_size);
};


// -- construct, destruct --

// constructor
template <typename T>
mmap_file<T>::mmap_file(string _path, int _size /* = -1 */, MMAP_MODE _mmap_mode /* = (MMAP_MODE)(PROT_READ|PROT_WRITE) */, char* _file_mode /* = "r+" */)
:   file(_path, _file_mode),	// parent class constructor
    buf(NULL),			// mapped buffer
    size(_size)			// mapped size
{
    if (!fp)
	return;
    
    // get file size to map whole file
    if (size == -1)
    {
	size = get_size();
	if (size == 1)
	    size = 0;
	else if(size == -1)
	    return;
    }

    // expand file if it has no data
    if (_mmap_mode&PROT_WRITE && !get_size())
    {
	size = 0;
	if (!set_position(0, POS_END))
	    return;
	
	if (!::write(fileno(fp), "", 1))
	    return;
	
	if (!set_position(0, POS_BEGIN))
	    return;
    }
    
    // map the file
    buf = (T*)mmap(NULL, (!size)?1:size, _mmap_mode, MAP_SHARED, fileno(fp), 0);
    if (buf == MAP_FAILED)
	buf = NULL;
}

// destructor
template <typename T>
mmap_file<T>::~mmap_file()
{
    if (!buf || size == -1)
	return;
    
    // unmap the file
    munmap(buf, (!size)?1:size);
    
    if (!size && get_size() == 1)
	truncate(0);
}


// -- operations --

/*
 * synchronises the mapped file.
 */
template <typename T>
bool mmap_file<T>::sync()
{
    if (!buf || size == -1)
	return false;
    
//  fflush(fp);
    
    return (!msync((void*)buf, (!size)?1:size, MS_ASYNC));
}

/*
 * remaps the mapped file.
 */
template <typename T>
bool mmap_file<T>::remap(int _new_size)
{
    if (!buf)
	return false;
    
    if (_new_size > size)
    {
	int expand = _new_size - ((!size)?1:size);
	char* tmp = new char[expand];
	if (!tmp)
	    return false;
	memset(tmp, 0, expand);
	
	if (!set_position(0, POS_END))
	{
	    delete []tmp;
	    return false;
	}
	
	if(!::write(fileno(fp), tmp, expand))
	{
	    delete []tmp;
	    return false;
	}
	
	if (!set_position(0, POS_BEGIN))
	{
	    delete []tmp;
	    return false;
	}
	
	delete []tmp;
    }
    else if (_new_size < size)
    {
    	if (!truncate((!_new_size)?1:_new_size))
	    return false;
    }
    else
	return false;

    buf = (T*)mremap((void*)buf, (!size)?1:size, (!_new_size)?1:_new_size, MREMAP_MAYMOVE);
    if (buf == MAP_FAILED)
    {
	buf = NULL;
	return false;
    }
    
    size = _new_size;
    
    return true;
}

#endif	/* _mmap_file_H */

