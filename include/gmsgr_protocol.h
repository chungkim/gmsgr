// 
// File:   gmsgr_protocol.h
// Author: William Kim
//
// Created on May 6, 2007, 5:22 PM
//

#ifndef _gmsgr_protocol_H
#define	_gmsgr_protocol_H

#define GMSGR_MAX_ID		    20				// max user and buddy id length
#define GMSGR_MAX_PWD		    20				// max user and buddy password length
#define GMSGR_MAX_BUDDY		    20				// max buddy count for a user
#define GMSGR_MAX_PACKET_BODY	    5120			// max packet body size (5KB)
#define GMSGR_MAX_PACKET_FILE	    (GMSGR_MAX_PACKET_BODY-30)	// max file binary packet body size
#define GMSGR_SIZEOF_PACKET_HEAD    8				// packet header size
#define GMSGR_SIZEOF_PWD_HASH	    20				// hashed password size

#endif	/* _gmsgr_protocol_H */

