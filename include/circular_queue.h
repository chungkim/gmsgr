// 
// File:   circular_queue.h
// Author: William Kim
//
// Created on April 5, 2007, 3:10 AM
//

#ifndef _circular_queue_H
#define	_circular_queue_H

#include <pthread.h>

#include "queue.h"

/*
 * template circular queue
 */
template <typename T>
class circular_queue : public queue<T>
{
private:
    // -- fields --

    T*			q;	    // array
	
    const unsigned int	limit;	    // limit count of queue (max+1) 
    unsigned int	front;	    // front index of queue
    unsigned int	rear;	    // rear index of queue

    pthread_mutex_t	mutx;	    // mutex object

    
public:
    // -- construct, destruct --
    
    // constructor
    circular_queue(unsigned int _max)
    :	q(NULL),	    // array
	limit(_max + 1),    // limit count of queue
	front(0),	    // front index of queue
	rear(0)		    // rear index of queue
    {
	if (pthread_mutex_init(&mutx, NULL))
	    return;
	
	q = new T[limit];
	if (!q)
	    return;	
    }

    // destructor
    ~circular_queue()
    {
	if (q)
	    delete []q;
	
	pthread_mutex_destroy(&mutx);
    }
    
    
    // -- operations --
    
    /*
     * puts a key at the rear.
     */
    virtual bool put(T _key)
    {
	if (!q)
	    return false;
	
	if (is_full())
	    return false;
	
	pthread_mutex_lock(&mutx);
	
	q[rear] = _key;
	
	rear = (rear+1)%limit;
	
	pthread_mutex_unlock(&mutx);
	
	return true;
    }

    /*
     * returns a key at the front.
     */
    virtual T get()
    {
	if (!q)
	    return -1;
	
	if (is_empty())
	    return -1;
	
	pthread_mutex_lock(&mutx);
	
	T key = q[front];
	
	front = (front+1)%limit;
	
	pthread_mutex_unlock(&mutx);
	
	return key;
    }
    
    /*
     * returns the max key count.
     */
    virtual unsigned int max() 
    {
	return (limit-1);
    }
    
    /*
     * returns the current key count.
     */
    virtual unsigned int count() 
    {
	if (!q)
	    return -1;
	
	unsigned count = rear-front;
	
	if (rear < front)
	    count += limit;
	
	return count;
    }
    
    /*
     * removes all keys.
     */
    virtual void clear()
    {
	pthread_mutex_lock(&mutx);
	
	front = rear = 0;
	
	pthread_mutex_unlock(&mutx);
    }
    
    /*
     * indicates whether the queue is empty.
     */
    virtual bool is_empty()
    {
	if (!q)
	return true;

	return (front == rear);
    }
    
    /*
     * indicates whether the queue is full.
     */
    virtual bool is_full()
    {
	if (!q)
	return false;

	return (front == (rear+1)%limit);
    }
};

#endif	/* _circular_queue_H */
