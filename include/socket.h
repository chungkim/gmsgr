// 
// File:   socket.h
// Author: willam kim
//
// Created on April 17, 2007, 6:58 PM
//

#ifndef _socket_H
#define	_socket_H

#include <string>

class socket 
{
protected:
    // -- fields -- 
    
    int	fd; // file descriptor
    
    
public:
    // -- construct, destruct --
    
    // constructor
    socket();
    
    // destructor
    virtual ~socket();

    
    // -- properties --

    /*
     * sets socket file descriptor.
     * returns false, if the file descriptor is already created.
     */
    virtual bool set_fd(int _fd);
    
    /*
     * gets socket address (ip, port number),
     * child classes must override.
     */
    virtual bool get_addr(std::string& _ip, unsigned short& _port) = 0;	

    
    // -- operations --
    
    /*
     * indicates whether socket is valid.
     */
    virtual bool is_valid();
    
    /*
     * creates socket.
     */
    virtual bool create();
        
    /*
     * closes socket, set file descriptor invalid.
     * child classes can override.
     */
    virtual void close();
};

typedef class socket* p_socket;

#endif	/* _socket_H */

