// 
// File:   queue.h
// Author: William Kim
//
// Created on May 8, 2007, 1:23 AM
//

#ifndef _queue_H
#define	_queue_H

/*
 * abstract template queue
 */
template <typename T>
class queue
{
public:    
    // -- operations --
    
    /*
     * puts a key.
     */
    virtual bool put(T _key) = 0;
    
    /*
     * returns and remove a key.
     */
    virtual T get() = 0;
    
    /*
     * returns the current key count.
     */
    virtual unsigned int count() = 0;

    /*
     * removes all keys.
     */
    virtual void clear() = 0;
    
    /*
     * indicates whether the queue is empty.
     */
    virtual bool is_empty() = 0;
};

#endif	/* _queue_H */

