// 
// File:   client_socket.h
// Author: William Kim
//
// Created on April 17, 2007, 7:28 PM
//

#ifndef _client_socket_H
#define	_client_socket_H

#include "socket.h"

class client_socket : public socket
{
private:
    // -- fields -- 
    
    FILE*   recv_stream;    // stream to receive
    FILE*   send_stream;    // stream to send
    
    
public:
    // -- construct, destruct --
    
    // constructor
    client_socket();
    
    // destructor
    virtual ~client_socket();

    
    // -- properties --

    /*
     * sets socket file descriptor.
     * returns false, if the file descriptor is already created.
     * overridden from the class socket.
     */
    virtual bool set_fd(int _fd);
    
    /*
     * gets client socket address (ip, port number).
     * overridden from the class socket.
     */
    virtual bool get_addr(std::string& _ip, unsigned short& _port);	

    
    // -- operations --
    
    /*
     * creates client socket socket.
     */
    virtual bool create();

    /*
     * connects client socket to the entered address
     */
    bool connect(std::string _ip, unsigned short _port);
    
    /*
     * indicates whether client socket is valid.
     */
    virtual bool is_valid();
    
    /*
     * closes socket and clear data.
     */
    virtual void close();

    // shutdown enumeration
    enum SHUT {
	SHUT_RECV = 0,	// disable receives
	SHUT_SEND,	// disable sends
	SHUT_BOTH	// disable receives and sends
    };
    
    /*
     * disables receives or sends
     */
    bool shutdown(SHUT _how);

    /*
     * receives data, returns the length of data received.
     */
    int recv(void* _buf, int _len);
    
    /*
     * sends data, returns the length of data sent.
     */
    int send(void* _buf, int _len);
    
private:
    // -- methods --
    
    /*
     * opens receive, send streams
     */
    bool open_streams();
};

typedef client_socket* p_client_socket;

#endif	/* _client_socket_H */

