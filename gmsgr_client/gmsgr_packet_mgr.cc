#include <gtkmm.h>
#include <iostream>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_packet_mgr.h"

// -- construct, destruct --
    
// constructor
gmsgr_packet_mgr::gmsgr_packet_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt)	// g messenger client
{
    if (!get_packet_info(pkt_interval)) 
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to get packet manager informtaion from the file!" << endl;
#endif // _DEBUG
	exit(1);
    }    
}

// destructor
gmsgr_packet_mgr::~gmsgr_packet_mgr()
{
    // stop packet manager
    stop();
    
    // free all packets left in the packet queue
    unsigned int cnt = pkt_queue.count();
    for (unsigned int i = 0; i < cnt; i++)
	delete (pkt_queue.get());
}


// -- operations --

/*
 * runs packet manager to transact packets from the server.
 */
void gmsgr_packet_mgr::run()
{
    // connect packet transaction timer procedure
    pkt_timer = Glib::signal_timeout().connect( sigc::bind( 
	sigc::mem_fun(*this, &gmsgr_packet_mgr::on_packet_received), 0 ), pkt_interval );    
    
#ifdef _DEBUG    
    cout << "[Notice] Packet transaction interval: " << pkt_interval << "ms" << endl;
#endif // _DEBUG
}

/*
 * stops packet manager.
 */
void gmsgr_packet_mgr::stop()
{
    // disconnect packet transaction timer procedure
    pkt_timer.disconnect();
}
    
/*
 * adds a packet into the packet queue.
 */
bool gmsgr_packet_mgr::add_packet(p_gmsgr_packet _pkt)
{
    // put a packet into the packet queue
    if (!pkt_queue.put(_pkt))
	return false;    
    
    return true;
}

    
// -- methods --

/*
 * transacts a g messenger packet.
 */
bool gmsgr_packet_mgr::transact(p_gmsgr_packet _pkt)
{
    switch (_pkt->head.type) 
    {
	case PACKET_DUMMY:		    
	    break;
	    
	case PACKET_JOIN_RSL:	
	    if (!transact_join_result(_pkt)) return false;
	    break;
	    
	case PACKET_LEAVE_RSL:		   
	    if (!transact_leave_result(_pkt)) return false;
	    break;

	case PACKET_LOGIN_RSL:		   
	    if (!transact_login_result(_pkt)) return false;
	    break;
	    
	case PACKET_BUDDY_LOGIN:
	    if (!transact_buddy_login(_pkt)) return false;
	    break;

	case PACKET_BUDDY_LOGOUT:
	    transact_buddy_logout(_pkt);
	    break;

	case PACKET_BUDDY_FIND_RSL:		   
	    if (!transact_buddy_find_result(_pkt)) return false;
	    break;

	case PACKET_BUDDY_ADD_QST:		   
	    if (!transact_buddy_add_question(_pkt)) return false;
	    break;
	
	case PACKET_BUDDY_ADD_RSL:		   
	    if (!transact_buddy_add_result(_pkt)) return false;
	    break;
	    
	case PACKET_BUDDY_DEL_RSL:		   
	    if (!transact_buddy_delete_result(_pkt)) return false;
	    break;

	case PACKET_CHAT_MSG:		   
	    if (!transact_chat_message(_pkt)) return false;
	    break;

	case PACKET_FILE_QST:		   
	    if (!transact_file_question(_pkt)) return false;
	    break;

	case PACKET_FILE_RSL:		   
	    if (!transact_file_result(_pkt)) return false;
	    break;

	case PACKET_FILE_BIN:		   
	    if (!transact_file_binary(_pkt)) return false;
	    break;

	case PACKET_FILE_ACK:		   
	    if (!transact_file_acknowledge(_pkt)) return false;
	    break;

	case PACKET_FILE_ESC:		   
	    if (!transact_file_escape(_pkt)) return false;
	    break;

	default:
#ifdef _DEBUG
	    cout << "[Warning] Unknown packet is received!" << endl;
#endif // _DEBUG	
	    return false;
    }
    
    return true;
}

/*
 * transacts the packet JOIN_RSL.
 */
bool gmsgr_packet_mgr::transact_join_result(p_gmsgr_packet _pkt)
{
    GMSGR_JOIN_RSL rsl = *(P_GMSGR_JOIN_RSL)_pkt->body;
    
    // handle join result
    if (!clnt->mgr_certification->handle_join_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet LEAVE_RSL.
 */
bool gmsgr_packet_mgr::transact_leave_result(p_gmsgr_packet _pkt) 
{
    GMSGR_LEAVE_RSL rsl = *(P_GMSGR_LEAVE_RSL)_pkt->body;
    
    // handle join result
    if (!clnt->mgr_certification->handle_leave_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet LOGIN_RSL.
 */
bool gmsgr_packet_mgr::transact_login_result(p_gmsgr_packet _pkt) 
{
    GMSGR_LOGIN_RSL rsl = *(P_GMSGR_LOGIN_RSL)_pkt->body;
    
    // handle join result
    if (!clnt->mgr_certification->handle_login_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet BUDDY_LOGIN.
 */
bool gmsgr_packet_mgr::transact_buddy_login(p_gmsgr_packet _pkt)
{
    string _id(_pkt->body);
    
    // handle buddy login
    if (!clnt->mgr_buddy->handle_buddy_login(_id))
	return false;
    
    return true;
}

/*
 * transacts the packet BUDDY_LOGOUT.
 */
void gmsgr_packet_mgr::transact_buddy_logout(p_gmsgr_packet _pkt)
{
    string _id(_pkt->body);
    
    // handle buddy logout
    clnt->mgr_buddy->handle_buddy_logout(_id);
}
    
/*
 * transacts the packet BUDDY_FIND_RSL.
 */
bool gmsgr_packet_mgr::transact_buddy_find_result(p_gmsgr_packet _pkt)
{
    GMSGR_BUDDY_FIND_RSL rsl = *(P_GMSGR_BUDDY_FIND_RSL)_pkt->body;
    
    // handle buddy find result
    if (!clnt->mgr_buddy->handle_buddy_find_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet BUDDY_ADD_QST.
 */
bool gmsgr_packet_mgr::transact_buddy_add_question(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    
    // handle buddy add question
    if (!clnt->mgr_buddy->handle_buddy_add_question(id))
	return false;
    
    return true;
}

/*
 * transacts the packet BUDDY_ADD_RSL.
 */
bool gmsgr_packet_mgr::transact_buddy_add_result(p_gmsgr_packet _pkt)
{
    GMSGR_BUDDY_ADD_RSL rsl = *(P_GMSGR_BUDDY_ADD_RSL)_pkt->body;
    
    // handle buddy add result
    if (!clnt->mgr_buddy->handle_buddy_add_result(rsl))
	return false;
    
    return true;
}
  
/*
 * transacts the packet BUDDY_DEL_RSL.
 */
bool gmsgr_packet_mgr::transact_buddy_delete_result(p_gmsgr_packet _pkt)
{
    GMSGR_BUDDY_DEL_RSL rsl = *(P_GMSGR_BUDDY_DEL_RSL)_pkt->body;
    
    // handle buddy delete result
    if (!clnt->mgr_buddy->handle_buddy_delete_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet CHAT_MSG.
 */
bool gmsgr_packet_mgr::transact_chat_message(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    string msg(_pkt->body + (GMSGR_MAX_ID+1));
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // take the message
    buddy->get_chat_manager()->take_message(msg);
    
    return true;
}

/*
 * transacts the packet FILE_QST.
 */
bool gmsgr_packet_mgr::transact_file_question(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    int size = *(int*)(_pkt->body + (GMSGR_MAX_ID+1));
    string name(_pkt->body + (GMSGR_MAX_ID+1) + sizeof(size));
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // handle file question
    if (!buddy->get_file_manager()->handle_file_question(name, size))
	return false;
    
    return true;
}

/*
 * transacts the packet FILE_RSL.
 */
bool gmsgr_packet_mgr::transact_file_result(p_gmsgr_packet _pkt)
{
    GMSGR_FILE_RSL rsl = *(P_GMSGR_FILE_RSL)_pkt->body;
    string id(_pkt->body + sizeof(rsl));
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // handle file result
    if (!buddy->get_file_manager()->handle_file_result(rsl))
	return false;
    
    return true;
}

/*
 * transacts the packet FILE_BIN.
 */
bool gmsgr_packet_mgr::transact_file_binary(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    int whole = *(int*)(_pkt->body + (GMSGR_MAX_ID+1));
    int cur = *(int*)(_pkt->body + (GMSGR_MAX_ID+1) + sizeof(whole));
    char* bin = _pkt->body + (GMSGR_MAX_ID+1) + sizeof(whole) + sizeof(cur);    
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // handle file binary
    if (!buddy->get_file_manager()->handle_file_binary(whole, cur, bin))
	return false;
    
    return true;
}

/*
 * transacts the packet FILE_ACK.
 */
bool gmsgr_packet_mgr::transact_file_acknowledge(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // handle file binary
    if (!buddy->get_file_manager()->handle_file_acknowledge())
	return false;
    
    return true;
}

/*
 * transacts the packet FILE_ESC.
 */
bool gmsgr_packet_mgr::transact_file_escape(p_gmsgr_packet _pkt)
{
    string id(_pkt->body);
    
    // get the buddy
    p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(id);
    if (!buddy)
	return false;
    
    // handle file binary
    if (!buddy->get_file_manager()->handle_file_escape())
	return false;
    
    return true;
}

/*
 * gets packets from the packet queue, transacts them.
 */
bool gmsgr_packet_mgr::on_packet_received(int timer_number)
{
    p_gmsgr_packet pkt;
    
    // get a packet from the packet queue
    pkt = pkt_queue.get();
    if (!pkt) 
	return true;
    
#ifdef _DEBUG
    // notice a packet is received
    cout << "[Notice] A packet is picked to be transacted: (" << pkt->head.type << ")" << endl;
#endif // _DEBUG	
    
    // transact the packetFail
#ifdef _DEBUG
    if (transact(pkt))
	cout << "[Notice] Succeeded to transact a packet: (" << pkt->head.type << ")" << endl;
    else
	cerr << "[Error] Failed to transact a packet: (" << pkt->head.type << ")" << endl;
#else
    transact(pkt);
#endif // _DEBUG	

    // free the packet
    delete pkt;
    
    return true;
}

/*
 * reads the packet manager informaion from the file.
 */
bool gmsgr_packet_mgr::get_packet_info(int& _interval)
{
    // read the file
    file file_pkt(GMSGR_PATH_PACKET, "r");
    char data[1024];
    int len = file_pkt.read(data, 1023);
    if (!len)
	return false;
	
    // convert it to a string
    data[len] = 0;
    string info(data);

    int begin, end;
    
    // find packet transaction interval
    begin = info.find("interval=\'");
    if (begin == string::npos)
	return false;    
    begin += 10;
    end = info.find('\'', begin);
    if (end == string::npos)
	return false;	    
    _interval = atoi(info.substr(begin, end - begin).c_str());
    
    return true;    
}
    
