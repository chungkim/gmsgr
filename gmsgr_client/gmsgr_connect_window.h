// 
// File:   gmsgr_connect_window.h
// Author: William Kim
//
// Created on May 27, 2007, 5:24 PM
//

#ifndef _gmsgr_connect_window_H
#define	_gmsgr_connect_window_H

#include <gtkmm.h>

typedef class gmsgr_connection_mgr* p_gmsgr_connection_mgr;

#define GMSGR_CONN_PROG_INTERVAL    60

/*
 * g messenger connect manager
 */
class gmsgr_connect_window : public Gtk::Window
{
private:
    // -- fields --
    
    const p_gmsgr_connection_mgr    mgr_connection;	// connection manager (passed from gmsgr_connection)

    Gtk::VBox			    vbox_all;		// vertical box for all widgets

    Gtk::Label			    label_conn;		// label to for connection
    Gtk::ProgressBar		    prog_conn;		// progress bar for connection
    sigc::connection		    timer_conn;		// timer to pulse connection progress bar
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_connect_window(p_gmsgr_connection_mgr _mgr_connection);
    
    // destructor
    ~gmsgr_connect_window();
    
    
    // -- operations --
    
    
    
private:
    // -- methods --        
    
    /*
     * handles the show message.
     */
    void on_window_show();
    
    /*
     * handles the show message.
     */
    void on_window_hide();
    
    /*
     * pulses the progress bar for connection.
     */
    bool on_timeout_progress_pulse();
};

typedef gmsgr_connect_window* p_gmsgr_connect_window;

#endif	/* _gmsgr_connect_window_H */

