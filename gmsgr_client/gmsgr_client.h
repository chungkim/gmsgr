// 
// File:   gmsgr_client.h
// Author: William Kim
//
// Created on May 5, 2007, 2:32 PM
//

#ifndef _gmsgr_client_H
#define	_gmsgr_client_H

#include <gtkmm.h>
#include <string>

#include "client_socket.h"
#include "gmsgr_connection_mgr.h"
#include "gmsgr_packet_mgr.h"
#include "gmsgr_transmission_mgr.h"
#include "gmsgr_certification_mgr.h"
#include "gmsgr_buddy_mgr.h"
#include "gmsgr_chat_mgr.h"
#include "gmsgr_file_mgr.h"
#include "gmsgr_main_window.h"

#define GMSGR_CLIENT_TITLE  "G Messenger Client"

/*
 * g messenger client
 */
class gmsgr_client
{
private:
    // -- fields --
    
    static gmsgr_client*	instance;	    // singleton instance

    Gtk::Main			gtkmm;		    // gtkmm kit
    
    client_socket		sock;		    // client socket
    
    std::string			id;		    // user id

    p_gmsgr_connection_mgr	mgr_connection;	    // connection manager
    p_gmsgr_packet_mgr		mgr_packet;	    // packet manager
    p_gmsgr_transmission_mgr	mgr_transmission;   // transmission manager
    p_gmsgr_certification_mgr	mgr_certification;  // certification manager
    p_gmsgr_buddy_mgr		mgr_buddy;	    // buddy manager
    
    gmsgr_main_window		wnd_main;	    // main window
    
    
    // -- construct, destruct --

    // constructor
    gmsgr_client(int _argc, char** _argv);

public:
    // destructor
    ~gmsgr_client();
    
    
    // -- properties --
    
    /*
     * returns user id.
     */
    std::string get_id();    
    
    
    // -- operations --
    
    /*
     * creates the g messenger client instance. (singleton pattern)
     */
    static gmsgr_client* create_instance(int _argc, char** _argv);
    
    /*
     * runs g messenger client.
     */
    void run();
    
    /*
     * stops g messenger client.
     */
    void stop();
    
    /*
     * resets g messager client.
     */
    void reset();
    
    /*
     * runs a message dialog synchronously.
     */
    int run_msg_dlg(const Glib::ustring& _msg, Gtk::Window* _prnt = NULL, 
	Gtk::MessageType _type = Gtk::MESSAGE_INFO, Gtk::ButtonsType _btns = Gtk::BUTTONS_OK);
    
    /*
     * shows a message dialog asynchronously.
     */
    void show_msg_dlg(const Glib::ustring& _msg, Gtk::Window* _prnt = NULL, 
	Gtk::MessageType _type = Gtk::MESSAGE_INFO, Gtk::ButtonsType _btns = Gtk::BUTTONS_OK);

    
private:    
    // -- aggregaction --
    
    friend class gmsgr_connection_mgr;	    // connection manager
    friend class gmsgr_packet_mgr;	    // packet manager
    friend class gmsgr_transmission_mgr;    // transmission manager
    friend class gmsgr_certification_mgr;   // certification manager
    friend class gmsgr_buddy_mgr;	    // buddy manager
    friend class gmsgr_chat_mgr;	    // chat manager
    friend class gmsgr_file_mgr;	    // file manager
    friend class gmsgr_main_window;	    // main window
};

typedef gmsgr_client* p_gmsgr_client;

#endif	/* _gmsgr_client_H */

