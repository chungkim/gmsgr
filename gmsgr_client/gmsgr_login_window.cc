#include <gtkmm.h>
#include <iostream>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_certification_mgr.h"
#include "gmsgr_login_window.h"

// -- construct, destruct --

// constructor
gmsgr_login_window::gmsgr_login_window(p_gmsgr_certification_mgr _mgr_certification)
:   mgr_certification(_mgr_certification),  // certification manager
    label_id("ID:"),			    // entry for id 
    label_pwd("Password:"),		    // entry for password 
    btn_login("Login"),			    // login button
    btn_join("Join")			    // join button
{
    // set window details
    set_default_size(300, 150);
    set_title("G Messenger - Login");
    set_position(WIN_POS_CENTER);
    set_resizable(false);
    set_border_width(5);
    if (!set_icon_from_file(GMSGR_PATH_ICON))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to set icon for login window!" << endl;
#endif // _DEBUG	
	exit(1);
    }
    signal_show().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_window_show) );
    signal_hide().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_window_hide) );

    // set horizontal box for id
    label_id.set_size_request(label_id.get_width() + 62, label_id.get_height());
    entry_id.signal_key_release_event().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_entry_key_release) );
    entry_id.set_max_length(20);
    entry_id.set_text("");
    hbox_id.pack_start(label_id);
    hbox_id.pack_start(entry_id);
    hbox_id.set_border_width(5);
    hbox_id.set_spacing(5);
    
    // set horizontal box for password
    entry_pwd.set_max_length(20);
    entry_pwd.set_text("");
    entry_pwd.set_visibility(false);
    entry_pwd.signal_key_release_event().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_entry_key_release) );
    hbox_pwd.pack_start(label_pwd);
    hbox_pwd.pack_start(entry_pwd);
    hbox_pwd.set_border_width(5);
    hbox_pwd.set_spacing(5);
    
    // set horizontal box for buttons
    btn_login.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_login_btn_clicked) );
    btn_join.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_login_window::on_join_btn_clicked) );
    hbox_btns.pack_start(btn_login);
    hbox_btns.pack_start(btn_join);
    hbox_btns.set_border_width(5);
    hbox_btns.set_spacing(5);
    
    // set vertical box for hboxes
    vbox_hboxes.add(hbox_id);
    vbox_hboxes.add(hbox_pwd);
    vbox_hboxes.add(hbox_btns);
    
    // add the vertical box into window
    add(vbox_hboxes);

    // show all widgets
    show_all_children();
}

// destructor
gmsgr_login_window::~gmsgr_login_window()
{
}


// -- operations --

/*
 * focuses on id entry.
 */
void gmsgr_login_window::focus_id_entry()
{
    set_focus(entry_id);
}

/*
 * focuses on password entry.
 */
void gmsgr_login_window::focus_pwd_entry()
{
    set_focus(entry_pwd);
}

/*
 * empties id entry.
 */
void gmsgr_login_window::clear_id_entry()
{
    entry_id.set_text("");
}

/*
 * empties password entry.
 */
void gmsgr_login_window::clear_pwd_entry()
{
    entry_pwd.set_text("");
}

/*
 * returns the id on id entry.
 */
string gmsgr_login_window::get_id()
{
    return entry_id.get_text();
}
    
    
// -- methods -- 

/*
 * handles the show message.
 */
void gmsgr_login_window::on_window_show()
{
    
}

/*
 * handles the show message.
 */
void gmsgr_login_window::on_window_hide()
{
    // clear id, password entries
    clear_id_entry();
    clear_pwd_entry();
    
    // quit application, if it's not logined
    if (mgr_certification->clnt->get_id().empty())
    {
	mgr_certification->clnt->stop();
	exit(0);
    }
}

/*
 * handles the clicked signal on login button.
 */
void gmsgr_login_window::on_login_btn_clicked()
{
    // is id typed?
    if (!entry_id.get_text_length())
    {
	mgr_certification->clnt->run_msg_dlg("Please enter your id!", this, MESSAGE_WARNING);
	focus_id_entry();
	return;
    }
    
    // is password typed?
    if (!entry_pwd.get_text_length())
    {
	mgr_certification->clnt->run_msg_dlg("Please enter your password!", this, MESSAGE_WARNING);
	focus_pwd_entry();
	return;
    }
    
    // disable window until the result comes
    set_sensitive(false);
    
    // apply login
    if (!mgr_certification->apply_login(entry_id.get_text(), entry_pwd.get_text()))
    {
#ifdef _DEBUG    
	cerr << "[Error] Fail to apply login!" << endl;
#endif // _DEBUG
	exit(1);
    }
}

/*
 * handles the clicked signal on join button.
 */
void gmsgr_login_window::on_join_btn_clicked()
{
    // is id typed?
    if (!entry_id.get_text_length())
    {
	mgr_certification->clnt->run_msg_dlg("Please enter your id!", this, MESSAGE_WARNING);
	focus_id_entry();
	return;
    }
    
    // is password typed?
    if (!entry_pwd.get_text_length())
    {
	mgr_certification->clnt->run_msg_dlg("Please enter your password!", this, MESSAGE_WARNING);
	focus_pwd_entry();
	return;
    }
    
    // disable window until the result comes
    set_sensitive(false);
    
    // apply login
    if (!mgr_certification->apply_join(entry_id.get_text(), entry_pwd.get_text()))
    {
#ifdef _DEBUG    
	cerr << "[Error] Fail to apply join!" << endl;
#endif // _DEBUG
	exit(1);
    }
}

/*
 * handles the key release signal on id, password entries.
 */
bool gmsgr_login_window::on_entry_key_release(GdkEventKey* event)
{
    // click login button, if enter key is released
    if (event->keyval == 0xFF0D)
	btn_login.clicked();
    // hide window, if escape is relesed
    else if (event->keyval == 0xFF1B)
	hide();
    
    return false;
}
