// 
// File:   gmsgr_chat_window.h
// Author: William Kim
//
// Created on May 5, 2007, 3:55 PM
//

#ifndef _gmsgr_chat_window_H
#define	_gmsgr_chat_window_H

#include <string>
#include <gtkmm.h>

typedef class gmsgr_buddy* p_gmsgr_buddy;

/*
 * g messenger chat message window
 */
class gmsgr_chat_window : public Gtk::Window
{
private:
    // -- fields --
    
    const p_gmsgr_buddy		    buddy;	    // g messenger buddy (passed from gmsgr_buddy)

    bool			    full_screened;  // indicates whether window is full-screened

    Gtk::Table			    tbl_all;	    // table for all widgets
    
    Gtk::ScrolledWindow		    scrl_output;    // scrolled window for message output entry
    Gtk::TextView		    txt_output;	    // message output text view
    Glib::RefPtr<Gtk::TextBuffer>   buf_output;	    // text buffer for message output text view
    
    Gtk::ScrolledWindow		    scrl_input;	    // scrolled window for message input entry
    Gtk::TextView		    txt_input;	    // message input text view
    Glib::RefPtr<Gtk::TextBuffer>   buf_input;	    // text buffer for message intput text view
    bool			    ctrl_pressed;   // indicates whether left control key is pressed
    bool			    shift_pressed;  // indicates whether left shift key is pressed
    
    Gtk::Button			    btn_msg;	    // message sending button
    Gtk::Button			    btn_file;	    // file sending button
    
    Gtk::ProgressBar		    prog_file;	    // file sending progress bar
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_chat_window(p_gmsgr_buddy _buddy);
    
    // destructor
    ~gmsgr_chat_window();
    
    
    // -- operations --
    
    /*
     * adds a message into the output text view.
     */
    void add_message(std::string _id, std::string _msg);
    
    /*
     * adds a notice into the output text view.
     */
    void add_notice(std::string _notice);

    /*
     * enables or diables file sending button.
     */
    void enable_file_button(bool enable);
    
    /*
     * click file sending button.
     */
    void click_file_button();
    
    /*
     * sets current percent of progress bar
     */
    void set_progress(int _cur, int _max);
    
    /*
     * runs a file dialog.
     */
    bool run_file_dialog(Gtk::Window* _prnt, std::string& _path, bool save, std::string* _name = NULL);
    
    
private:
    // -- methods --        
    
    /*
     * indicates whether the input entry is empty.
     */
    bool is_input_entry_empty();
    
    /*
     * handles the show message.
     */
    void on_window_show();

    /*
     * handles the hide message.
     */
    void on_window_hide();
    
    /*
     * handles the clicked signal on message sending button.
     */
    void on_message_btn_clicked();
    
    /*
     * handles the clicked signal on file sending button.
     */
    void on_file_btn_clicked();
    
    /*
     * handles the key press signal on input entry.
     */
    bool on_input_entry_key_press(GdkEventKey* _event);

    /*
     * handles the key release signal on input entry.
     */
    bool on_input_entry_key_release(GdkEventKey* _event);    
    
    /*
     * handles the key release signal on output entry.
     */
    bool on_output_entry_key_release(GdkEventKey* _event);    

    /*
     * handles the key release signal on window.
     */
    bool on_window_key_release(GdkEventKey* _event);    
};

typedef gmsgr_chat_window* p_gmsgr_chat_window;

#endif	/* _gmsgr_chat_window_H */

