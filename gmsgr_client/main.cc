// 
// File:   main.cc
// Author: William Kim
//
// Created on May 4, 2007, 11:04 PM
//

#include <iostream>
using namespace std;

#include "gmsgr_client.h"   // g messenger client

int
main(int argc, char** argv) 
{
    // create the g messenger client instance
    p_gmsgr_client clnt = gmsgr_client::create_instance(argc, argv);
    if (!clnt)
    {
#ifdef _DEBUG	
	cerr << "[Error] Not enough memory for client instance!" << endl;
#endif // _DEBUG	
	return 1;
    }
    
    // run g messenger client
    clnt->run(); 
    
    // delete the g messenger client instance
    delete clnt;

    return 0;
}


