#include <iostream>
#include <gtkmm.h>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_buddy.h"
#include "gmsgr_chat_window.h"

// -- construct, destruct --

// constructor
gmsgr_chat_window::gmsgr_chat_window(p_gmsgr_buddy _buddy)
:   buddy(_buddy),		    // g messenger buddy
    full_screened(false),	    // indicates whether window is full-screened
    tbl_all(4, 2),		    // table for all widgets
    ctrl_pressed(false),	    // indicates whether left control key is pressed
    shift_pressed(false),	    // indicates whether left shift key is pressed
    btn_msg("Message"),		    // message sending button
    btn_file("File")		    // file sending button
{
    set_default_size(260, 320);
    set_border_width(5);
    if (!set_icon_from_file(GMSGR_PATH_ICON))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to set icon for chat window!" << endl;
#endif // _DEBUG	
	exit(1);
    }
    signal_show().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_window_show) );
    signal_hide().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_window_hide) );
    
    // set message output text view
    buf_output = Gtk::TextBuffer::create();
    txt_output.set_buffer(buf_output);
    txt_output.set_editable(false);
    txt_output.set_accepts_tab(false);
    txt_output.set_left_margin(5);
    txt_output.set_right_margin(5);
    txt_output.set_pixels_above_lines(0);
    txt_output.set_wrap_mode(WRAP_CHAR);
    txt_output.signal_key_release_event().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_output_entry_key_release) );
    scrl_output.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    scrl_output.add(txt_output);

    // set message input text view
    buf_input = Gtk::TextBuffer::create();
    txt_input.set_buffer(buf_input);
    txt_input.set_accepts_tab(false);
    txt_input.set_left_margin(5);
    txt_input.set_right_margin(5);
    txt_input.set_pixels_above_lines(4);
    txt_input.set_wrap_mode(WRAP_CHAR);
    txt_input.signal_key_press_event().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_input_entry_key_press) );
    txt_input.signal_key_release_event().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_input_entry_key_release) );
    scrl_input.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    scrl_input.add(txt_input);
    
    // set buttons
    btn_msg.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_message_btn_clicked) );
    btn_file.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_chat_window::on_file_btn_clicked) );
    
    // attach all widgets into the table
    tbl_all.attach(scrl_output, 0, 2, 0, 1, EXPAND|FILL, EXPAND|FILL, 3, 3);
    tbl_all.attach(scrl_input, 0, 1, 1, 3, EXPAND|FILL, FILL, 3, 3);
    tbl_all.attach(btn_msg, 1, 2, 1, 2, SHRINK|FILL, SHRINK|FILL, 2, 2);
    tbl_all.attach(btn_file, 1, 2, 2, 3, SHRINK|FILL, SHRINK|FILL, 2, 2);
    tbl_all.attach(prog_file, 0, 2, 3, 4, SHRINK|FILL, SHRINK|FILL, 2, 2);
    
    // add table into the window
    add(tbl_all);
    
    show_all_children();
}

// destructor
gmsgr_chat_window::~gmsgr_chat_window()
{
    
}


// -- operations --

/*
 * adds a message into the output text view.
 */
void gmsgr_chat_window::add_message(string _id, string _msg)
{
    // add the id first
    buf_output->insert(buf_output->end(), _id + " :\n");

    // add the message
    buf_output->insert(buf_output->end(), _msg + "\n\n");
    
    // pull the scroll down
    buf_output->move_mark(buf_output->get_insert(), buf_output->end());
    txt_output.scroll_to_mark(buf_output->get_insert(), 0.0);
}

/*
 * adds a notice into the output text view.
 */
void gmsgr_chat_window::add_notice(string _notice)
{
    // add the title first
    buf_output->insert(buf_output->end(), " - G Messenger :\n");
    
    // add the notice
    buf_output->insert(buf_output->end(), _notice + "\n\n");

    // pull the scroll down
    buf_output->move_mark(buf_output->get_insert(), buf_output->end());
    txt_output.scroll_to_mark(buf_output->get_insert(), 0.0);
}

/*
 * enables or diables file sending button.
 */
void gmsgr_chat_window::enable_file_button(bool enable)
{
    btn_file.set_sensitive(enable);
}

/*
 * click file sending button.
 */
void gmsgr_chat_window::click_file_button()
{
    btn_file.clicked();
}

/*
 * sets current percent of progress bar
 */
void gmsgr_chat_window::set_progress(int _cur, int _max)
{
    char text[1024];
    sprintf(text, "%dkb / %dkb (%d\%)", _cur/1024, _max/1024, (int)(100*((double)_cur/_max)));
    prog_file.set_text(text);
    prog_file.set_fraction((double)_cur/_max);
}

/*
 * runs a file dialog.
 */
bool gmsgr_chat_window::run_file_dialog(Window* _prnt, string& _path, bool save, std::string* _name /* = NULL */)
{
    FileChooserDialog dialog("G Messenger - Please choose a file.", 
	(save) ? FILE_CHOOSER_ACTION_SAVE : FILE_CHOOSER_ACTION_OPEN);
    if (_prnt)
	dialog.set_transient_for(*_prnt);
    
    // add response buttons into the dialog
    dialog.add_button(Stock::CANCEL, RESPONSE_CANCEL);
    dialog.add_button((save) ? Stock::SAVE : Stock::OK, RESPONSE_OK);
    
    // add a filter
    if (save)
    {
	if (_name)
	{
	    string path = dialog.get_current_folder();
	    dialog.set_filename(path + "/" + *_name);
	}
    }
    else
    {
	FileFilter filter;
    	filter.set_name("Any files");
	filter.add_pattern("*");
	dialog.add_filter(filter);
    }
    
    // run the dialog
    int rsl = dialog.run();
    if (rsl == RESPONSE_OK)
    {
	_path = dialog.get_filename();
	return true;
    }
    
    return false;
}


// -- methods --

/*
 * indicates whether the input entry is empty.
 */
bool gmsgr_chat_window::is_input_entry_empty()
{
    if (buf_input->begin() == buf_input->end())
	return true;
    
    string msg = buf_input->get_text();
    if (msg == "\n")
	return true;
    
    return false;
}

/*
 * handles the show message.
 */
void gmsgr_chat_window::on_window_show()
{
    // set title with the name of the buddy
    string title("with ");
    set_title(title + buddy->id);

    // set control key released
    ctrl_pressed = false;
    
    // clear input and output text view
    buf_input->set_text("");
    buf_output->set_text("");
    
    // disable message sending button
    btn_msg.set_sensitive(false);
    
    // enable file sending button
    btn_file.set_sensitive(true);
    
    // focus on message input text view
    set_focus(txt_input);
    
    // set current progress zero
    set_progress(0, 1);
}

/*
 * handles the hide message.
 */
void gmsgr_chat_window::on_window_hide()
{
    // cancel file transferring, if it's going on
    buddy->mgr_file->escape_file();
}

/*
 * handles the clicked signal on message sending button.
 */
void gmsgr_chat_window::on_message_btn_clicked()
{
    // focus on message input text view
    set_focus(txt_input);
    
    // get the input text
    string msg = buf_input->get_text();

    // erase '\n' at the end, if it's exist
    int idx = msg.length() - 1;
    if (idx >= 0 && msg[idx] == '\n')
	msg.erase(idx, 1);
    
    // if it's empty, just return
    if (msg.empty())
	return;
    
    // clear input text view
    buf_input->set_text("");    
    
    // disable message sending button
    btn_msg.set_sensitive(false);
    
    // gives the message to the buddy
    if (!buddy->mgr_chat->give_message(msg))
    {
	add_notice("The message is not sent yet!\nPlease try again.");
	return;
    }
    
    // add the message into output text view
    add_message(buddy->clnt->get_id(), msg);
}

/*
 * handles the clicked signal on file sending button.
 */
void gmsgr_chat_window::on_file_btn_clicked()
{
    // run a file dialog
    string path;
    if (run_file_dialog(this, path, false))
    {
	// disable file sending button
	enable_file_button(false);
	
	// apply to send the file
	if (!buddy->mgr_file->apply_file(path))
	{
	    add_notice("The file is invalid to send!");
	    enable_file_button(true);
	}
    }
    
    // focus on message input text view
    set_focus(txt_input);
}

/*
 * handles the key press signal on input entry.
 */
bool gmsgr_chat_window::on_input_entry_key_press(GdkEventKey* _event)
{
    // set left control key is pressed
    if (_event->keyval == 0xFFE3)
	ctrl_pressed = true;
    
    // set left shift key is pressed
    else if (_event->keyval == 0xFFE1)
	shift_pressed = true;
    
    return true;
}

/*
 * handles the key release signal on input entry.
 */
bool gmsgr_chat_window::on_input_entry_key_release(GdkEventKey* _event)
{
    // set left control key is released
    if (_event->keyval == 0xFFE3)
	ctrl_pressed = false;
    
    // set left shift key is released
    else if (_event->keyval == 0xFFE1)
	shift_pressed = false;
    
    // click message sending button, if enter key is released when left control or shift key is not pressed
    else if (_event->keyval == 0xFF0D && (!ctrl_pressed && !shift_pressed))
	btn_msg.clicked();
    
    // enable or disable message sending button by the count of charactors
    btn_msg.set_sensitive(buf_input->begin() != buf_input->end());   
    
    // pass it to key release handler for window
    return on_window_key_release(_event);
}

/*
 * handles the key release signal on output entry.
 */
bool gmsgr_chat_window::on_output_entry_key_release(GdkEventKey* _event)
{
    // pass it to key release handler for window
    return on_window_key_release(_event);
}

/*
 * handles the key release signal on window.
 */
bool gmsgr_chat_window::on_window_key_release(GdkEventKey* _event)
{
    // hide window, if escape is released
    if (_event->keyval == 0xFF1B)
	hide();
    
    // full-screen or unfullscreen, if f11 is released
    else if (_event->keyval == 0xFFC8)
    {
	full_screened = !full_screened;	
	(full_screened) ? fullscreen() : unfullscreen();
    }
    
    return true;
}
