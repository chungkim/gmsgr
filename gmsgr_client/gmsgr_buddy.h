// 
// File:   gmsgr_buddy.h
// Author: William Kim
//
// Created on May 21, 2007, 5:05 PM
//

#ifndef _gmsgr_buddy_H
#define	_gmsgr_buddy_H

#include <string>

#include "gmsgr_chat_mgr.h"
#include "gmsgr_file_mgr.h"
#include "gmsgr_chat_window.h"

typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger buddy
 */
class gmsgr_buddy
{
private:
    // -- fields --
    
    const p_gmsgr_client	clnt;		    // g messenger client (passed from gmsgr_buddy_mgr)
    
    std::string			id;		    // buddy id
    
    p_gmsgr_chat_mgr		mgr_chat;	    // chat manager
    p_gmsgr_file_mgr		mgr_file;	    // file manager
    
    gmsgr_chat_window		wnd_chat;	    // chat window
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_buddy(std::string _id, p_gmsgr_client _clnt);
    
    // destructor
    ~gmsgr_buddy();
    
    
    // -- properties --
    
    /*
     * returns buddy id.
     */
    std::string get_id();
    
    /*
     * returns chat manager.
     */
    p_gmsgr_chat_mgr get_chat_manager();
    
    /*
     * returns file manager.
     */
    p_gmsgr_file_mgr get_file_manager();
    
    
    // -- operations --
    
    
private:
    // -- methods --        
    
    
    // -- aggregation --
    
    friend class gmsgr_chat_mgr;    // chat manager
    friend class gmsgr_file_mgr;    // file manager
    friend class gmsgr_chat_window; // chat window
};

typedef gmsgr_buddy* p_gmsgr_buddy;

#endif	/* _gmsgr_buddy_H */

