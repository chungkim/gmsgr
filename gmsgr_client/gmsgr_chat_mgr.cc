#include <iostream>
using namespace std;

#include "gmsgr_client.h"
#include "gmsgr_protocol.h"
#include "gmsgr_packet.h"
#include "gmsgr_chat_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_chat_mgr::gmsgr_chat_mgr(p_gmsgr_buddy _buddy)
:   buddy(_buddy)   // g messenger buddy
{
    
}

// destructor
gmsgr_chat_mgr::~gmsgr_chat_mgr()
{
    
}


// -- operations --

/*
 * runs chat manager.
 */
void gmsgr_chat_mgr::run()
{
    // show and raise chat window
    buddy->wnd_chat.show();
    buddy->wnd_chat.raise();
}
    
/*
 * gives a message to the buddy.
 */
bool gmsgr_chat_mgr::give_message(std::string _msg)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_CHAT_MSG;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + (_msg.length()+1);
    
    // set the packet body
    strncpy(pkt.body, buddy->id.c_str(), GMSGR_MAX_ID);
    strncpy(pkt.body + (GMSGR_MAX_ID+1), _msg.c_str(), (GMSGR_MAX_PACKET_BODY - (GMSGR_MAX_ID+1)));
   
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
	return false;
    
#ifdef _DEBUG    
    // notice the chat apply packet is sent
    cout << "[Notice] Chat message packet is sent!" << endl;
#endif // _DEBUG

    return true;
}
    
/*
 * takes a message to the buddy.
 */
void gmsgr_chat_mgr::take_message(std::string _msg)
{
    // show and raise chat window
    buddy->wnd_chat.show();
    buddy->wnd_chat.raise();
    
    // add message into output text view
    buddy->wnd_chat.add_message(buddy->id, _msg);    
}
    
    
// -- methods --

