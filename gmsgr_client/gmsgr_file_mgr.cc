#include <iostream>
#include <math.h>
#include <gtkmm.h>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_protocol.h"
#include "gmsgr_packet.h"
#include "gmsgr_buddy.h"
#include "gmsgr_file_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_file_mgr::gmsgr_file_mgr(p_gmsgr_buddy _buddy)
:   buddy(_buddy),  // g messenger buddy
    file_obj(NULL)  // file object
{
    
}

// destructor
gmsgr_file_mgr::~gmsgr_file_mgr()
{
    
}


// -- operations --

/*
 * runs file manager.
 */
void gmsgr_file_mgr::run()
{
    // show and raise chat window
    buddy->wnd_chat.show();
    buddy->wnd_chat.raise();
    
    // click file sending button
    buddy->wnd_chat.click_file_button();
}

/*
 * applies sending file to the buddy.
 */
bool gmsgr_file_mgr::apply_file(string _path)
{
    if (file_obj)
    {
#ifdef _DEBUG    
	cerr << "[Error] You have a file transferring already!" << endl;
#endif // _DEBUG
	return false;
    }
    
    // allocate a file object
    file_obj = new file(_path, "r");
    if (!file_obj) 
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for a file object!" << endl;
#endif // _DEBUG
	return false;
    }
    else if (!file_obj->fp) 
    {
#ifdef _DEBUG    
	cerr << "[Error] Fail to open a file!" << endl;
#endif // _DEBUG
	delete file_obj;
	file_obj = NULL;
	return false;
    }
	    
    // get file size and name
    int size = file_obj->get_size();
    if (size <= 0)
    {
	delete file_obj;
	file_obj = NULL;
	return false;    
    }
    string name = file_obj->get_name();
    if (name.empty())
    {
	delete file_obj;
	file_obj = NULL;
	return false;    
    }
    
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_FILE_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + sizeof(size) + (name.length()+1);
    
    // set the packet body
    strncpy(pkt.body, buddy->id.c_str(), GMSGR_MAX_ID);
    memcpy(pkt.body + (GMSGR_MAX_ID+1), &size, sizeof(size));
    strncpy(pkt.body + (GMSGR_MAX_ID+1) + sizeof(size), name.c_str(), (GMSGR_MAX_PACKET_BODY - (GMSGR_MAX_ID+1) - sizeof(size)));
   
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
    {
	delete file_obj;
	file_obj = NULL;
	return false;    
    }
    
#ifdef _DEBUG    
    cout << "[Notice] File apply packet is sent to the buddy[" << buddy->id << "]: (" 
	<< name << "/" << parse_file_size(size) << ")" << endl;
#endif // _DEBUG
    
    return true;
}

/*
 * escapes(cancels) file transferring.
 */
bool gmsgr_file_mgr::escape_file()
{
    if (!file_obj)
	return false;
	
    // free the file object
    delete file_obj;
    file_obj = NULL;
    
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_FILE_ESC;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, buddy->id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
	return false;
    
    // enable file sending button
    buddy->wnd_chat.enable_file_button(true);
    
    // notice it's escaped
    buddy->wnd_chat.add_notice("File transferring is canceled!");
    
#ifdef _DEBUG    
    cout << "[Notice] File escape is sent to the buddy[" << buddy->id << "]!" << endl;
#endif // _DEBUG
    
    return true;
}
 
/*
 * handles file question.
 */
bool gmsgr_file_mgr::handle_file_question(std::string _name, int _size)
{
    int rsl = RESPONSE_NO;
    
    // answer no, if file transferring is already on
    if (!file_obj)
    {
	// show and raise chat window
	buddy->wnd_chat.show();
	buddy->wnd_chat.raise();

	// ask user whether he or she wants to accept
	string msg("Buddy \"");
	rsl = buddy->clnt->run_msg_dlg(msg + buddy->id + "\" wants to send a file to you!\n\n\tName:\t" 
	    + _name + "\n\tSize:\t" + parse_file_size(_size) + "\n\nWould you like to receive the file?", 
	    &buddy->wnd_chat, MESSAGE_QUESTION, BUTTONS_YES_NO);

	if (rsl == RESPONSE_YES)
	{
	    string path;

	    // run file dialog for the path to save the file
	    if (buddy->wnd_chat.run_file_dialog(&buddy->wnd_chat, path, true, &_name))
	    {
		// allocate a file object
		file_obj = new file(path, "w");
		if (!file_obj)
		{
#ifdef _DEBUG    
		    cerr << "[Error] Not enough memory for a file object!" << endl;
#endif // _DEBUG
		    return false;
		}
		else if (!file_obj->fp)
		{
		    cerr << "[Error] Fail to create a file!" << endl;
		    delete file_obj;
		    file_obj = NULL;

		    msg = "Cannot create the file \"";
		    buddy->wnd_chat.add_notice(msg + _name + "\"!\nReceiving the file will be denied.");

		    rsl = RESPONSE_NO;		    
		}
		else
		{
		    // disable file sending button
		    buddy->wnd_chat.enable_file_button(false);
		    
		    // set current progress zero
		    buddy->wnd_chat.set_progress(0, 1);
		    
		    // notice it's started
		    string notice = "Receiving the file \"";
		    buddy->wnd_chat.add_notice(notice + file_obj->get_name() + "\" is started!");		
		}
	    }
	    else
		rsl = RESPONSE_NO;
	}
    }

    // answer the question
    if (!answer_file((rsl == RESPONSE_YES) ? FILE_ASW_YES : FILE_ASW_NO, buddy->id))
	return false;
    
    return true;
}

/*
 * handles file result.
 */
bool gmsgr_file_mgr::handle_file_result(GMSGR_FILE_RSL _rsl)
{
    if (!file_obj)
    {
#ifdef _DEBUG    
	cerr << "[Error] File object is not allocated, when the file result comes!" << endl;
#endif // _DEBUG
	return false;
    }
    
    string notice;
		
    // handle by the result value
    switch (_rsl)
    {
	case FILE_RSL_SUCCESS:
	    // notice it's started
	    notice = "Sending the file \"";
	    buddy->wnd_chat.add_notice(notice + file_obj->get_name() + "\" is started!");
	    
	    // set current progress zero
	    buddy->wnd_chat.set_progress(0, 1);
	    
	    // send the first part
	    if (!send_file_binary())
		escape_file();
	    
	    break;
	    
	case FILE_RSL_FAIL_REFUSED:
	    delete file_obj;
	    file_obj = NULL;
	    buddy->wnd_chat.add_notice("The buddy has denied to receive the file!");
	    buddy->wnd_chat.enable_file_button(true);
	    break;

	case FILE_RSL_FAIL_UNKNWN: 
	    buddy->clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", 
		&buddy->wnd_chat, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
    
    return true;
}

/*
 * handles file binary.
 */
bool gmsgr_file_mgr::handle_file_binary(int _whole, int _cur, char* _bin)
{
    if (!file_obj)
    {
#ifdef _DEBUG    
	cerr << "[Error] File object is not allocated, when the file binary comes!" << endl;
#endif // _DEBUG
	return false;
    }
    
    // get received binary size
    int part = (_whole - _cur < GMSGR_MAX_PACKET_FILE) ? (_whole - _cur) : GMSGR_MAX_PACKET_FILE;
    
    // write the file    
    if (file_obj->write(_bin, part) != part)
    {
	escape_file();
	return false;
    }
    
    // add current file size
    _cur += part;

    // check the size
    if (file_obj->get_size() != _cur)
    {
	escape_file();
	return false;	
    }
    
    // set the progress
    buddy->wnd_chat.set_progress(_cur, _whole);

    // acknowledge that binary is received
    if (!acknowledge_file())
    {
	escape_file();
	return false;
    }
    
#ifdef _DEBUG    
    cout << "[Notice] File binary is received from the buddy[" << buddy->id << "]: (" 
	<< file_obj->get_name() << "/" << parse_file_size(_cur) << ")" << endl;
#endif // _DEBUG
    
    // free the file object, if it's done
    if (_cur == _whole)
    {
	// notice it's done
	string notice("The file \"");
	buddy->wnd_chat.add_notice(notice + file_obj->get_name() + "\" is successfully received!");
	
	delete file_obj;
	file_obj = NULL;

	// enable file sending button
	buddy->wnd_chat.enable_file_button(true);
    }
    
    return true;
}

/*
 * handles file acknowledge.
 */
bool gmsgr_file_mgr::handle_file_acknowledge()
{
    if (!file_obj)
    {
#ifdef _DEBUG
	cerr << "[Error] File object is not allocated, when the file acknowledge comes!" << endl;
#endif // _DEBUG
	return false;
    }
    
    // get whole and current sent size
    int whole = file_obj->get_size();
    int cur = file_obj->get_position();
    
    // set the progress
    buddy->wnd_chat.set_progress(cur, whole);
    
    // free the file object, if it's done
    if (cur >= whole)
    {
	// notice it's done
	string notice("The file \"");
	buddy->wnd_chat.add_notice(notice + file_obj->get_name() + "\" is successfully sent!");

	delete file_obj;
	file_obj = NULL;
	
	// enable file sending button
	buddy->wnd_chat.enable_file_button(true);
    }
    
    // send the next part
    if (!send_file_binary())
	escape_file();
    
    return true;
}

/*
 * handles file escape.
 */
bool gmsgr_file_mgr::handle_file_escape()
{
    if (!file_obj)
    {
#ifdef _DEBUG
	cerr << "[Error] File object is not allocated, when the file escape comes!" << endl;
#endif // _DEBUG
	return false;
    }
    
    // free the file object
    delete file_obj;
    file_obj = NULL;
    
    // enable file sending button
    buddy->wnd_chat.enable_file_button(true);
    
    // notice it's escaped
    buddy->wnd_chat.add_notice("File transferring is canceled by the buddy!");
    
    return true;
}    


// -- methods --

/*
 * answers the file question.
 */
bool gmsgr_file_mgr::answer_file(GMSGR_FILE_ASW _asw, string _id)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_FILE_ASW;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_FILE_ASW) + (GMSGR_MAX_ID+1);
    
    // set the packet body
    memcpy(pkt.body, &_asw, sizeof(GMSGR_FILE_ASW));
    strncpy(pkt.body + sizeof(GMSGR_FILE_ASW), _id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

/*
 * acknowledge that binary is received successfully
 */
bool gmsgr_file_mgr::acknowledge_file()
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_FILE_ACK;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, buddy->id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
	return false;

#ifdef _DEBUG    
    cout << "[Notice] File acknowledge is sent to the buddy[" << buddy->id << "]!" << endl;
#endif // _DEBUG
    
    return true;
}

/*
 * sends a part of file binary to the buddy.
 */
bool gmsgr_file_mgr::send_file_binary()
{
    if (!file_obj)
	return false;
    
    gmsgr_packet pkt;
    
    // get whole and current sent file size
    int whole = file_obj->get_size();
    if (whole <= 0)
	return false;
    int cur = file_obj->get_position();
    if (cur == -1)
	return false;
    
    // get part size to send
    int part = (whole - cur < GMSGR_MAX_PACKET_FILE) ? (whole - cur) : GMSGR_MAX_PACKET_FILE;
    
    // set the packet head
    pkt.head.type = PACKET_FILE_BIN;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + sizeof(whole) + sizeof(cur) + part;
    
    // set the packet body
    strncpy(pkt.body, buddy->id.c_str(), GMSGR_MAX_ID);
    memcpy(pkt.body + (GMSGR_MAX_ID+1), &whole, sizeof(whole));
    memcpy(pkt.body + (GMSGR_MAX_ID+1) + sizeof(whole), &cur, sizeof(cur));
    if (file_obj->read(pkt.body + (GMSGR_MAX_ID+1) + sizeof(whole) + sizeof(cur), part) != part)
	return false;
   
    // add current file size
    cur += part;
        
    // send the packet
    if (!buddy->clnt->mgr_transmission->send(&pkt))
	return false;
    
#ifdef _DEBUG    
    cout << "[Notice] File binary is sent to the buddy[" << buddy->id << "]: (" 
	<< file_obj->get_name() << "/" << parse_file_size(cur) << ")" << endl;
#endif // _DEBUG
    
    return true;
}

// rounds up a number.
inline double round(double _val, int _pos) {
    double tmp = pow10(_pos-1);
    _val *= tmp;
    _val = floor(_val+0.5);
    return (_val/tmp);
}

/*
 * parses the size to a string.
 */
string gmsgr_file_mgr::parse_file_size(int size)
{
    char str_size[1024];
    
    // set the string by the size
    if (size < 1024)
	sprintf(str_size, "%db", size);
    else if (size < 1024*1024)
	sprintf(str_size, "%fkb", round((double)size/1024, 2));
    else if (size < 1024*1024*1024)
	sprintf(str_size, "%fmb", round((double)size/(1024*1024), 2));
    else
	sprintf(str_size, "%fgb", round((double)size/(1024*1024*1024), 2));
    
    // remove '0's
    if (size >= 1024)
    {
	int len = strlen(str_size);
	for (int i = len-3; i > 0; i--)
	{
	    if (str_size[i] == '0' && str_size[i-1] != '0')
	    {
		if (str_size[i-1] == '.')
		    strcpy(&str_size[i+1], &str_size[len-2]);
		else
		    strcpy(&str_size[i], &str_size[len-2]);
		break;
	    }
	}
    }
    
    return str_size;
}
