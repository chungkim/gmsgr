#include <iostream>
using namespace std;

#include "gmsgr_buddy.h"

// -- construct, destruct --

// constructor
gmsgr_buddy::gmsgr_buddy(string _id, p_gmsgr_client _clnt)
:   clnt(_clnt),    // g messenger client	
    id(_id),	    // buddy id
    mgr_chat(NULL), // chat manager
    mgr_file(NULL), // file manager
    wnd_chat(this)  // chat window
{
    // allocate chat manager
    mgr_chat = new gmsgr_chat_mgr(this); 
    if (!mgr_chat)
    {
#ifdef _DEBUG
	cerr << "[Error] Not enough memory for chat manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // allocate file manager
    mgr_file = new gmsgr_file_mgr(this); 
    if (!mgr_file)
    {
#ifdef _DEBUG
	cerr << "[Error] Not enough memory for file manager!" << endl;
#endif // _DEBUG
	exit(1);
    }    
}

// destructor
gmsgr_buddy::~gmsgr_buddy()
{
    // free file manager
    if (mgr_file)
	delete mgr_file;

    // free chat manager
    if (mgr_chat)
	delete mgr_chat;
}


// -- properties --

/*
 * returns buddy id.
 */
string gmsgr_buddy::get_id()
{
    return id;
}

/*
 * returns chat manager.
 */
p_gmsgr_chat_mgr gmsgr_buddy::get_chat_manager()
{
    return mgr_chat;
}

/*
 * returns file manager.
 */
p_gmsgr_file_mgr gmsgr_buddy::get_file_manager()
{
    return mgr_file;
}


// -- operations --


// -- methods --

