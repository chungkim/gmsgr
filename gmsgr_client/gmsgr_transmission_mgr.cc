#include <gtkmm.h>
#include <iostream>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_transmission_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_transmission_mgr::gmsgr_transmission_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt),    // g messenger client
    thrd_recv(0),   // receiving thread
    thrd_on(false)  // whether receiving thread is on or off
{
    
}

// destructor
gmsgr_transmission_mgr::~gmsgr_transmission_mgr()
{
    // stop transmission manager
    stop();
}


// -- operations --

/*
 * runs transmission manager to receive packets from the server.
 */
void gmsgr_transmission_mgr::run()
{
    // set thread on
    thrd_on = true;
    
    // create the receiving thread
    if (pthread_create(&thrd_recv, NULL, thread_recv, this))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to create receiving thread!" << endl;
#endif // _DEBUG	
	exit(1);
    }
}

/*
 * stops transmission manager
 */
void gmsgr_transmission_mgr::stop()
{
    // stop receiving thread
    if (thrd_recv)
    {
	// set thread off
	thrd_on = false;
	
	// wait for finishing the thread
	pthread_join(thrd_recv, NULL);
	
	// set thread invalid
	thrd_recv = 0;
    }
}

/*
 * receives a packet from the server.
 */
bool gmsgr_transmission_mgr::recv(p_gmsgr_packet _pkt)
{
    // receive the head of a packet
    if (clnt->sock.recv(&_pkt->head, GMSGR_SIZEOF_PACKET_HEAD) != GMSGR_SIZEOF_PACKET_HEAD) 
	return false;
    
    // receive the body of the packet
    if (_pkt->head.size - GMSGR_SIZEOF_PACKET_HEAD > 0)
    {
	if (clnt->sock.recv(&_pkt->body, _pkt->head.size - GMSGR_SIZEOF_PACKET_HEAD) != _pkt->head.size - GMSGR_SIZEOF_PACKET_HEAD) 
	    return false;
    }

#ifdef _DEBUG	
    // notice a packet is received
    cout << "[Notice] A packet is received: type(" << _pkt->head.type << ")" << endl;
#endif // _DEBUG

    return true;
}
    
/*
 * sends a packet to the server.
 */
bool gmsgr_transmission_mgr::send(p_gmsgr_packet _pkt)
{
    // send the packet
    if (clnt->sock.send(_pkt, _pkt->head.size) != _pkt->head.size)
	return false;
    
#ifdef _DEBUG	
    // notice a packet is sent
    cout << "[Notice] A packet is sent: type(" << _pkt->head.type << ")" << endl;
#endif // _DEBUG

    return true;
}


// -- methods --

/*
 * waits and receives packets from the server.
 */
#define this p
void* gmsgr_transmission_mgr::thread_recv(void* _arg)
{
    p_gmsgr_transmission_mgr p = (p_gmsgr_transmission_mgr)_arg;

    // loop for receiving packets
    while (this->thrd_on)
    {
	// allocate a packet
	p_gmsgr_packet pkt = new gmsgr_packet;
	if (!pkt)
	{
#ifdef _DEBUG	
	    cerr << "[Error] Not enough memory for a packet!" << endl;
#endif // _DEBUG	
	    continue;
	}
	
	// receive a packet
	if (!this->recv(pkt))
	{
	    // free the packet
	    delete pkt;

	    // quit if it's disconnected by the server
	    if (this->thrd_on)
	    {
#ifdef _DEBUG	
		cout << "[Warning] Disconnected by the server!" << endl;
#endif // _DEBUG	
		exit(1);
	    }
	    
	    // stop g messenger client
	    this->clnt->stop();

	    continue;
	}
	
	// add a packet into the packet manager 
	if (!this->clnt->mgr_packet->add_packet(pkt))
	{
	    // free the packet
	    delete pkt;

#ifdef _DEBUG	
	    cerr << "[Error] Fail to add a packet into the packet manager!" << endl;
#endif // _DEBUG	
	    exit(1);
	}	
    }
    
    return NULL;
}
#undef this
