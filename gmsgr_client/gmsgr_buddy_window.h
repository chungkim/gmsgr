// 
// File:   gmsgr_buddy_window.h
// Author: William Kim
//
// Created on May 19, 2007, 11:36 PM
//

#ifndef _gmsgr_buddy_window_H
#define	_gmsgr_buddy_window_H

#include <string>
#include <gtkmm.h>

typedef class gmsgr_buddy_mgr* p_gmsgr_buddy_mgr;

/*
 * g messenger buddy find/add window
 */
class gmsgr_buddy_window : public Gtk::Window
{
private:
    // -- fields --
    
    p_gmsgr_buddy_mgr	mgr_buddy;	// buddy manager (passed by gmsgr_buddy_mgr)
    
    Gtk::VBox		vbox_hboxes;	// vertical box for all horizontal boxes
    
    Gtk::HBox		hbox_id;	// horizontal box for buddy id
    Gtk::Label		label_id;	// label for buddy id
    Gtk::Entry		entry_id;	// entry for buddy id
    
    Gtk::HBox		hbox_btns;	// horizontal box for buttons
    Gtk::Button		btn_find;	// find button
    Gtk::Button		btn_add;	// add button
    Gtk::Button		btn_cancel;	// cancel button
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_buddy_window(p_gmsgr_buddy_mgr _mgr_buddy);
    
    // destructor
    ~gmsgr_buddy_window();
    
    
    // -- operations --
    
    /*
     * focuses on id entry.
     */
    void focus_id_entry();

    /*
     * clears id entry.
     */
    void clear_id_entry();
    
    /*
     * enables or disables id entry.
     */
    void enable_id_entry(bool _enable);
    
    /*
     * returns the id on id entry.
     */
    std::string get_id(); 
    
    /*
     * enables or disables find button.
     */
    void enable_find_button(bool _enable);
    
    /*
     * focuses on add button
     */
    void focus_add_button();
    
    /*
     * enables or disables add button.
     */
    void enable_add_button(bool _enable);

    
private:
    // -- methods --        

    /*
     * handles the show message.
     */
    void on_window_show();
    
    /*
     * handles the hide message.
     */
    void on_window_hide();

    /*
     * handles the clicked signal on find button.
     */
    void on_find_btn_clicked();
    
    /*
     * handles the clicked signal on add button.
     */
    void on_add_btn_clicked();
    
    /*
     * handles the clicked signal on cancel button.
     */
    void on_cancel_btn_clicked();
    
    /*
     * handles the key release signal on id entry.
     */
    bool on_entry_key_release(GdkEventKey* event);
};

typedef gmsgr_buddy_window* p_gmsgr_buddy_window;

#endif	/* _gmsgr_buddy_window_H */

