// 
// File:   gmsgr_packet_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 4:00 PM
//

#ifndef _gmsgr_packet_mgr_H
#define	_gmsgr_packet_mgr_H

#include <gtkmm.h>

#include "infinite_queue.h"
#include "gmsgr_packet.h"

typedef class gmsgr_client* p_gmsgr_client;

#define GMSGR_PATH_PACKET   "packet.ini"    // packet manager information file name

/*
 * g messenger packet manager
 */
class gmsgr_packet_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_client	    clnt;		// g messenger client (passed from gmsgr_client)

    int				    pkt_interval;	// packet transaction interval
    
    infinite_queue<p_gmsgr_packet>  pkt_queue;		// packet queue
    sigc::connection		    pkt_timer;		// packet transaction timer
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_packet_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_packet_mgr();
    
    
    // -- operations --
    
    /*
     * runs packet manager to transact packets from the server.
     */
    void run();
    
    /*
     * stops packet manager.
     */
    void stop();
    
    /*
     * adds a packet into the packet queue.
     */
    bool add_packet(p_gmsgr_packet _pkt);
    
    
private:    
    // -- methods --        
    
    /*
     * transacts a g messenger packet.
     */
    bool transact(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet JOIN_RSL.
     */
    bool transact_join_result(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet LEAVE_RSL.
     */
    bool transact_leave_result(p_gmsgr_packet _pkt);

    /*
     * transacts the packet LOGIN_RSL.
     */
    bool transact_login_result(p_gmsgr_packet _pkt);

    /*
     * transacts the packet BUDDY_LOGIN.
     */
    bool transact_buddy_login(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet BUDDY_LOGOUT.
     */
    void transact_buddy_logout(p_gmsgr_packet _pkt);

    /*
     * transacts the packet BUDDY_FIND_RSL.
     */
    bool transact_buddy_find_result(p_gmsgr_packet _pkt);

    /*
     * transacts the packet BUDDY_ADD_QST.
     */
    bool transact_buddy_add_question(p_gmsgr_packet _pkt);

    /*
     * transacts the packet BUDDY_ADD_RSL.
     */
    bool transact_buddy_add_result(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet BUDDY_DEL_RSL.
     */
    bool transact_buddy_delete_result(p_gmsgr_packet _pkt);

    /*
     * transacts the packet CHAT_MSG.
     */
    bool transact_chat_message(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_QST.
     */
    bool transact_file_question(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_RSL.
     */
    bool transact_file_result(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_BIN.
     */
    bool transact_file_binary(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_ACK.
     */
    bool transact_file_acknowledge(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_ESC.
     */
    bool transact_file_escape(p_gmsgr_packet _pkt);
    
    /*
     * gets packets from the packet queue, transacts them. 
     */
    bool on_packet_received(int timer_number);

    /*
     * reads the packet manager informaion from the file.
     */
    bool get_packet_info(int& _interval);    
};

typedef gmsgr_packet_mgr* p_gmsgr_packet_mgr;

#endif	/* _gmsgr_packet_mgr_H */

