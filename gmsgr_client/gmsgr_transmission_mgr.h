// 
// File:   gmsgr_transmission_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 3:36 PM
//

#ifndef _gmsgr_transmission_mgr_H
#define	_gmsgr_transmission_mgr_H

#include <pthread.h>

#include "client_socket.h"
#include "gmsgr_connection_mgr.h"
#include "gmsgr_packet_mgr.h"
#include "gmsgr_packet.h"

typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger transmission manager
 */
class gmsgr_transmission_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_client	clnt;		// g messenger client (passed from gmsgr_client)
    
    pthread_t			thrd_recv;	// receiving thread (posix)
    bool			thrd_on;	// whether receiving thread is on or off

    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_transmission_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_transmission_mgr();
    
    
    // -- operations --
    
    /*
     * runs transmission manager to receive packets from the server.
     */
    void run();
    
    /*
     * stops transmission manager.
     */
    void stop();

    /*
     * receives a packet from the server.
     */
    bool recv(p_gmsgr_packet _pkt);
    
    /*
     * sends a packet to the server.
     */
    bool send(p_gmsgr_packet _pkt);
    

private:    
    // -- methods --        
    
    /*
     * waits and receives packets from the server.
     */
    static void* thread_recv(void* _arg);
};

typedef gmsgr_transmission_mgr* p_gmsgr_transmission_mgr;

#endif	/* _gmsgr_transmission_mgr_H */

