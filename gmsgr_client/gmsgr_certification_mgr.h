// 
// File:   gmsgr_certification_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 3:32 PM
//

#ifndef _gmsgr_certification_mgr_H
#define	_gmsgr_certification_mgr_H

#include <string>

#include "gmsgr_protocol.h"
#include "gmsgr_packet_type.h"
#include "gmsgr_transmission_mgr.h"
#include "gmsgr_main_window.h"
#include "gmsgr_login_window.h"

/*
 * g messenger certification manager
 */
class gmsgr_certification_mgr
{
private:
    // -- fields --

    const p_gmsgr_client	clnt;		    // g messenger client (passed from gmsgr_client)

    gmsgr_login_window		wnd_login;	    // login window
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_certification_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_certification_mgr();
    
    
    // -- operations --
    
    /*
     * runs certification manager to join or login.
     */
    void run();  
    
    /*
     * stops certification manager.
     */
    void stop();
    
    /*
     * applies to leave the user.
     */
    bool apply_leave();

    /*
     * logout the user immediately.
     */
    void logout();

    /*
     * handles join result.
     */
    bool handle_join_result(GMSGR_JOIN_RSL _rsl);

    /*
     * handles leave result.
     */
    bool handle_leave_result(GMSGR_LEAVE_RSL _rsl);
    
    /*
     * handles login result
     */
    bool handle_login_result(GMSGR_LOGIN_RSL _rsl);

    
private:    
    // -- methods --        
  
    /*
     * applies to join as a new user.
     */
    bool apply_join(std::string _id, std::string _pwd);
    
    /*
     * applies to login the user.
     */
    bool apply_login(std::string _id, std::string _pwd);

    
    // -- aggregation --
    
    friend class gmsgr_login_window;	// login window
};

typedef gmsgr_certification_mgr* p_gmsgr_certification_mgr;

#endif	/* _msgr_certification_H */

