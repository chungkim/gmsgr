// 
// File:   gmsgr_connection_mgr.h
// Author: William Kim
//
// Created on May 6, 2007, 3:31 PM
//

#ifndef _gmsgr_connection_mgr_H
#define	_gmsgr_connection_mgr_H

#include <string>

#include "gmsgr_connect_window.h"

typedef class gmsgr_client* p_gmsgr_client;

#define GMSGR_PATH_CONNECT  "connect.ini"   // connection information file name

/*
 * g messenger connection manager
 */
class gmsgr_connection_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_client    clnt;	// g messenger client (passed from gmsgr_client)

    std::string		    serv_ip;	// server ip
    unsigned short	    serv_port;	// server port number
    int			    conn_sec;	// connection time (second)
    
    sigc::connection	    timer_wait;	// waiting timer for connection
    
    gmsgr_connect_window    wnd_conn;	// connection window
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_connection_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_connection_mgr();
    
    
    // -- operations --
    
    /*
     * runs connection manager to connect to the server.
     */
    void run();
    
    /*
     * stops connection manager to disconnect from the server.
     */
    void stop();
    
    /*
     * indicates whether the connected.
     */
    bool is_connected();
    
    
private:    
    // -- methods --        
    
    /*
     * reads the server informaion from the file.
     */
    bool get_server_info(std::string& _ip, unsigned short& _port, int& _time);    
    
    /*
     * connects to the server.
     */
    static void* thread_connect(void* _arg);

    /*
     * waits for the connection time, and closes the connection.
     */
    bool on_timeout_wait();

    /*
     * closes the connection.
     */
    bool on_timeout_close();

    /*
     * is called when the connection is succeeded.
     */
    bool on_timeout_open();

    
    // -- aggregation -- 
    
    friend class gmsgr_connect_window;	// connect window
};

typedef gmsgr_connection_mgr* p_gmsgr_connection_mgr;

#endif	/* _gmsgr_connection_mgr_H */

