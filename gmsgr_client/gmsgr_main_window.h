// 
// File:   gmsgr_main_window.h
// Author: William Kim
//
// Created on May 5, 2007, 3:51 PM
//

#ifndef _gmsgr_main_window_H
#define	_gmsgr_main_window_H

#include <string>
#include <gtkmm.h>

typedef class gmsgr_client* p_gmsgr_client;

#define GMSGR_PATH_ICON		"gmsgr.png"
#define GMSGR_BUDDY_TREE_COL	"Buddies online"

/*
 * g messenger main window
 */
class gmsgr_main_window : public Gtk::Window
{
private:
    // -- inner types --
    
    // column model for buddy tree view
    struct ModelColumns : Gtk::TreeModel::ColumnRecord {
	Gtk::TreeModelColumn<Glib::ustring> id;	    // buddy id
	ModelColumns() { add(id); }
    };
    
    
    // -- fields --
    
    p_gmsgr_client		    clnt;	    // g messenger client (passed from gmsgr_client)

    Gtk::VBox			    vbox_all;	    // vertical box for a all widgets

    Glib::RefPtr<Gtk::UIManager>    ui_mgr;	    // ui manager for menu and menu bar
    Glib::RefPtr<Gtk::ActionGroup>  act_grp;	    // action group for menu and menu bar
    Gtk::Widget*		    mi_remove;	    // buddy remove menu item
    Gtk::Widget*		    mi_msg;	    // send message menu item
    Gtk::Widget*		    mi_file;	    // send file menu item
    Gtk::Widget*		    ti_remove;	    // buddy remove tool bar item
    Gtk::Widget*		    ti_msg;	    // send message tool bar item
    Gtk::Widget*		    ti_file;	    // send file tool bar item
    
    Gtk::ScrolledWindow		    scrl_buddy;	    // scroll window for buddy tree view
    Gtk::TreeView		    tv_buddy;	    // tree view for buddies
    Glib::RefPtr<Gtk::ListStore>    tm_buddy;	    // tree model for buddy tree view
    ModelColumns		    cols_buddy;	    // column model for buddy tree view
    Gtk::Menu			    menu_buddy;	    // pop up menu for buddy tree view
    Gtk::TreeModel::Row		    sel_row_buddy;  // currently selected row on the buddy tree view
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_main_window(p_gmsgr_client _clnt);
    
    // destructor
    ~gmsgr_main_window();
    
    
    // -- operations --
    
    /*
     * enables window, data.
     */
    void enable(std::string _user_id);
    
    /*
     * disables window, data.
     */
    void disable();
    
    /*
     * adds a buddy into the tree view.
     */
    bool add_buddy(Glib::ustring _id);
    
    /*
     * removes a buddy from the tree view.
     */
    void remove_buddy(Glib::ustring _id);
    
    /*
     * indicates whether the buddy is in the tree view.
     */
    bool is_buddy_in(Glib::ustring _id);
    
    
private:
    // -- methods --        
    
    /*
     * enables or disables menu items related buddy count.
     */
    void enable_buddy_items(bool _enable);    
    
    /*
     * get currently selected row.
     * returns false, if no row is selected.
     */
    bool get_selected_row(Gtk::TreeModel::Row& _row);
    
    /*
     * clears buddy tree view.
     */
    void clear_buddies();
    
    /*
     * handles the show message.
     */
    void on_window_show();

    /*
     * handles the hide message.
     */
    void on_window_hide();

    /*
     * handles the file|logout menu item.
     */
    void on_menu_file_logout();
    
    /*
     * handles the file|leave menu item.
     */
    void on_menu_file_leave();

    /*
     * handles the quit menu item.
     */
    void on_menu_file_quit();

    /*
     * handles the buddy|add menu item.
     */
    void on_menu_buddy_add();
    
    /*
     * handles the buddy|remove menu item.
     */
    void on_menu_buddy_remove();
    
    /*
     * handles the action|message menu item.
     */
    void on_menu_action_message();
    
    /*
     * handles the action|file menu item.
     */
    void on_menu_action_file();

    /*
     * handles the help|about menu item.
     */
    void on_menu_help_about();
    
    /*
     * handles the button release on buddy tree view.
     */
    bool on_buddy_tree_btn_release(GdkEventButton* _event);
    
    /*
     * handles the row activated on buddy tree view.
     */
    void on_buddy_tree_row_activated(const Gtk::TreeModel::Path& _path, Gtk::TreeViewColumn* _col);
};

typedef gmsgr_main_window* p_gmsgr_main_window;

#endif	/* _gmsgr_main_window_H */

