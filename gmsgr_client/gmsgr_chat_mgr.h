// 
// chat:   gmsgr_chat_mgr.h
// Author: William Kim
//
// Created on May 21, 2007, 5:09 PM
//

#ifndef _gmsgr_chat_mgr_H
#define	_gmsgr_chat_mgr_H

#include <string>

typedef class gmsgr_buddy* p_gmsgr_buddy;

/*
 * g messenger chat manager
 */
class gmsgr_chat_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_buddy	    buddy;  // g messenger buddy (passed from gmsgr_buddy)

    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_chat_mgr(p_gmsgr_buddy _buddy);
    
    // destructor
    ~gmsgr_chat_mgr();
    
    
    // -- operations --
    
    /*
     * runs chat manager.
     */
    void run();
    
    /*
     * gives a message to the buddy.
     */
    bool give_message(std::string _msg);
    
    /*
     * takes a message from the buddy.
     */
    void take_message(std::string _msg);
    
    
private:
    // -- methods --        
    
    
};

typedef gmsgr_chat_mgr* p_gmsgr_chat_mgr;

#endif	/* _gmsgr_chat_mgr_H */

