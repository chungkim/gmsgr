// 
// File:   gmsgr_file_mgr.h
// Author: William Kim
//
// Created on May 21, 2007, 5:10 PM
//

#include <string>

#include "file.h"
#include "gmsgr_packet_type.h"

#ifndef _gmsgr_file_mgr_H
#define	_gmsgr_file_mgr_H

typedef class gmsgr_buddy* p_gmsgr_buddy;

/*
 * g messenger file manager
 */
class gmsgr_file_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_buddy	    buddy;	// g messenger buddy (passed from gmsgr_buddy)

    p_file		    file_obj;	// file object
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_file_mgr(p_gmsgr_buddy _buddy);
    
    // destructor
    ~gmsgr_file_mgr();
    
    
    // -- operations --
    
    /*
     * runs file manager.
     */
    void run();
    
    /*
     * applies sending file to the buddy.
     */
    bool apply_file(std::string _path);
    
    /*
     * escapes(cancels) file transferring.
     */
    bool escape_file();
    
    /*
     * handles file question.
     */
    bool handle_file_question(std::string _name, int _size);

    /*
     * handles file result.
     */
    bool handle_file_result(GMSGR_FILE_RSL _rsl);
    
    /*
     * handles file binary.
     */
    bool handle_file_binary(int _whole, int _cur, char* _bin);

    /*
     * handles file acknowledge.
     */
    bool handle_file_acknowledge();
    /*
     * handles file escape.
     */
    bool handle_file_escape();

    
private:
    // -- methods --        
    
    /*
     * answers the file question.
     */
    bool answer_file(GMSGR_FILE_ASW _asw, std::string _id);

    /*
     * sends a part of file binary to the buddy.
     */
    bool send_file_binary();
    
    /*
     * acknowledge that binary is received successfully
     */
    bool acknowledge_file();    

    /*
     * parses the size to a string.
     */
    std::string parse_file_size(int size);
};

typedef gmsgr_file_mgr* p_gmsgr_file_mgr;

#endif	/* _gmsgr_file_mgr_H */

