// 
// File:   gmsgr_buddy_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 3:37 PM
//

#ifndef _gmsgr_buddy_mgr_H
#define	_gmsgr_buddy_mgr_H

#include <string>
#include <map>

#include "gmsgr_packet.h"
#include "gmsgr_buddy.h"
#include "gmsgr_buddy_window.h"

typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger buddy manager
 */
class gmsgr_buddy_mgr
{
private:
    // -- inner type --
    
    // buddy id - object table
    typedef std::map<std::string, p_gmsgr_buddy> map_buddy;
    
    
    // -- fields --
    
    const p_gmsgr_client    clnt;	// g messenger client (passed from gmsgr_client)

    map_buddy		    buddy_tbl;	// logined buddy table

    gmsgr_buddy_window	    wnd_buddy;	// buddy find/add window
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_buddy_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_buddy_mgr();
    
    
    // -- operations --
    
    /*
     * runs buddy manager.
     */
    void run(int _main_x, int _main_y, int _main_w, int _main_h);
    
    /*
     * stops buddy manager.
     */
    void stop();
    
    /*
     * returns a buddy by the id.
     */
    p_gmsgr_buddy get_buddy(std::string _id);
    
    /*
     * handles buddy login.
     */
    bool handle_buddy_login(std::string _id);
    
    /*
     * handles buddy logout.
     */
    void handle_buddy_logout(std::string _id);

    /*
     * handles buddy find result.
     */
    bool handle_buddy_find_result(GMSGR_BUDDY_FIND_RSL _rsl);
    
    /*
     * handles buddy add question.
     */
    bool handle_buddy_add_question(std::string _id);

    /*
     * handles buddy add result.
     */
    bool handle_buddy_add_result(GMSGR_BUDDY_ADD_RSL _rsl);

    /*
     * handles buddy delete result.
     */
    bool handle_buddy_delete_result(GMSGR_BUDDY_DEL_RSL _rsl);
    
    /*
     * applies to delete a buddy.
     */
    bool apply_delete_buddy(std::string _id);

    
private:    
    // -- methods --        
    
    /*
     * applies to find a buddy.
     */
    bool apply_find_buddy(std::string _id);
    
    /*
     * applies to add a buddy.
     */
    bool apply_add_buddy(std::string _id);

    /*
     * answers the buddy add question.
     */
    bool answer_add_buddy(GMSGR_BUDDY_ADD_ASW _asw, std::string _id);

    
    // -- aggregation --
    
    friend class gmsgr_buddy_window;	// buddy find/search window
};

typedef gmsgr_buddy_mgr* p_gmsgr_buddy_mgr;

#endif	/* _gmsgr_buddy_mgr_H */

