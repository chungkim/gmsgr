#include <gtkmm.h>
#include <iostream>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_buddy.h"
#include "gmsgr_main_window.h"

// constructor
gmsgr_main_window::gmsgr_main_window(p_gmsgr_client _clnt)
:   clnt(_clnt)	    // g messenger client
{
    set_size_request(260, 320);
    set_border_width(5);
    // set_position(WIN_POS_CENTER);
    set_resizable(false);
    if (!set_icon_from_file(GMSGR_PATH_ICON))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to set icon for main window!" << endl;
#endif // _DEBUG	
	exit(1);
    }
    
    signal_show().connect( sigc::mem_fun(*this, &gmsgr_main_window::on_window_show) );
    signal_hide().connect( sigc::mem_fun(*this, &gmsgr_main_window::on_window_hide) );

    // create and actions for menus and toolbars
    act_grp = ActionGroup::create();

    // set file menu
    act_grp->add( Action::create("FileMenu", "_File"), AccelKey("<alt>F") );
    act_grp->add( Action::create("FileLogout", "Log_out"), AccelKey("<ctrl>O"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_file_logout) );
    act_grp->add( Action::create("FileLeave", "_Leave"), AccelKey("<ctrl>L"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_file_leave) );
    act_grp->add( Action::create("FileQuit", Stock::QUIT), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_file_quit) );
    
    // set buddy menu
    act_grp->add( Action::create("BuddyMenu", "_Buddy"), AccelKey("<alt>B") );
    act_grp->add( Action::create("BuddyAdd", Stock::ADD), AccelKey("<ctrl>A"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_buddy_add) );
    act_grp->add( Action::create("BuddyRemove", Stock::REMOVE), AccelKey("<ctrl>R"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_buddy_remove) );
    
    // set action menu
    act_grp->add( Action::create("ActionMenu", "_Action"), AccelKey("<alt>A") );
    act_grp->add( Action::create("ActionMessage", "Send _Message"), AccelKey("<ctrl>M"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_action_message) );
    act_grp->add( Action::create("ActionFile", "Send _File"), AccelKey("<ctrl>F"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_action_file) );

    // set help menu
    act_grp->add( Action::create("HelpMenu", "_Help"), AccelKey("<alt>H") );
    act_grp->add( Action::create("HelpAbout", "about G Messenger Client"), 
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_help_about) );
    
    // create and set ui manager for menus and toolbars
    ui_mgr = UIManager::create();
    ui_mgr->insert_action_group(act_grp);
    add_accel_group(ui_mgr->get_accel_group());
    
    // layout the actions in a memubar and toolbar
    Glib::ustring ui_info = 
	    "<ui>"
	    "  <menubar name='MenuBar'>"
	    "    <menu action='FileMenu'>"
	    "      <menuitem action='FileLogout'/>"
	    "      <menuitem action='FileLeave'/>"
	    "      <separator/>"
	    "      <menuitem action='FileQuit'/>"
	    "    </menu>"
	    "    <menu action='BuddyMenu'>"
	    "      <menuitem action='BuddyAdd'/>"
	    "      <menuitem action='BuddyRemove'/>"
	    "    </menu>"
	    "    <menu action='ActionMenu'>"
	    "      <menuitem action='ActionMessage'/>"
	    "      <menuitem action='ActionFile'/>"
	    "    </menu>"
	    "    <menu action='HelpMenu'>"
	    "      <menuitem action='HelpAbout'/>"
	    "    </menu>"
	    "  </menubar>"
	    "  <toolbar name='ToolBar'>"
	    "    <toolitem action='FileQuit'/>"
	    "    <separator/>"
	    "    <toolitem action='BuddyAdd'/>"
	    "    <toolitem action='BuddyRemove'/>"
	    "    <separator/>"
	    "    <toolitem action='ActionMessage'/>"
	    "    <toolitem action='ActionFile'/>"
	    "  </toolbar>"
	    "</ui>";
    
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    try
    {
	ui_mgr->add_ui_from_string(ui_info);
    }
    catch (const Glib::Error& ex)
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to build menus: " << ex.what() << endl;
#endif // _DEBUG
	exit(1);
    }
#else
    std::auto_ptr<Glib::Error> ex;
    ui_mgr->add_ui_from_string(ui_info, ex);
    if (ex.get())
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to build menus: " << ex->what() << endl;
#endif // _DEBUG
	exit(1);
    }
#endif //GLIBMM_EXCEPTIONS_ENABLED
    
    // get the menubar and toolbar widgets, and them to a container widget
    Widget* menubar = ui_mgr->get_widget("/MenuBar");
    if (!menubar)
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to get menu bar from ui manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    Widget* toolbar = ui_mgr->get_widget("/ToolBar");
    if (!toolbar)
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to get tool bar from ui manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // get all menu items related buddy count
    mi_remove = ui_mgr->get_widget("/MenuBar/BuddyMenu/BuddyRemove");
    mi_msg = ui_mgr->get_widget("/MenuBar/ActionMenu/ActionMessage");
    mi_file = ui_mgr->get_widget("/MenuBar/ActionMenu/ActionFile");
    ti_remove = ui_mgr->get_widget("/ToolBar/BuddyRemove");
    ti_msg = ui_mgr->get_widget("/ToolBar/ActionMessage");
    ti_file = ui_mgr->get_widget("/ToolBar/ActionFile");
    if (!mi_remove || !mi_msg || !mi_file || !ti_remove || !ti_msg || !ti_file)
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to get all menu items related buddy count from ui manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // set tree view for buddies
    tm_buddy = ListStore::create(cols_buddy);
    tv_buddy.set_model(tm_buddy);
    tv_buddy.append_column(GMSGR_BUDDY_TREE_COL, cols_buddy.id);
    tv_buddy.signal_button_release_event().connect( sigc::mem_fun(*this, &gmsgr_main_window::on_buddy_tree_btn_release) );
    tv_buddy.signal_row_activated().connect( sigc::mem_fun(*this, &gmsgr_main_window::on_buddy_tree_row_activated) );
    
    // fill and set pop up menu for buddy tree view
    Menu::MenuList& menulist = menu_buddy.items();
    menulist.push_back( Menu_Helpers::MenuElem("Send _Message",
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_action_message)) );
    menulist.push_back( Menu_Helpers::MenuElem("Send _File",
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_action_file)) );
    menulist.push_back( Menu_Helpers::MenuElem("_Remove",
	sigc::mem_fun(*this, &gmsgr_main_window::on_menu_buddy_remove)) );
    menu_buddy.accelerate(*this);
    
    // add buddy tree view into scrolled window
    scrl_buddy.add(tv_buddy);
    scrl_buddy.set_policy(POLICY_AUTOMATIC, POLICY_AUTOMATIC);
    
    // pack all the widgets into vertical box
    vbox_all.pack_start(*menubar, PACK_SHRINK);
    vbox_all.pack_end(*toolbar, PACK_SHRINK);
    vbox_all.pack_start(scrl_buddy);
    
    // add the vertical box to window
    add(vbox_all);
    
    // disable window first
    disable();

    show_all_children();    
}

// destructor
gmsgr_main_window::~gmsgr_main_window()
{
    
}


// -- operations --

/*
 * sets information.
 */
void gmsgr_main_window::enable(string _user_id)
{
    // set new title with the user id
    set_title(string("G Messenger - ") + _user_id);

    // enable window
    set_sensitive(true);    
}

/*
 * deletes information.
 */
void gmsgr_main_window::disable()
{
    // disable window
    set_sensitive(false);
    
    set_title("G Messenger Client");
    
    // clear buddy tree view
    clear_buddies();
}

/*
 * adds a buddy into the tree view.
 */
bool gmsgr_main_window::add_buddy(Glib::ustring _id)
{
    if (is_buddy_in(_id))
	return false;
    
    TreeModel::Row row = *(tm_buddy->append());
    row[cols_buddy.id] = _id;
 
    return true;
}

/*
 * removes a buddy from the tree view.
 */
void gmsgr_main_window::remove_buddy(Glib::ustring _id)
{
    TreeModel::Children children = tm_buddy->children();
    
    for (TreeModel::Children::iterator iter = children.begin(); iter != children.end(); iter++)
    {
	TreeModel::Row row = *iter;
	
	if (_id == row[cols_buddy.id])
	{
	    tm_buddy->erase(iter);
	    break;
	}
    }    
    
    // set current selected row and disable buddy items
    enable_buddy_items(get_selected_row(sel_row_buddy));
}
    
/*
 * indicates whether the buddy is in the tree view.
 */
bool gmsgr_main_window::is_buddy_in(Glib::ustring _id)
{
    TreeModel::Children children = tm_buddy->children();
    
    for (TreeModel::Children::iterator iter = children.begin(); iter != children.end(); iter++)
    {
	TreeModel::Row row = *iter;
	
	if (_id == row[cols_buddy.id])
	    return true;
    }
    
    return false;
}
    
    
// -- methods --

/*
 * enables or disables menu items related buddy count.
 */
void gmsgr_main_window::enable_buddy_items(bool _enable)
{
    mi_remove->set_sensitive(_enable);
    mi_msg->set_sensitive(_enable);
    mi_file->set_sensitive(_enable);
    ti_remove->set_sensitive(_enable);
    ti_msg->set_sensitive(_enable);
    ti_file->set_sensitive(_enable);
}

/*
 * get currently selected row.
 * returns false, if no row is selected.
 */
bool gmsgr_main_window::get_selected_row(Gtk::TreeModel::Row& _row)
{
    Glib::RefPtr<TreeView::Selection> sel = tv_buddy.get_selection();
    if (!sel)
	return false;
    
    TreeModel::iterator iter = sel->get_selected();
    if (!iter)
	return false;

    // set input row
    _row = *iter;
    
    return (_row);
}

/*
 * clears buddy tree view.
 */
void gmsgr_main_window::clear_buddies()
{
    TreeModel::Children children = tm_buddy->children();
    
    TreeModel::Children::iterator iter = children.begin();
    while (iter != children.end())
	iter = tm_buddy->erase(iter);
}
    
/*
 * handles the show message.
 */
void gmsgr_main_window::on_window_show()
{
    // enable window
    enable(clnt->get_id());

    // disable menu items related buddy count
    enable_buddy_items(false);
}

/*
 * handles the hide message.
 */
void gmsgr_main_window::on_window_hide()
{
    // disable window
    disable();
}

/*
 * handles the file|logout menu item.
 */
void gmsgr_main_window::on_menu_file_logout()
{
    // logout the user
    clnt->mgr_certification->logout();
}

/*
 * handles the file|leave menu item.
 */
void gmsgr_main_window::on_menu_file_leave()
{
    // apply to leave
    if (!clnt->mgr_certification->apply_leave())
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to apply leaving!" << endl;
#endif // _DEBUG
	exit(1);
    }
}

/*
 * handles the file|quit menu item.
 */
void gmsgr_main_window::on_menu_file_quit()
{
    hide();
}

/*
 * handles the buddy|add menu item.
 */
void gmsgr_main_window::on_menu_buddy_add()
{
    // get position and size
    int x, y, w, h;
    get_position(x, y);
    get_size(w, h);
    
    // run buddy manager
    clnt->mgr_buddy->run(x, y, w, h);
}

/*
 * handles the buddy|remove menu item.
 */
void gmsgr_main_window::on_menu_buddy_remove()
{
    // apply to delete the buddy, if a buddy is selected
    if (sel_row_buddy)
    {
#ifdef _DEBUG
	if (!clnt->mgr_buddy->apply_delete_buddy(sel_row_buddy.get_value(cols_buddy.id)))
	    cerr << "[Error] Fail to apply to delete a buddy!" << endl;
#else
	clnt->mgr_buddy->apply_delete_buddy(sel_row_buddy.get_value(cols_buddy.id));
#endif // _DEBUG	
    }
}

/*
 * handles the action|message menu item.
 */
void gmsgr_main_window::on_menu_action_message()
{
    // run a chat manager, if a buddy is selected
    if (sel_row_buddy) 
    {
	// get the buddy clicked
	p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(sel_row_buddy.get_value(cols_buddy.id));
	if (!buddy)
	{
#ifdef _DEBUG
	    cerr << "[Error] Fail to get a buddy!" << endl;
#endif // _DEBUG	
	    exit(1);
	}
	
	// run the chat manager of the buddy
	buddy->get_chat_manager()->run();
    }
}

/*
 * handles the action|file menu item.
 */
void gmsgr_main_window::on_menu_action_file()
{
    // run a file manager, if a buddy is selected
    if (sel_row_buddy) 
    {
	// get the buddy clicked
	p_gmsgr_buddy buddy = clnt->mgr_buddy->get_buddy(sel_row_buddy.get_value(cols_buddy.id));
	if (!buddy)
	{
#ifdef _DEBUG
	    cerr << "[Error] Fail to get a buddy!" << endl;
#endif // _DEBUG	
	    exit(1);
	}
	
	// run the file manager of the buddy
	buddy->get_file_manager()->run();
    }
}

/*
 * handles the help|about menu item.
 */
void gmsgr_main_window::on_menu_help_about()
{
    AboutDialog dialog;
 
    // set about dialog
    dialog.set_title("About");
    dialog.set_name("G Messenger Client");
    dialog.set_version("\nversion 1.0");
    dialog.set_comments("for GNOME Desktop");
    dialog.set_copyright("Copyright @ Boys. All rights Reserved.");
    dialog.set_license("\n\t - G Messenger Client for GNOME Desktop -\n\n"
	"This software is created by the project team \"Boys\".\n"
	"You can use this client for free.\n"
	"Please contact us by the e-mail, if you are interested in administrating the server.\n\n\n"
	"\t\t\t\t\t\t- written by William Kim");
    dialog.set_wrap_license(true);
    dialog.set_website("e-mail: lostinspace@india.com");
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    dialog.set_default_icon_from_file(GMSGR_PATH_ICON);
#else
    std::auto_ptr<Glib::Error> ex;
    dialog.set_default_icon_from_file(GMSGR_PATH_ICON, ex);
#endif // GLIBMM_EXCEPTIONS_ENABLED
    Glib::RefPtr<Gdk::Pixbuf> logo;
    dialog.set_logo(logo);
    
    dialog.run();
}

/*
 * handles the button press on buddy tree view.
 */
bool gmsgr_main_window::on_buddy_tree_btn_release(GdkEventButton* _event)
{
    if (_event->type == GDK_BUTTON_RELEASE) 
    {
	// set current selected row and enable buddy items
	enable_buddy_items(get_selected_row(sel_row_buddy));
	
	// is this right-clicked?
	if (_event->button == 3) 
	    menu_buddy.popup(_event->button, _event->time);
    }

    return true;
}

/*
 * handles the row activated on buddy tree view.
 */
void gmsgr_main_window::on_buddy_tree_row_activated(const Gtk::TreeModel::Path& _path, TreeViewColumn* _col)
{
    // set current selected row and enable buddy items
    enable_buddy_items(get_selected_row(sel_row_buddy));
	
    // run action|message
    on_menu_action_message();
}
