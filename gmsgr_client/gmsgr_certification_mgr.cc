#include <iostream>
#include <gtkmm/main.h>
using namespace std;
using namespace Gtk;

#include "sha1.h"
#include "gmsgr_client.h"
#include "gmsgr_packet.h"

// -- construct, destruct --

// constructor
gmsgr_certification_mgr::gmsgr_certification_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt),    // g messenger client
    wnd_login(this) // login window
{
}

// destructor
gmsgr_certification_mgr::~gmsgr_certification_mgr()
{
    // stops certification manager
    stop();
}


// -- operations --

/*
 * runs certification manager to join or login.
 */
void gmsgr_certification_mgr::run()
{
    // run the login window loop (never returns until the window hides)
    Main::run(wnd_login);    
}

/*
 * stops certification manager.
 */
void gmsgr_certification_mgr::stop()
{
    // hide login window
    wnd_login.hide();
    
    // clear user id
    clnt->id.clear();
}
   
/*
 * applies to leave the user.
 */
bool gmsgr_certification_mgr::apply_leave()
{
    if (clnt->id.empty())
	return false;
    
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_LEAVE_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, clnt->id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

/*
 * logout the user immediately.
 */
void gmsgr_certification_mgr::logout()
{
    // disable main window
    clnt->wnd_main.disable();
    
    // reset g messenger client
    clnt->reset();

    // enable main window
    clnt->wnd_main.enable(clnt->id);    
}

/*
 * handles join result.
 */
bool gmsgr_certification_mgr::handle_join_result(GMSGR_JOIN_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case JOIN_RSL_SUCCESS:
	    clnt->run_msg_dlg("You have successfully joined!\nPlease enter password again to log in.", &wnd_login, MESSAGE_INFO);
	    wnd_login.set_sensitive(true);
	    wnd_login.clear_pwd_entry();
	    wnd_login.focus_pwd_entry();
	    break;
	    
	case JOIN_RSL_FAIL_ID_EXIST: 
	    clnt->run_msg_dlg("The same ID already exists!\nPlease try again.", &wnd_login, MESSAGE_WARNING);
	    wnd_login.set_sensitive(true);
	    wnd_login.clear_pwd_entry();
	    wnd_login.focus_id_entry();
	    break;
    
	case JOIN_RSL_FAIL_UNKNWN: 
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &wnd_login, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
    
    return true;
}

/*
 * handles leave result.
 */
bool gmsgr_certification_mgr::handle_leave_result(GMSGR_LEAVE_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case LEAVE_RSL_SUCCESS:
	    clnt->run_msg_dlg("You have successfully left!\nPlease join to use G Messager again.", &clnt->wnd_main, MESSAGE_INFO);
	    logout();
	    break;
	    
	case LEAVE_RSL_FAIL_ID_INVLD:
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &clnt->wnd_main, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	case LEAVE_RSL_FAIL_UNKNWN:
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &clnt->wnd_main, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
    
    return true;    
}

/*
 * handles login result.
 */
bool gmsgr_certification_mgr::handle_login_result(GMSGR_LOGIN_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case LOGIN_RSL_SUCCESS:
	    clnt->id = wnd_login.get_id();
	    wnd_login.set_sensitive(true);
	    wnd_login.hide();
#ifdef _DEBUG
	    cout << "[Notice] Logined id: " << clnt->id << endl;
#endif // _DEBUG
	    break;
	    
	case LOGIN_RSL_FAIL_ID_INVLD:
	    clnt->run_msg_dlg("The id does not exist!\nPlease try again.", &wnd_login, MESSAGE_WARNING);
	    wnd_login.set_sensitive(true);
	    wnd_login.clear_pwd_entry();
	    wnd_login.focus_id_entry();
	    break;
	    
	case LOGIN_RSL_FAIL_PWD_INVLD:
	    clnt->run_msg_dlg("The password is invalid!\nPlease try again.", &wnd_login, MESSAGE_WARNING);
	    wnd_login.set_sensitive(true);
	    wnd_login.clear_pwd_entry();
	    wnd_login.focus_pwd_entry();
	    break;
	    
	case LOGIN_RSL_FAIL_UNKNWN:
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &wnd_login, MESSAGE_ERROR);
	    exit(1);
	    break;

	default:
	    return false;
    }
    
    return true;    
}

    
// -- methods --

/*
 * applies to join as a new user.
 */
bool gmsgr_certification_mgr::apply_join(string _id, string _pwd)
{
    // hash the password
    char hash[GMSGR_SIZEOF_PWD_HASH];
    CSHA1 sha1;
    sha1.Reset();
    sha1.Update((UINT_8*)_pwd.c_str(), _pwd.length());
    sha1.Final();
    sha1.GetHash((UINT_8*)hash);
    
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_JOIN_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + GMSGR_SIZEOF_PWD_HASH;
    
    // set the packet body
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
    memcpy(pkt.body + (GMSGR_MAX_ID+1), hash, GMSGR_SIZEOF_PWD_HASH);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
#ifdef _DEBUG    
    // notice the join apply packet is sent
    cout << "[Notice] Join apply packet is sent!" << endl;
#endif // _DEBUG    

    return true;
}

/*
 * applies to login the user.
 */
bool gmsgr_certification_mgr::apply_login(string _id, string _pwd)
{
    // hash the password
    char hash[GMSGR_SIZEOF_PWD_HASH];
    CSHA1 sha1;
    sha1.Reset();
    sha1.Update((UINT_8*)_pwd.c_str(), _pwd.length());
    sha1.Final();
    sha1.GetHash((UINT_8*)hash);

    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_LOGIN_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + GMSGR_SIZEOF_PWD_HASH;
    
    // set the packet body
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
    memcpy(pkt.body + (GMSGR_MAX_ID+1), hash, GMSGR_SIZEOF_PWD_HASH);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

