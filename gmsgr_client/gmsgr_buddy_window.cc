#include <iostream>
#include <gtkmm.h>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_buddy_mgr.h"
#include "gmsgr_buddy_window.h"

// -- construct, destruct --

// constructor
gmsgr_buddy_window::gmsgr_buddy_window(p_gmsgr_buddy_mgr _mgr_buddy)
:   mgr_buddy(_mgr_buddy),	// buddy manager
    label_id("ID:"),		// entry for id
    btn_find(Stock::FIND),	// find button
    btn_add(Stock::ADD),	// add button
    btn_cancel(Stock::CANCEL)	// cancel button
{
    // set window details
    set_default_size(350, 200);
    set_title("Buddy Find/Add");
    // set_position(WIN_POS_CENTER);
    set_resizable(false);
    set_border_width(5);
    if (!set_icon_from_file(GMSGR_PATH_ICON))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to set icon for buddy add/find window!" << endl;
#endif // _DEBUG	
	exit(1);
    }
    
    signal_show().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_window_show) );
    signal_hide().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_window_hide) );
    
    // set horizontal box for id
    entry_id.set_max_length(20);
    entry_id.signal_key_release_event().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_entry_key_release) );
    hbox_id.pack_start(label_id);
    hbox_id.pack_start(entry_id);
    hbox_id.set_spacing(5);
    hbox_id.set_border_width(5);
    
    // set horizontal box for buttons
    btn_find.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_find_btn_clicked) );
    btn_add.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_add_btn_clicked) );
    btn_cancel.signal_clicked().connect( sigc::mem_fun(*this, &gmsgr_buddy_window::on_cancel_btn_clicked) );
    hbox_btns.pack_start(btn_find, PACK_SHRINK);
    hbox_btns.pack_start(btn_add, PACK_SHRINK);
    hbox_btns.pack_start(btn_cancel, PACK_SHRINK);
    hbox_btns.set_spacing(5);
    hbox_btns.set_border_width(5);
    
    // set vertical box for hboxes
    vbox_hboxes.add(hbox_id);
    vbox_hboxes.add(hbox_btns);
    
    // add the vertical box into window
    add(vbox_hboxes);

    // show all widgets
    show_all_children();
}

// destructor
gmsgr_buddy_window::~gmsgr_buddy_window()
{
    
}


// -- operations --

/*
 * focuses on id entry.
 */
void gmsgr_buddy_window::focus_id_entry()
{
    set_focus(entry_id);
}

/*
 * clears id entry.
 */
void gmsgr_buddy_window::clear_id_entry()
{
    entry_id.set_text("");
}

/*
 * enables or disables id entry.
 */
void gmsgr_buddy_window::enable_id_entry(bool _enable)
{
    entry_id.set_sensitive(_enable);
}
    
/*
 * returns the id on id entry.
 */
string gmsgr_buddy_window::get_id()
{
    return entry_id.get_text();
}
    
/*
 * enables or disables find button.
 */
void gmsgr_buddy_window::enable_find_button(bool _enable)
{
    btn_find.set_sensitive(_enable);
}

/*
 * focuses on add button
 */
void gmsgr_buddy_window::focus_add_button()
{
    set_focus(btn_add);
}

/*
 * enables or disables add button.
 */
void gmsgr_buddy_window::enable_add_button(bool _enable)
{
    btn_add.set_sensitive(_enable);
}


// -- methods --

/*
 * handles the show message.
 */
void gmsgr_buddy_window::on_window_show()
{
    enable_id_entry(true);
    enable_find_button(true);
    enable_add_button(false);
    focus_id_entry();
}

/*
 * handles the show message.
 */
void gmsgr_buddy_window::on_window_hide()
{
    set_sensitive(true);
    clear_id_entry();
}

/*
 * handles the clicked signal on find button.
 */
void gmsgr_buddy_window::on_find_btn_clicked()
{
    // is id typed?
    if (!entry_id.get_text_length())
    {
	mgr_buddy->clnt->run_msg_dlg("Please enter a buddy id!", this, MESSAGE_WARNING);
	focus_id_entry();
	return;
    }
    
    // is id the same as user id?
    if (entry_id.get_text() == mgr_buddy->clnt->get_id())
    {
	mgr_buddy->clnt->run_msg_dlg("You entered your own id!\nPlease enter a buddy id.", this, MESSAGE_WARNING);
	clear_id_entry();
	focus_id_entry();
	return;
    }
    
    // disable window until the result comes
    set_sensitive(false);
    
    // apply to find the buddy
#ifdef _DEBUG
    if (!mgr_buddy->apply_find_buddy(entry_id.get_text()))
	cerr << "[Error] Fail to apply to find a buddy!" << endl;
#else
    mgr_buddy->apply_find_buddy(entry_id.get_text());
#endif // _DEBUG
}

/*
 * handles the clicked signal on add button.
 */
void gmsgr_buddy_window::on_add_btn_clicked()
{
    // disable window until the result comes
    set_sensitive(false);
    
    // apply to add the buddy
#ifdef _DEBUG
    if (!mgr_buddy->apply_add_buddy(entry_id.get_text()))
	cerr << "[Error] Fail to apply to add a buddy!" << endl;
#else
    mgr_buddy->apply_add_buddy(entry_id.get_text());
#endif // _DEBUG

}

/*
 * handles the clicked signal on cancel button.
 */
void gmsgr_buddy_window::on_cancel_btn_clicked()
{
    hide();
}

/*
 * handles the key release signal on id entry.
 */
bool gmsgr_buddy_window::on_entry_key_release(GdkEventKey* event)
{
    // click find button, if enter key is released
    if (event->keyval == 0xFF0D)
	btn_find.clicked();
    // hide window, if escape is relesed
    else if (event->keyval == 0xFF1B)
	hide();
    
    return false;
}
