#include <iostream>
#include <gtkmm.h>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"
#include "gmsgr_buddy_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_buddy_mgr::gmsgr_buddy_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt),    // g messenger client
    wnd_buddy(this) // buddy find/add window
{
    
}

// destructor
gmsgr_buddy_mgr::~gmsgr_buddy_mgr()
{
    // stop buddy manager
    stop();
    
    // free all buddies from the table
    for (map_buddy::iterator iter = buddy_tbl.begin(); iter != buddy_tbl.end(); iter++)
	delete (iter->second);
}


// -- operations --

/*
 * runs buddy manager.
 */
void gmsgr_buddy_mgr::run(int _main_x, int _main_y, int _main_w, int _main_h)
{
    // move the buddy add/find window in the center of the main window
    if (!wnd_buddy.is_visible())
    {
	int w, h;
	wnd_buddy.get_size(w, h);
    	wnd_buddy.move(_main_x + (_main_w - w) / 2, _main_y + (_main_h - h) / 2);
    }
    
    // show buddy find/add window
    wnd_buddy.show();    
    wnd_buddy.raise();
    wnd_buddy.grab_focus();
}

/*
 * stops buddy manager.
 */
void gmsgr_buddy_mgr::stop()
{
    // hide buddy find/add window
    wnd_buddy.hide();
}

/*
 * returns a buddy by the id.
 */
p_gmsgr_buddy gmsgr_buddy_mgr::get_buddy(string _id)
{
    // find the buddy on the table
    map_buddy::iterator iter = buddy_tbl.find(_id);
    if (iter == buddy_tbl.end())
	return NULL;
    
    return iter->second;
}

/*
 * handles buddy login.
 */
bool gmsgr_buddy_mgr::handle_buddy_login(string _id)
{
    // allocate a new buddy
    p_gmsgr_buddy buddy = new gmsgr_buddy(_id, clnt);
    if (!buddy)
    {
#ifdef _DEBUG
	cerr << "[Error] Not enough memory for a buddy!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // add the buddy into the table
    buddy_tbl[_id] = buddy;
    
    // add the buddy into the buddy tree view
    if (!clnt->wnd_main.add_buddy(_id))
    {
	handle_buddy_logout(_id);
	return false;
    }

#ifdef _DEBUG
    cout << "[Notice] Buddy[" << _id << "] is logined!" << endl;
#endif // _DEBUG

    return true;
}

/*
 * handles buddy logout.
 */
void gmsgr_buddy_mgr::handle_buddy_logout(string _id)
{
    // remove the buddy from the buddy tree view
    clnt->wnd_main.remove_buddy(_id);
    
    // find the buddy on the table
    map_buddy::iterator iter = buddy_tbl.find(_id);
    if (iter == buddy_tbl.end())
    {
#ifdef _DEBUG
	cerr << "[Error] Fail to delete a buddy from the table!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // free the buddy
    delete iter->second;

    // erase the buddy from the table
    buddy_tbl.erase(iter);

#ifdef _DEBUG
    cout << "[Notice] Buddy[" << _id << "] is logouted!" << endl;
#endif // _DEBUG
}

/*
 * handles buddy find result.
 */
bool gmsgr_buddy_mgr::handle_buddy_find_result(GMSGR_BUDDY_FIND_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case BUDDY_FIND_RSL_SUCCESS:
	    clnt->run_msg_dlg("The buddy has been found!\nPlease click add button to ask the buddy.", &wnd_buddy, MESSAGE_INFO);
	    wnd_buddy.set_sensitive(true);
	    wnd_buddy.enable_find_button(false);
	    wnd_buddy.enable_add_button(true);
	    wnd_buddy.enable_id_entry(false);
	    wnd_buddy.focus_add_button();
	    break;
	    
	case BUDDY_FIND_RSL_FAIL_ID_INVLD: 
	    clnt->run_msg_dlg("The buddy id does not exist!\nPlease try again.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.set_sensitive(true);
	    wnd_buddy.focus_id_entry();
	    break;

	case BUDDY_FIND_RSL_FAIL_ALREADY: 
	    clnt->run_msg_dlg("You already have the buddy!\nPlease try again.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.set_sensitive(true);
	    wnd_buddy.focus_id_entry();
	    break;

	case BUDDY_FIND_RSL_FAIL_OFFLINE: 
	    clnt->run_msg_dlg("The buddy id exists,\nbut is off-line right now!\nPlease try again.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.set_sensitive(true);
	    wnd_buddy.focus_id_entry();
	    break;

	case BUDDY_FIND_RSL_FAIL_UNKNWN: 
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &wnd_buddy, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
    
    return true;
}

/*
 * handles buddy add question.
 */
bool gmsgr_buddy_mgr::handle_buddy_add_question(string _id)
{
    // ask user whether he or she wants to accept
    string msg("User \"");
    int rsl = clnt->run_msg_dlg(msg + _id + "\" wants to make you his or her buddy!\nWould you like to accept?", 
	&clnt->wnd_main, MESSAGE_QUESTION, BUTTONS_YES_NO);
    
    // answer the question
    if (!answer_add_buddy((rsl == RESPONSE_YES) ? BUDDY_ADD_ASW_YES : BUDDY_ADD_ASW_NO, _id))
	return false;
    
    return true;
}

/*
 * handles buddy add result.
 */
bool gmsgr_buddy_mgr::handle_buddy_add_result(GMSGR_BUDDY_ADD_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case BUDDY_ADD_RSL_SUCCESS:
	    wnd_buddy.hide();
	    break;
	    
	case BUDDY_ADD_RSL_FAIL_ID_INVLD: 
	    clnt->run_msg_dlg("The buddy has left!\nPlease try next time.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.hide();
	    break;
    
	case BUDDY_ADD_RSL_FAIL_USR_FULL: 
	    clnt->run_msg_dlg("You already have the maximum number of buddies!\nPlease remove some buddies to make new one.", 
		&wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.hide();
	    break;

	case BUDDY_ADD_RSL_FAIL_BDY_FULL: 
	    clnt->run_msg_dlg("The buddy already has the maximum number of buddies!\nPlease try next time.", 
		&wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.hide();
	    break;

	case BUDDY_ADD_RSL_FAIL_OFFLINE: 
	    clnt->run_msg_dlg("The buddy is off-line right now!\nPlease try next time.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.hide();
	    break;

	case BUDDY_ADD_RSL_FAIL_REFUSED: 
	    clnt->run_msg_dlg("Unfortunately the user has denied to be your buddy!\nPlease try next time.", &wnd_buddy, MESSAGE_WARNING);
	    wnd_buddy.hide();
	    break;

	case BUDDY_ADD_RSL_FAIL_UNKNWN: 
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &wnd_buddy, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
    
    return true;
}

/*
 * handles buddy delete result.
 */
bool gmsgr_buddy_mgr::handle_buddy_delete_result(GMSGR_BUDDY_DEL_RSL _rsl)
{
    // handle by the result value
    switch (_rsl)
    {
	case BUDDY_DEL_RSL_SUCCESS:
	    break;
	    
	case BUDDY_DEL_RSL_FAIL_UNKNWN: 
	    clnt->run_msg_dlg("Sorry. Unknown error occured!\nPlease contact to the administrator.", &clnt->wnd_main, MESSAGE_ERROR);
	    exit(1);
	    break;
	    
	default:
	    return false;
    }
}

/*
 * applies to delete a buddy.
 */
bool gmsgr_buddy_mgr::apply_delete_buddy(string _id)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_BUDDY_DEL_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}


// -- methods --

/*
 * applies to find a buddy.
 */
bool gmsgr_buddy_mgr::apply_find_buddy(string _id)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_BUDDY_FIND_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

/*
 * applies to add a buddy.
 */
bool gmsgr_buddy_mgr::apply_add_buddy(string _id)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_BUDDY_ADD_APP;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    
    // set the packet body
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

/*
 * answers the buddy add question.
 */
bool gmsgr_buddy_mgr::answer_add_buddy(GMSGR_BUDDY_ADD_ASW _asw, string _id)
{
    gmsgr_packet pkt;
    
    // set the packet head
    pkt.head.type = PACKET_BUDDY_ADD_ASW;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_BUDDY_ADD_ASW) + (GMSGR_MAX_ID+1);
    
    // set the packet body
    memcpy(pkt.body, &_asw, sizeof(GMSGR_BUDDY_ADD_ASW));
    strncpy(pkt.body + sizeof(GMSGR_BUDDY_ADD_ASW), _id.c_str(), GMSGR_MAX_ID);
   
    // send the packet
    if (!clnt->mgr_transmission->send(&pkt))
	return false;
    
    return true;
}

