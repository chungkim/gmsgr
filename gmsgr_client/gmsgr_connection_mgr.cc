#include <gtkmm.h>
#include <iostream>
#include <pthread.h>
using namespace std;
using namespace Gtk;

#include "file.h"
#include "gmsgr_client.h"
#include "gmsgr_connection_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_connection_mgr::gmsgr_connection_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt),    // g messenger client
    serv_port(0),   // server port number
    conn_sec(0),    // connection time
    wnd_conn(this)  // connection window
{
    if (!get_server_info(serv_ip, serv_port, conn_sec))
    {
#ifdef _DEBUG    
	cerr << "[Error] Fail to get connection informtaion from the file!" << endl;
#endif // _DEBUG
	exit(1);
    }
}

// destructor
gmsgr_connection_mgr::~gmsgr_connection_mgr()
{
    // stop connection mananger
    stop();
}


// -- operations --

/*
 * runs connection manager to connect to the server.
 */
void gmsgr_connection_mgr::run()
{
    // create client socket
    if(!clnt->sock.create())
    {
#ifdef _DEBUG    
	cerr << "[Error] Fail to create client socket!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
#ifdef _DEBUG    
    // notice it's trying connecting
    cout << "[Notice] Try connecting to the server(" << serv_ip << "/" << serv_port 
	<< ") within " << conn_sec << " seconds!" << endl;     
#endif // _DEBUG

    // connect the waiting timer
    timer_wait = Glib::signal_timeout().connect(
	sigc::mem_fun(*this, &gmsgr_connection_mgr::on_timeout_wait), conn_sec * 1000 );    

    // create the connection thread
    pthread_t thrd_conn;
    if (pthread_create(&thrd_conn, NULL, thread_connect, this))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to create connection thread!" << endl;
#endif // _DEBUG	
	exit(1);
    }

    // run the connection window loop (never returns until the window hides)
    Main::run(wnd_conn);        
}

/*
 * stops connection manager to disconnect from the server.
 */
void gmsgr_connection_mgr::stop()
{
    if (clnt->sock.is_valid())
    {
	// disconnect from the server
	clnt->sock.close();

#ifdef _DEBUG    
	// notice it's disconnected
	cout << "[Notice] Disconnected from the server: (" << serv_ip << "/" << serv_port << ")" << endl;     
#endif // _DEBUG
    }	    
}

/*
 * indicates whether the connected.
 */
bool gmsgr_connection_mgr::is_connected()
{
    return (clnt->sock.is_valid());
}


// -- methods --        

/*
 * reads the server informaion from the file.
 */
bool gmsgr_connection_mgr::get_server_info(string& _ip, unsigned short& _port, int& _time)
{
    // read the file
    file file_serv(GMSGR_PATH_CONNECT, "r");
    char data[1024];
    int len = file_serv.read(data, 1023);
    if (!len)
	return false;
	
    // convert it to a string
    data[len] = 0;
    string info(data);

    int begin, end;
    
    // find server ip
    begin = info.find("ip=\'");
    if (begin == string::npos)
	return false;    
    begin += 4;
    end = info.find('\'', begin);
    if (end == string::npos)
	return false;	    
    _ip = info.substr(begin, end - begin);
    
    // find server port number
    begin = info.find("port=\'", end);
    if (begin == string::npos)
	return false;    
    begin += 6;
    end = info.find('\'', begin);
    if (end == string::npos)
	return false;	    
    _port = atoi(info.substr(begin, end - begin).c_str());
    
    // find connection time
    begin = info.find("time=\'", end);
    if (begin == string::npos)
	return false;    
    begin += 6;
    end = info.find('\'', begin);
    if (end == string::npos)
	return false;	    
    _time = atoi(info.substr(begin, end - begin).c_str());

    return true;
}

/*
 * connects to the server.
 */
#define this p
void* gmsgr_connection_mgr::thread_connect(void* _arg)
{
    p_gmsgr_connection_mgr p = (p_gmsgr_connection_mgr)_arg;
    
    // connect to the server
    if (!this->clnt->sock.connect(this->serv_ip, this->serv_port))
    {
	// disconnect the waiting timer
	this->timer_wait.disconnect();
	
	// close the connection immediately
	Glib::signal_timeout().connect(
	    sigc::mem_fun(*this, &gmsgr_connection_mgr::on_timeout_close), 0 );    	

#ifdef _DEBUG    
	cerr << "[Error] Fail to connect to the server!" << endl;
#endif // _DEBUG	

	return NULL;
    }
    
    // disconnect the waiting timer
    this->timer_wait.disconnect();

    // set the connection is succeeded
    Glib::signal_timeout().connect(
	    sigc::mem_fun(*this, &gmsgr_connection_mgr::on_timeout_open), 0 );    	
	
    return NULL;
}
#undef this

/*
 * waits for the connection time, and closes the connection.
 */
bool gmsgr_connection_mgr::on_timeout_wait()
{
    // close the socket
    clnt->sock.close();
        
    return false;
}

/*
 * closes the connection.
 */
bool gmsgr_connection_mgr::on_timeout_close()
{
    // notice the server is invalid
    clnt->mgr_connection->clnt->run_msg_dlg("Cannot connect to the server!\n\nCheck the ip and port in \"connect.ini\",\n"
	" or contact the administrator.", &wnd_conn, MESSAGE_ERROR);

    // hide the connection window
    wnd_conn.hide();
    
    exit(1);
    
    return false;
}

/*
 * is called when the connection is succeeded.
 */
bool gmsgr_connection_mgr::on_timeout_open()
{
    // hide the connection window
    wnd_conn.hide();

#ifdef _DEBUG    
    // notice it's connected
    cout << "[Notice] Connected to the server!" << endl;     
#endif // _DEBUG
    
    return false;
}
