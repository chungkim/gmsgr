#include <iostream>
#include <gtkmm.h>
using namespace Gtk;
using namespace std;

#include "gmsgr_client.h"
#include "gmsgr_connection_mgr.h"
#include "gmsgr_connect_window.h"

// -- construct, destruct --

// constructor
gmsgr_connect_window::gmsgr_connect_window(p_gmsgr_connection_mgr _mgr_connection)
:   mgr_connection(_mgr_connection) // connection manager
{
    // set window details
    set_size_request(200, 80);
    set_title("Connecting");
//    set_decorated(false);
    set_position(WIN_POS_CENTER);
    set_resizable(false);
    set_border_width(5);
    if (!set_icon_from_file(GMSGR_PATH_ICON))
    {
#ifdef _DEBUG	
	cerr << "[Error] Fail to set icon for login window!" << endl;
#endif // _DEBUG	
	exit(1);
    }
    signal_show().connect( sigc::mem_fun(*this, &gmsgr_connect_window::on_window_show) );
    signal_hide().connect( sigc::mem_fun(*this, &gmsgr_connect_window::on_window_hide) );

    // set label for connection
    label_conn.set_text("Connecting to the\n\tG Messenger server..");
    
    // add label and progress bar into the vertical box
    vbox_all.pack_start(label_conn);
    vbox_all.pack_start(prog_conn, PACK_SHRINK);
    
    // add vertical box into the window
    add(vbox_all);
    
    show_all_children();
}

// destructor
gmsgr_connect_window::~gmsgr_connect_window()
{
}


// -- operations --



// -- methods --

/*
 * handles the show message.
 */
void gmsgr_connect_window::on_window_show()
{
    // connect progress bar pulse timer
    timer_conn = Glib::signal_timeout().connect( 
	sigc::mem_fun(*this, &gmsgr_connect_window::on_timeout_progress_pulse), GMSGR_CONN_PROG_INTERVAL );    
}

/*
 * handles the show message.
 */
void gmsgr_connect_window::on_window_hide()
{
    timer_conn.disconnect();

    // exit if it's not connected yet
    if (mgr_connection->timer_wait.connected())
	exit(0);
}

/*
 * pulses the progress bar for connection.
 */
bool gmsgr_connect_window::on_timeout_progress_pulse()
{
    // pulse the progress bar
    prog_conn.pulse();
    
    return true;
}
