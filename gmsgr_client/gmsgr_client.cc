#include <iostream>
#include <gtkmm/main.h>
using namespace std;
using namespace Gtk;

#include "gmsgr_client.h"

// -- fields --

gmsgr_client*	gmsgr_client::instance;


// -- construct, destruct --

// constructor
gmsgr_client::gmsgr_client(int _argc, char** _argv)
:   gtkmm(_argc, _argv),	// gtkmm kit
    mgr_connection(NULL),	// connection manager
    mgr_packet(NULL),		// packet manager
    mgr_transmission(NULL),	// transmission manager
    mgr_certification(NULL),	// certification manager
    mgr_buddy(NULL),		// buddy manager
    wnd_main(this)		// main window
{   
#ifdef _DEBUG    
    cout << endl << "<< G Messenger Client v1.0 >>" << endl << endl;
#endif // _DEBUG
    
    // allocate connection manager
    mgr_connection = new gmsgr_connection_mgr(this); 
    if (!mgr_connection)
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for connection manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // allocate packet manager
    mgr_packet = new gmsgr_packet_mgr(this); 
    if (!mgr_packet)
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for packet manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
    
    // allocate transmission manager
    mgr_transmission = new gmsgr_transmission_mgr(this); 
    if (!mgr_transmission)
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for transmission manager!" << endl;
#endif // _DEBUG
	exit(1);
    }    
    
    // allocate certification manager
    mgr_certification = new gmsgr_certification_mgr(this); 
    if (!mgr_certification)
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for certification manager!" << endl;
#endif // _DEBUG
	exit(1);
    } 
    
    // allocate buddy manager
    mgr_buddy = new gmsgr_buddy_mgr(this); 
    if (!mgr_buddy)
    {
#ifdef _DEBUG    
	cerr << "[Error] Not enough memory for buddy manager!" << endl;
#endif // _DEBUG
	exit(1);
    }
}

// destructor
gmsgr_client::~gmsgr_client()
{
    // stop g messenger client
    stop();    
    
    // free connection manager
    if (mgr_connection)
	delete mgr_connection;

    // free buddy manager
    if (mgr_buddy)
	delete mgr_buddy;

    // free certification manager
    if (mgr_certification)
	delete mgr_certification;
    
    // free transmission manager
    if (mgr_transmission)
	delete mgr_transmission;
    
    // free packet manager
    if (mgr_packet)
	delete mgr_packet;
}
    
// -- properties --

/*
 * returns user id.
 */
string gmsgr_client::get_id()
{
    return id;
}
    
    
// -- operations --
    
/*
 * creates the g messenger client instance. (singleton pattern)
 */
gmsgr_client* gmsgr_client::create_instance(int _argc, char** _argv)
{
    if (instance) return NULL;
    
    // allocate the only instance
    instance = new gmsgr_client(_argc, _argv);
    if (!instance) return NULL;
    
    return instance;
}

/*
 * runs g messenger client.
 */
void gmsgr_client::run()
{
    // run connection manager
    mgr_connection->run();
    if (mgr_connection->is_connected())
    {
	// run packet manager
	mgr_packet->run();

	// run transmission manager
	mgr_transmission->run();

	// run certification manager (never returns until the login window hides)
	mgr_certification->run();	    

	// run main window loop (never returns until the window hides)
	Main::run(wnd_main);    
    }
}
    
/*
 * stops g messenger client.
 */
void gmsgr_client::stop()
{
    // stop connection manager
    mgr_connection->stop();

    // stop buddy manager
    mgr_buddy->stop();

    // stop certification manager
    mgr_certification->stop();
    
    // stop transmission manager
    mgr_transmission->stop();
    
    // stop packet manager
    mgr_packet->stop();
}

/*
 * resets g messager client.
 */
void gmsgr_client::reset()
{
    // stop all managers
    stop();

#ifdef _DEBUG    
    cout << "[Notice] G Messenger Client is reset!" << endl;
#endif // _DEBUG

    // run all managers again
    mgr_connection->run();
    if (mgr_connection->is_connected())
    {
	mgr_packet->run();
	mgr_transmission->run();

	// run certification manager (never returns until the login window hides)
	mgr_certification->run();	    
    }
}

/*
 * runs a message dialog synchronously.
 */
int gmsgr_client::run_msg_dlg(const Glib::ustring& _msg, Gtk::Window* _prnt /* = NULL */,
    MessageType _type /* = MESSAGE_INFO */, ButtonsType _btns /* = BUTTONS_OK */)
{
    MessageDialog* dlg = (_prnt) 
	? new MessageDialog(*_prnt, _msg, false, _type, _btns) 
	: new MessageDialog(_msg, false, _type, _btns);
    
    dlg->set_title(GMSGR_CLIENT_TITLE);
    
    int rsl = dlg->run();
    
    delete dlg;
    
    return rsl;
}

/*
 * shows a message dialog asynchronously.
 */
void gmsgr_client::show_msg_dlg(const Glib::ustring& _msg, Gtk::Window* _prnt /* = NULL */,
    MessageType _type /* = MESSAGE_INFO */, ButtonsType _btns /* = BUTTONS_OK */)
{
    MessageDialog* dlg = (_prnt) 
	? new MessageDialog(*_prnt, _msg, false, _type, _btns) 
	: new MessageDialog(_msg, false, _type, _btns);
    
    dlg->set_title(GMSGR_CLIENT_TITLE);
    
    dlg->show();
    
    delete dlg;    
}
