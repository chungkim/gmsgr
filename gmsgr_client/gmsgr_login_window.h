// 
// File:   gmsgr_login_window.h
// Author: William Kim
//
// Created on May 5, 2007, 4:02 PM
//

#ifndef _gmsgr_login_window_H
#define	_gmsgr_login_window_H

#include <string>
#include <gtkmm.h>

typedef class gmsgr_certification_mgr* p_gmsgr_certification_mgr;

/*
 * g messenger login window
 */
class gmsgr_login_window : public Gtk::Window
{
private:
    // -- fields --
    
    p_gmsgr_certification_mgr	mgr_certification;  // certification manager (passed by gmsgr_certification_mgr)
    
    Gtk::VBox			vbox_hboxes;	    // vertical box for hboxes
    
    Gtk::HBox			hbox_id;	    // horizontal box for id
    Gtk::Label			label_id;	    // label for id
    Gtk::Entry			entry_id;	    // entry for id 
    
    Gtk::HBox			hbox_pwd;	    // horizontal box for password
    Gtk::Label			label_pwd;	    // label for password
    Gtk::Entry			entry_pwd;	    // entry for password 
    
    Gtk::HBox			hbox_btns;	    // horizontal box for buttons
    Gtk::Button			btn_login;	    // login button
    Gtk::Button			btn_join;	    // join button
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_login_window(p_gmsgr_certification_mgr _mgr_certification);
    
    // destructor
    ~gmsgr_login_window();
    
    
    // -- operations --
    
    /*
     * focuses on id entry.
     */
    void focus_id_entry();

    /*
     * focuses on password entry.
     */
    void focus_pwd_entry();
    
    /*
     * clears id entry.
     */
    void clear_id_entry();
    
    /*
     * clears password entry.
     */
    void clear_pwd_entry();
    
    /*
     * returns the id on id entry.
     */
    std::string get_id(); 

    
private:
    // -- methods --        
    
    /*
     * handles the show message.
     */
    void on_window_show();

    /*
     * handles the hide message.
     */
    void on_window_hide();
    
    /*
     * handles the clicked signal on login button.
     */
    void on_login_btn_clicked();
    
    /*
     * handles the clicked signal on join button.
     */
    void on_join_btn_clicked();
    
    /*
     * handles the key release signal on id, password entries.
     */
    bool on_entry_key_release(GdkEventKey* event);
};

typedef gmsgr_login_window* p_gmsgr_login_window;

#endif	/* _gmsgr_login_window_H */

