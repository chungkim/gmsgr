// 
// File:   gmsgr_packet_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 12:23 AM
//

#ifndef _gmsgr_packet_mgr_H
#define	_gmsgr_packet_mgr_H

#include "gmsgr_packet.h"

typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger packet manager
 */
class gmsgr_packet_mgr
{
private:
    // -- fields --

    const p_gmsgr_client    clnt;	// managed g messenger client
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_packet_mgr(p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_packet_mgr();
    
    
    // -- operations --
    
    /*
     * transacts a g messenger packet
     */
    bool transact(p_gmsgr_packet _pkt);
    
    
private:    
    // -- methods --    
    
    /*
     * transacts the packet JOIN_APP.
     */
    bool transact_join_apply(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet LEAVE_APP.
     */
    bool transact_leave_apply(p_gmsgr_packet _pkt);

    /*
     * transacts the packet LOGIN_APP.
     */
    bool transact_login_apply(p_gmsgr_packet _pkt);    
    
    /*
     * transacts the packet BUDDY_FIND_APP.
     */
    bool transact_buddy_find_apply(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet BUDDY_ADD_APP.
     */
    bool transact_buddy_add_apply(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet BUDDY_ADD_ASW.
     */
    bool transact_buddy_add_answer(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet BUDDY_DEL_APP.
     */
    bool transact_buddy_delete_apply(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet CHAT_MSG.
     */
    bool transact_chat_message(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet FILE_APP.
     */
    bool transact_file_apply(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_ASW.
     */
    bool transact_file_answer(p_gmsgr_packet _pkt);
    
    /*
     * transacts the packet FILE_BIN.
     */
    bool transact_file_binary(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_ACK.
     */
    bool transact_file_acknowledge(p_gmsgr_packet _pkt);

    /*
     * transacts the packet FILE_ESC.
     */
    bool transact_file_escape(p_gmsgr_packet _pkt);
};

typedef gmsgr_packet_mgr* p_gmsgr_packet_mgr;

#endif	/* _gmsgr_packet_mgr_H */

