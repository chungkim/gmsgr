// 
// File:   gmsgr_file_mgr.h
// Author: root
//
// Created on May 24, 2007, 5:56 AM
//

#ifndef _gmsgr_file_mgr_H
#define	_gmsgr_file_mgr_H

#include <string>

#include "gmsgr_packet_type.h"

typedef class gmsgr_user* p_gmsgr_user;

/*
 * g messenger file manager
 */
class gmsgr_file_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_user	usr;	// g meesenger user (passed from gmsgr_user)

    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_file_mgr(p_gmsgr_user _usr);

    // destructor
    ~gmsgr_file_mgr();
    
    
    // -- operations --
    
    /*
     * asks whether the user wants to accept the file.
     */
    bool ask_file(std::string _buddy_id, std::string _name, int _size);
    
    /*
     * answers whether the buddy wants to accept the file/
     */
    bool answer_file(std::string _buddy_id, GMSGR_FILE_ASW _asw);
    
    /*
     * passes the file binary to the user.
     */
    bool pass_binary(std::string _buddy_id, int _whole, int _cur, char* _bin);
    
    /*
     * passes the file acknowledge to the user.
     */
    bool pass_acknowledge(std::string _buddy_id);
    
    /*
     * passes the file escape to the user.
     */
    bool pass_escape(std::string _buddy_id);
    
    
private:    
    // -- methods --    
    
};

typedef gmsgr_file_mgr* p_gmsgr_file_mgr;

#endif	/* _gmsgr_file_mgr_H */

