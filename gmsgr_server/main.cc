// 
// File:   main.cc
// Author: William Kim
//
// Created on May 4, 2007, 11:03 PM
//

#include <iostream>
using namespace std;

#include "gmsgr_server.h"   // g messenger server

int
main(int argc, char** argv) 
{
    if (argc != 3)
    {
	// display how to use
	cout << "usage: " << argv[0] << " <port> <max client number>" << endl;
	return 1;
    }
    
    // convert the arguments
    unsigned short port = atoi(argv[1]);
    unsigned int clnt_max = atoi(argv[2]);
    
    // create the g messenger server instance
    p_gmsgr_server serv = gmsgr_server::create_instance(port, clnt_max);
    if (!serv)
    {
	cerr << "[Error] Not enough memory for server instance!" << endl;
	return 1;
    }
    
    // run g messenger server
    serv->run();
    
    // delete the g messenger server instance
    delete serv;
    
    return 0;
}

