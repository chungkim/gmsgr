#include <iostream>
using namespace std;

#include "gmsgr_packet.h"
#include "gmsgr_client.h"
#include "gmsgr_user.h"
#include "gmsgr_file_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_file_mgr::gmsgr_file_mgr(p_gmsgr_user _usr)
:   usr(_usr)	// g meesenger user
{
    
}

// destructor
gmsgr_file_mgr::~gmsgr_file_mgr()
{
    
}


// -- operations --

/*
 * asks whether the user wants to accept the file.
 */
bool gmsgr_file_mgr::ask_file(string _buddy_id, string _name, int _size)
{
    cout << "[Notice] User[" << _buddy_id << "] apllied to send a file(" 
	<< _name << ") to buddy[" << usr->id << "]!" << endl;
    
    // make a file question packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_FILE_QST;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + sizeof(_size) + (_name.length()+1);
    strcpy(pkt.body, _buddy_id.c_str());
    memcpy(pkt.body + (GMSGR_MAX_ID+1), &_size, sizeof(_size));
    strcpy(pkt.body + (GMSGR_MAX_ID+1) + sizeof(_size), _name.c_str());
    
    // send the packet to the client
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}

/*
 * answers whether the buddy wants to accept the file/
 */
bool gmsgr_file_mgr::answer_file(std::string _buddy_id, GMSGR_FILE_ASW _asw)
{
    cout << "[Notice] User[" << _buddy_id << "] file answers to the buddy[" << usr->id << "]: (" << _asw << ")" << endl;

    // convert the answer to result
    GMSGR_FILE_RSL rsl = (_asw == FILE_ASW_NO) ? FILE_RSL_FAIL_REFUSED : FILE_RSL_SUCCESS;
    
    // make a file result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_FILE_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_FILE_RSL) + (GMSGR_MAX_ID+1);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_FILE_RSL));
    strcpy(pkt.body + sizeof(GMSGR_FILE_RSL), _buddy_id.c_str());

    // send the packet to the buddy who asked
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }   

    return true;
}

/*
 * passes the file binary to the user.
 */
bool gmsgr_file_mgr::pass_binary(string _buddy_id, int _whole, int _cur, char* _bin)
{
    // get part size
    int part = (_whole - _cur < GMSGR_MAX_PACKET_FILE) ? (_whole - _cur) : GMSGR_MAX_PACKET_FILE;

#ifdef _DEBUG    
    cout << "[Notice] User[" << _buddy_id << "] sent file binary(" 
	<< part << "b) to buddy[" << usr->id << "]!" << endl;
#endif // _DEBUG

    // make a file binary packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_FILE_BIN;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + sizeof(_whole) + sizeof(_cur) + part;
    strncpy(pkt.body, _buddy_id.c_str(), GMSGR_MAX_ID);
    memcpy(pkt.body + (GMSGR_MAX_ID+1), &_whole, sizeof(_whole));
    memcpy(pkt.body + (GMSGR_MAX_ID+1) + sizeof(_whole), &_cur, sizeof(_cur));    
    memcpy(pkt.body + (GMSGR_MAX_ID+1) + sizeof(_whole) + sizeof(_cur), _bin, part);
    
    // send the packet to the client
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}

/*
 * passes the file acknowledge to the user.
 */
bool gmsgr_file_mgr::pass_acknowledge(string _buddy_id)
{
#ifdef _DEBUG    
    cout << "[Notice] User[" << _buddy_id << "] sent file acknowledge to buddy[" << usr->id << "]!" << endl;
#endif // _DEBUG

    // make a file acknowledge packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_FILE_ACK;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    strncpy(pkt.body, _buddy_id.c_str(), GMSGR_MAX_ID);
    
    // send the packet to the client
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}

/*
 * passes the file escape to the user.
 */
bool gmsgr_file_mgr::pass_escape(string _buddy_id)
{
    cout << "[Notice] User[" << _buddy_id << "] sent file escape to buddy[" << usr->id << "]!" << endl;
    
    // make a file acknowledge packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_FILE_ESC;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    strncpy(pkt.body, _buddy_id.c_str(), GMSGR_MAX_ID);
    
    // send the packet to the client
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}
    
    
// -- methods --

