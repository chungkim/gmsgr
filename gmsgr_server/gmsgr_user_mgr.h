// 
// File:   gmsgr_user_mgr.h
// Author: William Kim
//
// Created on May 9, 2007, 1:42 AM
//

#ifndef _gmsgr_user_mgr_H
#define	_gmsgr_user_mgr_H

#include <pthread.h>
#include <string>
#include <map>

#include "mmap_file.h"
#include "gmsgr_protocol.h"
#include "gmsgr_packet_type.h"
#include "gmsgr_user.h"

typedef class gmsgr_server* p_gmsgr_server;
typedef class gmsgr_client* p_gmsgr_client;

#define GMSGR_PATH_USER	"user.dat"  // user data file name

/*
 * g messenger user data manager
 */
class gmsgr_user_mgr
{
private:
    // -- inner types --
    
    /*
     * user data structure
     */
    typedef struct gmsgr_user_data {
	char	id[GMSGR_MAX_ID+1];			    // id
	char	pwd_hash[GMSGR_SIZEOF_PWD_HASH];	    // password hash
	char	buddy_ids[GMSGR_MAX_BUDDY][GMSGR_MAX_ID+1]; // buddy ids
    }* p_gmsgr_user_data;
    
    // user id - data table
    typedef std::map<std::string, p_gmsgr_user_data> map_user_data;
    
    // user id - object table
    typedef std::map<std::string, p_gmsgr_user> map_user_log;
    
    
    // -- fields --
    
    p_gmsgr_server		serv;		// g meesenger server (passed from gmsgr_server)

    mmap_file<gmsgr_user_data>	usr_dat_file;	// user data memory mapped file
    map_user_data		usr_dat_tbl;	// user data table
    pthread_mutex_t		usr_dat_mutx;	// mutex object for user data
    
    map_user_log		usr_log_tbl;	// logined user table
    pthread_mutex_t		usr_log_mutx;	// mutex object for loginned users
    
    
public:
    // -- construct, destruct 
    
    // constructor
    gmsgr_user_mgr(p_gmsgr_server _serv);

    // destructor
    ~gmsgr_user_mgr();
    
    
    // -- operations --
    
    /*
     * runs user data manager.
     */
    void run();
    
    /*
     * return a user.
     */
    p_gmsgr_user get_user_log(std::string _id);
    
    /*
     * joins a new user.
     */
    GMSGR_JOIN_RSL join(std::string _id, char* _pwd_hash);
    
    /*
     * leaves a user.
     */
    GMSGR_LEAVE_RSL leave(std::string _id);
    
    /*
     * logins a user.
     */
    GMSGR_LOGIN_RSL login(std::string _id, char* _pwd_hash, p_gmsgr_client _clnt);

    /*
     * logouts a user.
     */
    bool logout(std::string _id, p_gmsgr_client _clnt);
    
    /*
     * finds a user.
     */
    GMSGR_BUDDY_FIND_RSL find_user(string _usr_id, std::string _buddy_id);

    /*
     * asks a user to add a buddy.
     */
    GMSGR_BUDDY_ADD_RSL ask_add_buddy(p_gmsgr_user _usr, std::string _buddy_id);

    /*
     * adds buddy to each other users.
     */
    bool create_buddyship(p_gmsgr_user _usr_1, p_gmsgr_user _usr_2);
    
    /*
     * deletes buddy from each other users.
     */
    bool destroy_buddyship(p_gmsgr_user _usr_1, p_gmsgr_user _usr_2);

    
private:    
    // -- methods --     
        
    /*
     * returns a user data.
     */
    p_gmsgr_user_data get_user_data(std::string _id);

    /*
     * adds a buddy into user data.
     */
    bool add_buddy_data(std::string _id, std::string _buddy_id);
    
    /*
     * deletes a buddy from user data.
     */
    bool del_buddy_data(std::string _id, std::string _buddy_id);
    
    /*
     * indicates whether the buddyship has been created.
     */
    bool are_buddies(std::string _id_1, std::string _id_2);
    
    /*
     * indicates whether the user's buddy data is full.
     */
    bool is_buddy_data_full(std::string _id);
};

typedef gmsgr_user_mgr* p_gmsgr_user_mgr;

#endif	/* _gmsgr_user_mgr_H */

