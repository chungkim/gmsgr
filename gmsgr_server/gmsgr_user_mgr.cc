#include <iostream>
using namespace std;

#include "gmsgr_server.h"
#include "gmsgr_client.h"
#include "gmsgr_user_mgr.h"

// -- construct, destruct

// constructor
gmsgr_user_mgr::gmsgr_user_mgr(p_gmsgr_server _serv)
:   serv(_serv),		    // g meesenger server
    usr_dat_file(GMSGR_PATH_USER)   // user data memory mapped file
{
    if (!usr_dat_file.buf)
    {
	cerr << "[Error] Fail to map user data file!" << endl;
	exit(1);
    }
    
    // initialize mutex for user data
    if (pthread_mutex_init(&usr_dat_mutx, NULL))
    {
	cerr << "[Error] Fail to initialize mutex for user data!" << endl;
	exit(1);
    }
    
    // initialize mutex for logined users
    if (pthread_mutex_init(&usr_log_mutx, NULL))
    {
	cerr << "[Error] Fail to initialize mutex for logined users!" << endl;
	exit(1);
    }
}

// destructor
gmsgr_user_mgr::~gmsgr_user_mgr()
{
    // destroy mutex for logined users
    pthread_mutex_destroy(&usr_log_mutx);

    // destroy mutex for user data
    pthread_mutex_destroy(&usr_dat_mutx);
}


// -- operations --

/*
 * runs user data manager,
 */
void gmsgr_user_mgr::run()
{
    // get all user data count
    int cnt = usr_dat_file.size / sizeof(gmsgr_user_data);
    
    // put all user data on the table
    cout << "[Notice] All users joined" << endl;
    for (unsigned int i = 0; i < cnt; i++)
    {
    	usr_dat_tbl[usr_dat_file.buf[i].id] = &usr_dat_file.buf[i];    
	cout << "(" << i+1 << ")\t\t" << usr_dat_file.buf[i].id << endl;
    }
    cout << "[Notice] Total user count: " << usr_dat_tbl.size() << endl;
}

/*
 * return a user.
 */
p_gmsgr_user gmsgr_user_mgr::get_user_log(string _id)
{
    pthread_mutex_lock(&usr_log_mutx);
    
    // is the user not logined?
    map_user_log::iterator iter = usr_log_tbl.find(_id);
    if (iter == usr_log_tbl.end()) 
    {
	pthread_mutex_unlock(&usr_log_mutx);
	return NULL;
    }
    
    pthread_mutex_unlock(&usr_log_mutx);
    
    return iter->second;
}

/*
 * joins a new user.
 */
GMSGR_JOIN_RSL gmsgr_user_mgr::join(string _id, char* _pwd_hash)
{
    cout << "[Notice] User[" << _id << "] apllied to join!" << endl;
    
    pthread_mutex_lock(&usr_dat_mutx);

    // does this id already exist?
    if (usr_dat_tbl.find(_id) != usr_dat_tbl.end())
    {
	pthread_mutex_unlock(&usr_dat_mutx);
    	return JOIN_RSL_FAIL_ID_EXIST;
    }
    
    // remap user data file to expand
    if (!usr_dat_file.remap(usr_dat_file.size + sizeof(gmsgr_user_data)))
    {
	pthread_mutex_unlock(&usr_dat_mutx);
    	return JOIN_RSL_FAIL_UNKNWN;
    }
    
    // set the user data on the file
    p_gmsgr_user_data usr_dat = &((usr_dat_file.buf)[usr_dat_tbl.size()]);
    memset(usr_dat, 0, sizeof(gmsgr_user_data));
    strcpy(usr_dat->id, _id.c_str());
    memcpy(usr_dat->pwd_hash, _pwd_hash, GMSGR_SIZEOF_PWD_HASH);
    
    // synchronize the user data file
    if (!usr_dat_file.sync())
    {
	pthread_mutex_unlock(&usr_dat_mutx);
    	return JOIN_RSL_FAIL_UNKNWN;
    }
    
    // put the user data on the table
    usr_dat_tbl[usr_dat->id] = usr_dat;

    pthread_mutex_unlock(&usr_dat_mutx);
    
    cout << "[Notice] User[" << _id << "] is joined!" << endl;
    cout << "[Notice] Current joined user count: " << usr_dat_tbl.size() << endl;

    return JOIN_RSL_SUCCESS;
}

/*
 * leaves a user.
 */
GMSGR_LEAVE_RSL gmsgr_user_mgr::leave(string _id)
{
    cout << "[Notice] User[" << _id << "] apllied to leave!" << endl;
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // does this id exist?
    map_user_data::iterator iter = usr_dat_tbl.find(_id);
    if (iter == usr_dat_tbl.end())
    {
	pthread_mutex_unlock(&usr_dat_mutx);
    	return LEAVE_RSL_FAIL_ID_INVLD;
    }
    
    // pick the user data on the table
    p_gmsgr_user_data usr_dat = iter->second;
    
    pthread_mutex_unlock(&usr_dat_mutx);

    // get the user from the table
    p_gmsgr_user usr = get_user_log(_id);
    if (!usr)
	return LEAVE_RSL_FAIL_UNKNWN;

    // destroy the user from all the all buddy data, and buddy object if he or she's logined
    for (unsigned int i = 0; i < GMSGR_MAX_BUDDY && strlen(usr_dat->buddy_ids[i]) > 0; i++)
    {
	p_gmsgr_user buddy = get_user_log(usr_dat->buddy_ids[i]);
	if (buddy)
	{
	    if (!buddy->del_buddy(usr))
		return LEAVE_RSL_FAIL_UNKNWN;
	}

	if (!usr->del_buddy(buddy))
	    return LEAVE_RSL_FAIL_UNKNWN;
	
	if (!del_buddy_data(usr_dat->buddy_ids[i], _id))
	    return LEAVE_RSL_FAIL_UNKNWN;
    }

    pthread_mutex_lock(&usr_dat_mutx);
        
    // overwrite the data at the end of the file on the place of the user data
    memcpy(usr_dat, &usr_dat_file.buf[usr_dat_tbl.size()-1], sizeof(gmsgr_user_data));
    
    // remap the file to truncate
    if (!usr_dat_file.remap(usr_dat_file.size - sizeof(gmsgr_user_data)))
    {
	pthread_mutex_unlock(&usr_dat_mutx);
    	return LEAVE_RSL_FAIL_UNKNWN;
    }
    
    // remove the user data on the table
    usr_dat_tbl.erase(iter);
    
    // reset the moved user data on the table
    usr_dat_tbl[usr_dat->id] = usr_dat;

    pthread_mutex_unlock(&usr_dat_mutx);
    
    cout << "[Notice] User[" << _id << "] is left!" << endl;
    cout << "[Notice] Current joined user count: " << usr_dat_tbl.size() << endl;

    return LEAVE_RSL_SUCCESS;
}

/*
 * logins a user.
 */
GMSGR_LOGIN_RSL gmsgr_user_mgr::login(string _id, char* _pwd_hash, p_gmsgr_client _clnt)
{
    cout << "[Notice] User[" << _id << "] apllied to login!" << endl;
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // does this id exist in the user data table?
    map_user_data::iterator iter_dat = usr_dat_tbl.find(_id);
    if (iter_dat == usr_dat_tbl.end()) 
    {
	pthread_mutex_unlock(&usr_dat_mutx);
	return LOGIN_RSL_FAIL_ID_INVLD;
    }

    // pick the user data on the table
    p_gmsgr_user_data usr_dat = iter_dat->second;
    
    // is the password hash valid?
    if (memcmp(usr_dat->pwd_hash, _pwd_hash, GMSGR_SIZEOF_PWD_HASH)) 
    {
	pthread_mutex_unlock(&usr_dat_mutx);
	return LOGIN_RSL_FAIL_PWD_INVLD;
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    pthread_mutex_lock(&usr_log_mutx);
    
    // has the user already logined?
    map_user_log::iterator iter_log = usr_log_tbl.find(_id);
    if (iter_log != usr_log_tbl.end()) 
    {
	pthread_mutex_unlock(&usr_log_mutx);
	
	cout << "[Notice] User[" << _id << "] is already logined!" << endl;
	
	// disconnect the user logined before
	serv->mgr_connection->del_client(iter_log->second->get_clnt()->get_index());
	
	pthread_mutex_lock(&usr_log_mutx);
    }
    
    // allocate a user
    p_gmsgr_user usr = new gmsgr_user(_id, _clnt);
    if (!usr) 
    {
	pthread_mutex_unlock(&usr_log_mutx);
	cerr << "[Error] Not enough memory for a user!" << endl;
	return LOGIN_RSL_FAIL_UNKNWN;
    }
    
    // link the client with the user
    _clnt->set_user(usr); 
    
    // put the user on the table
    usr_log_tbl[_id] = usr;
    
    pthread_mutex_unlock(&usr_log_mutx);
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // add logined buddies from the user data
    for (unsigned int i = 0; i < GMSGR_MAX_BUDDY && strlen(usr_dat->buddy_ids[i]) > 0; i++) 
    {
	iter_log = usr_log_tbl.find(usr_dat->buddy_ids[i]);
	if (iter_log != usr_log_tbl.end())
	{
	    usr->add_buddy(iter_log->second);
	    iter_log->second->add_buddy(usr);
	}
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    cout << "[Notice] User[" << _id << "] is logined!" << endl;
    cout << "[Notice] Current logined user count: " << usr_log_tbl.size() << endl;
    
    return LOGIN_RSL_SUCCESS;
}
    
/*
 * logouts a user.
 */
bool gmsgr_user_mgr::logout(string _id, p_gmsgr_client _clnt)
{
    cout << "[Notice] User[" << _id << "] apllied to logout!" << endl;
    
    pthread_mutex_lock(&usr_log_mutx);
    
    // is the user logined?
    map_user_log::iterator iter = usr_log_tbl.find(_id);
    if (iter == usr_log_tbl.end()) 
    {
	pthread_mutex_unlock(&usr_log_mutx);
	return false;
    }
    
    // pick the user on the table
    p_gmsgr_user usr = iter->second;

    // clear all buddies
    usr->clear_buddies();

    // remove the user on the table
    usr_log_tbl.erase(iter);
    
    // unlink the user and client
    _clnt->set_user(NULL);
    
    // free the user
    delete usr;

    pthread_mutex_unlock(&usr_log_mutx);
    
    cout << "[Notice] User[" << _id << "] is logouted!" << endl;
    cout << "[Notice] Current logined user count: " << usr_log_tbl.size() << endl;
    
    return true;
}

/*
 * finds a user.
 */
GMSGR_BUDDY_FIND_RSL gmsgr_user_mgr::find_user(string _user_id, string _buddy_id)
{
    // does this id exist in the user data table?
    if (!get_user_data(_buddy_id)) 
	return BUDDY_FIND_RSL_FAIL_ID_INVLD;
    
    // are they already buddies?
    if (are_buddies(_user_id, _buddy_id))
	return BUDDY_FIND_RSL_FAIL_ALREADY;
    
    // is the user not logined?
    if (!get_user_log(_buddy_id)) 
	return BUDDY_FIND_RSL_FAIL_OFFLINE;
    
    return BUDDY_FIND_RSL_SUCCESS;
}

/*
 * asks a user to add a buddy.
 */
GMSGR_BUDDY_ADD_RSL gmsgr_user_mgr::ask_add_buddy(p_gmsgr_user _usr, string _buddy_id)
{
    cout << "[Notice] User[" << _usr->get_id() << "] apllied to add the buddy[" << _buddy_id << "]!" << endl;
    
    GMSGR_BUDDY_FIND_RSL find_rsl = find_user(_usr->get_id(), _buddy_id);

    // has the buddy left?
    if (find_rsl == BUDDY_FIND_RSL_FAIL_ID_INVLD)
	return BUDDY_ADD_RSL_FAIL_ID_INVLD;    
    // is the buddy off-line?
    else if (find_rsl == BUDDY_FIND_RSL_FAIL_OFFLINE)
	return BUDDY_ADD_RSL_FAIL_OFFLINE;
    // has error occured?
    else if (find_rsl == BUDDY_FIND_RSL_FAIL_UNKNWN)
	return BUDDY_ADD_RSL_FAIL_UNKNWN;
    
    // is the user's buddy data full?
    if (is_buddy_data_full(_usr->get_id()))
	return BUDDY_ADD_RSL_FAIL_USR_FULL;
    
    // is the buddy's buddy data full?
    if (is_buddy_data_full(_buddy_id))
	return BUDDY_ADD_RSL_FAIL_BDY_FULL;
    
    // pick the buddy on the table
    pthread_mutex_lock(&usr_log_mutx);
    p_gmsgr_user buddy = usr_log_tbl[_buddy_id];
    pthread_mutex_unlock(&usr_log_mutx);
    
    // make a buddy add question packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_ADD_QST;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    strncpy(pkt.body, _usr->get_id().c_str(), GMSGR_MAX_ID);
    
    // send the packet to the client
    if (!buddy->get_clnt()->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return BUDDY_ADD_RSL_FAIL_UNKNWN;
    }

    return BUDDY_ADD_RSL_SUCCESS;   // meaning the question is successful
}

/*
 * adds buddy to each other users.
 */
bool gmsgr_user_mgr::create_buddyship(p_gmsgr_user _usr_1, p_gmsgr_user _usr_2)
{
    // add buddy data to each other
    if (!add_buddy_data(_usr_1->get_id(), _usr_2->get_id()))
	return false;
    if (!add_buddy_data(_usr_2->get_id(), _usr_1->get_id()))
	return false;
    
    // add buddy object to each other
    if (!_usr_1->add_buddy(_usr_2))
	return false;
    if (!_usr_2->add_buddy(_usr_1))
	return false;
    
    // notice it's successful
    cout << "[Notice] Users[" << _usr_1->get_id() << "/" << _usr_2->get_id() << "] have created a buddyship!" << endl;
    
    return true;
}
    
/*
 * deletes buddy from each other users.
 */
bool gmsgr_user_mgr::destroy_buddyship(p_gmsgr_user _usr_1, p_gmsgr_user _usr_2)
{
    // delete buddy data from each other
    if (!del_buddy_data(_usr_1->get_id(), _usr_2->get_id()))
	return false;
    if (!del_buddy_data(_usr_2->get_id(), _usr_1->get_id()))
	return false;
    
    // delete buddy object from each other
    if (!_usr_1->del_buddy(_usr_2))
	return false;
    if (!_usr_2->del_buddy(_usr_1))
	return false;
    
    // notice it's successful
    cout << "[Notice] Users[" << _usr_1->get_id() << "/" << _usr_2->get_id() << "] have destroyed a buddyship!" << endl;
	
    return true;
}

    
// -- methods --

/*
 * returns a user data.
 */
gmsgr_user_mgr::p_gmsgr_user_data gmsgr_user_mgr::get_user_data(string _id)
{
    pthread_mutex_lock(&usr_dat_mutx);
    
    // does this id exist in the user data table?
    map_user_data::iterator iter = usr_dat_tbl.find(_id);
    if (iter == usr_dat_tbl.end()) 
    {
	pthread_mutex_unlock(&usr_dat_mutx);
	return NULL;
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    return iter->second;
}

/*
 * adds a buddy into user data.
 */
bool gmsgr_user_mgr::add_buddy_data(string _id, string _buddy_id)
{
    // get user data
    p_gmsgr_user_data usr_dat = get_user_data(_id);
    if (!usr_dat)
	return false;
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // add a buddy data in empty block
    unsigned int i;
    for (i = 0; i < GMSGR_MAX_BUDDY; i++)
    {
	if (!strlen(usr_dat->buddy_ids[i]))
	{
	    strcpy(usr_dat->buddy_ids[i], _buddy_id.c_str());
	    break;
	}
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    if (i >= GMSGR_MAX_BUDDY)
	return false;
    
    return true;
}

/*
 * deletes a buddy from user data.
 */
bool gmsgr_user_mgr::del_buddy_data(string _id, string _buddy_id)
{
    // get user data
    p_gmsgr_user_data usr_dat = get_user_data(_id);
    if (!usr_dat)
	return false;
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // find the buddy data which has the same id
    unsigned int i, j;
    for (i = 0; i < GMSGR_MAX_BUDDY; i++)
    {
	// pull the other buddy data next
	if (!strcmp(usr_dat->buddy_ids[i], _buddy_id.c_str()))
	{
	    for (j = i+1; j < GMSGR_MAX_BUDDY; j++)
		strcpy(usr_dat->buddy_ids[j-1], usr_dat->buddy_ids[j]);
	    
	    // set the last buddy data invalid
	    strcpy(usr_dat->buddy_ids[j-1], "");
	    
	    break;
	}
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    if (i >= GMSGR_MAX_BUDDY)
	return false;
    
    return true;
}

/*
 * indicates whether the buddyship has been created.
 */
bool gmsgr_user_mgr::are_buddies(string _id_1, string _id_2)
{
    // get both user data
    p_gmsgr_user_data usr_dat_1 = get_user_data(_id_1);
    if (!usr_dat_1)
	return false;
    p_gmsgr_user_data usr_dat_2 = get_user_data(_id_2);
    if (!usr_dat_2)
	return false;
    
    pthread_mutex_lock(&usr_dat_mutx);
    
    // check if user 1 has user 2 as a buddy
    unsigned int i;
    bool buddyship = false;    
    for (i = 0; i < GMSGR_MAX_BUDDY && strlen(usr_dat_1->buddy_ids[i]) > 0; i++)
    {
	if (!strcmp(usr_dat_1->buddy_ids[i], _id_2.c_str()))
	{
	    buddyship = true;
	    break;
	}
    }
    if (!buddyship) 
    {
	pthread_mutex_unlock(&usr_dat_mutx);
	return false;
    }
    
    // check if user 2 has user 1 as a buddy
    buddyship = false;
    for (i = 0; i < GMSGR_MAX_BUDDY && strlen(usr_dat_2->buddy_ids[i]) > 0; i++)
    {
	if (!strcmp(usr_dat_2->buddy_ids[i], _id_1.c_str()))
	{
	    buddyship = true;
	    break;
	}
    }
    if (!buddyship) 
    {
	pthread_mutex_unlock(&usr_dat_mutx);
	return false;
    }
    
    pthread_mutex_unlock(&usr_dat_mutx);
    
    return true;
}

/*
 * indicates whether the user's buddy data is full.
 */
bool gmsgr_user_mgr::is_buddy_data_full(string _id)
{
    // get user data
    p_gmsgr_user_data usr_dat = get_user_data(_id);
    if (!usr_dat)
	return true;
    
    // count the buddy blocks
    unsigned int i;
    for (i = 0; i < GMSGR_MAX_BUDDY; i++)
    {
	if (!strlen(usr_dat->buddy_ids[i]))
	    break;
    }
    
    return (i >= GMSGR_MAX_BUDDY);
}
