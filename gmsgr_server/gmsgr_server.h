// 
// File:   gmsgr_server.h
// Author: William Kim
//
// Created on May 4, 2007, 11:33 PM
//

#ifndef _gmsgr_server_H
#define	_gmsgr_server_H

#include "gmsgr_client.h"
#include "gmsgr_user_mgr.h"
#include "gmsgr_connection_mgr.h"

/*
 * g messegner server
 */
class gmsgr_server
{
private:
    // -- fields --
    
    static gmsgr_server*    instance;	    // singleton instance
    
    p_gmsgr_client	    clnts;	    // g messenger clients
   
    p_gmsgr_user_mgr	    mgr_user;	    // user data mananger
    p_gmsgr_connection_mgr  mgr_connection; // connection manager
    
    
    // -- construct, destruct --

    // constructor
    gmsgr_server(unsigned short _port, unsigned int _clnt_max);

public:
    // destructor
    ~gmsgr_server();
    
    
    // -- operations --
    
    /*
     * creates the g messenger server instance. (singleton pattern)
     */
    static gmsgr_server* create_instance(unsigned short _port, unsigned int _clnt_max);
    
    /*
     * runs g messenger server.
     */
    void run();
    
private:    
    // -- aggregation --
    
    friend class gmsgr_user_mgr;	// user manager
    friend class gmsgr_connection_mgr;	// connection manager
};

typedef gmsgr_server* p_gmsgr_server;

#endif	/* _gmsgr_server_H */

