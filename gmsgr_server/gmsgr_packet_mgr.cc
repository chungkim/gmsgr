#include <iostream>
using namespace std;

#include "gmsgr_protocol.h"
#include "gmsgr_client.h"
#include "gmsgr_packet_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_packet_mgr::gmsgr_packet_mgr(p_gmsgr_client _clnt)
:   clnt(_clnt)	    // managed g messenger client
{
    
}

// destructor
gmsgr_packet_mgr::~gmsgr_packet_mgr()
{
    
}


// -- operations --

/*
 * transacts a g messenger packet
 */
bool gmsgr_packet_mgr::transact(p_gmsgr_packet _pkt)
{
    // classify the packet by its type
    switch (_pkt->head.type) 
    {
	case PACKET_DUMMY:		    
	    break;
	    
	case PACKET_JOIN_APP:
	    if (!transact_join_apply(_pkt)) return false;
	    break;
	    
	case PACKET_LEAVE_APP:		
	    if (!transact_leave_apply(_pkt)) return false;
	    break;
	    
	case PACKET_LOGIN_APP:		
	    if (!transact_login_apply(_pkt)) return false;
	    break;
	    
	case PACKET_BUDDY_FIND_APP:
	    if (!transact_buddy_find_apply(_pkt)) return false;
	    break;
	    
	case PACKET_BUDDY_ADD_APP:
	    if (!transact_buddy_add_apply(_pkt)) return false;
	    break;

	case PACKET_BUDDY_ADD_ASW:
	    if (!transact_buddy_add_answer(_pkt)) return false;
	    break;
	    
	case PACKET_BUDDY_DEL_APP:
	    if (!transact_buddy_delete_apply(_pkt)) return false;
	    break;
	    
	case PACKET_CHAT_MSG:
	    if (!transact_chat_message(_pkt)) return false;
	    break;

	case PACKET_FILE_APP:
	    if (!transact_file_apply(_pkt)) return false;
	    break;

	case PACKET_FILE_ASW:
	    if (!transact_file_answer(_pkt)) return false;
	    break;

	case PACKET_FILE_BIN:
	    if (!transact_file_binary(_pkt)) return false;
	    break;

	case PACKET_FILE_ACK:
	    if (!transact_file_acknowledge(_pkt)) return false;
	    break;

	case PACKET_FILE_ESC:
	    if (!transact_file_escape(_pkt)) return false;
	    break;

	default:	
	    cout << "[Warning] Unknown packet is received!" << endl;
	    return false;
    }
        
    return true;
}

    
// -- methods --

/*
 * transacts the packet JOIN_APP.
 */
bool gmsgr_packet_mgr::transact_join_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get id and password hash
    string id(_pkt->body);
    char pwd_hash[GMSGR_SIZEOF_PWD_HASH];
    memcpy(pwd_hash, _pkt->body + (GMSGR_MAX_ID+1), GMSGR_SIZEOF_PWD_HASH);

    // join the new user
    GMSGR_JOIN_RSL rsl = clnt->mgr_user->join(id, pwd_hash);
    if (rsl == JOIN_RSL_FAIL_UNKNWN)
	succeeded = false;
    
    cout << "[Notice] User[" << id << "] join result: (" << rsl << ")" << endl;

    // make a join result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_JOIN_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_JOIN_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_JOIN_RSL));
    
    // send the packet to the client
    if (!clnt->mgr_transmission->send(&pkt))
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return succeeded;
}

/*
 * transacts the packet LEAVE_APP.
 */
bool gmsgr_packet_mgr::transact_leave_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get id
    string id(_pkt->body);

    // join the new user
    GMSGR_LEAVE_RSL rsl = clnt->mgr_user->leave(id);
    if (rsl == LEAVE_RSL_FAIL_UNKNWN)
	succeeded = false;

    cout << "[Notice] User[" << id << "] leave result: (" << rsl << ")" << endl;
    
    // make a leave result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_LEAVE_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_LEAVE_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_LEAVE_RSL));
    
    // send the packet to the client
    if (!clnt->mgr_transmission->send(&pkt))
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }

    return succeeded;
}

/*
 * transacts the packet LOGIN_APP.
 */
bool gmsgr_packet_mgr::transact_login_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get id and password hash
    string id(_pkt->body);
    char pwd_hash[GMSGR_SIZEOF_PWD_HASH];
    memcpy(pwd_hash, _pkt->body + (GMSGR_MAX_ID+1), GMSGR_SIZEOF_PWD_HASH);

    // join the new user
    GMSGR_LOGIN_RSL rsl = clnt->mgr_user->login(id, pwd_hash, clnt);
    if (rsl == LOGIN_RSL_FAIL_UNKNWN)
	succeeded = false;
    
    cout << "[Notice] User[" << id << "] login result: (" << rsl << ")" << endl;

    // make a login result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_LOGIN_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_LOGIN_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_LOGIN_RSL));
    
    // send the packet to the client
    if (!clnt->mgr_transmission->send(&pkt))
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return succeeded;
}

/*
 * transacts the packet BUDDY_FIND_APP.
 */
bool gmsgr_packet_mgr::transact_buddy_find_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get buddy id
    string id(_pkt->body);
    
    // find the buddy
    GMSGR_BUDDY_FIND_RSL rsl = clnt->mgr_user->find_user(clnt->usr->get_id(), id);
    if (rsl == BUDDY_FIND_RSL_FAIL_UNKNWN)
	succeeded = false;
    
    cout << "[Notice] User[" << clnt->usr->get_id() << "] buddy find result: (" << rsl << ")" << endl;

    // make a buddy find result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_FIND_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_BUDDY_FIND_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_BUDDY_FIND_RSL));
    
    // send the packet to the client
    if (!clnt->mgr_transmission->send(&pkt))
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return succeeded;
}

/*
 * transacts the packet BUDDY_ADD_APP.
 */
bool gmsgr_packet_mgr::transact_buddy_add_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get buddy id
    string id(_pkt->body);
    
    // ask the buddy to add the user as a buddy
    GMSGR_BUDDY_ADD_RSL rsl = clnt->mgr_user->ask_add_buddy(clnt->usr, id);
    if (rsl == BUDDY_ADD_RSL_FAIL_UNKNWN)
	succeeded = false;
    
    cout << "[Notice] User[" << clnt->usr->get_id() << "] buddy question result: (" << rsl << ")" << endl;

    // send the buddy add result, if the buddy question is failed
    if (rsl != BUDDY_ADD_RSL_SUCCESS) 
    {
	// make a buddy add result packet
	gmsgr_packet pkt;
	pkt.head.type = PACKET_BUDDY_ADD_RSL;
	pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_BUDDY_ADD_RSL);
	memcpy(pkt.body, &rsl, sizeof(GMSGR_BUDDY_ADD_RSL));
	
	// send the packet to the client
	if (!clnt->mgr_transmission->send(&pkt)) 
	{
	    cerr << "[Error] Fail to send a packet!" << endl;
	    return false;
	}
    }
    
    return succeeded;
}

/*
 * transacts the packet BUDDY_ADD_ASW.
 */
bool gmsgr_packet_mgr::transact_buddy_add_answer(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    
    // get the answer and buddy id
    GMSGR_BUDDY_ADD_ASW asw = *(P_GMSGR_BUDDY_ADD_ASW)_pkt->body;
    string id(_pkt->body + sizeof(GMSGR_BUDDY_ADD_ASW));
    
    cout << "[Notice] User[" << clnt->usr->get_id() << "] buddy add answer: (" << asw << ")" << endl;

    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;
    
    // convert the answer to result
    GMSGR_BUDDY_ADD_RSL rsl = (asw == BUDDY_ADD_ASW_NO) ? BUDDY_ADD_RSL_FAIL_REFUSED : BUDDY_ADD_RSL_SUCCESS;
    
    // add buddy to each other, if it was successful
    if (rsl == BUDDY_ADD_RSL_SUCCESS)
    {
	if (!clnt->mgr_user->create_buddyship(buddy, clnt->usr))
	{
	    rsl == BUDDY_ADD_RSL_FAIL_UNKNWN;
	    succeeded = false;
	}
    }
	
    // make a buddy add result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_ADD_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_BUDDY_ADD_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_BUDDY_ADD_RSL));

    // send the packet to the buddy who asked first
    if (!buddy->get_clnt()->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
	
    return succeeded;
}

/*
 * transacts the packet BUDDY_DEL_APP.
 */
bool gmsgr_packet_mgr::transact_buddy_delete_apply(p_gmsgr_packet _pkt)
{
    bool succeeded = true;
    GMSGR_BUDDY_DEL_RSL rsl = BUDDY_DEL_RSL_SUCCESS;
    
    // get buddy id
    string id(_pkt->body);
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (buddy)
    {
	// delete buddy from each other
	if (!clnt->mgr_user->destroy_buddyship(clnt->usr, buddy)) 
	{
	    rsl == BUDDY_DEL_RSL_FAIL_UNKNWN;
	    succeeded = false;
	}
    }
    else
    {
	rsl == BUDDY_DEL_RSL_FAIL_UNKNWN;
	succeeded = false;	
    }
    
    cout << "[Notice] User[" << clnt->usr->get_id() << "] buddy delete result: (" << rsl << ")" << endl;

    // make a buddy delete result packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_DEL_RSL;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_BUDDY_DEL_RSL);
    memcpy(pkt.body, &rsl, sizeof(GMSGR_BUDDY_DEL_RSL));
    
    // send the packet to the client
    if (!clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return succeeded;
}

/*
 * transacts the packet CHAT_MSG.
 */
bool gmsgr_packet_mgr::transact_chat_message(p_gmsgr_packet _pkt)
{
    // get the buddy id and message
    string id(_pkt->body);
    string msg(_pkt->body + (GMSGR_MAX_ID+1));
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;
    
    // passes the message to the buddy
    if (!buddy->get_chat_manager()->pass_message(clnt->usr->get_id(), msg))
	return false;
	
    return true;
}

/*
 * transacts the packet FILE_APP.
 */
bool gmsgr_packet_mgr::transact_file_apply(p_gmsgr_packet _pkt)
{
    // get the buddy id and message
    string id(_pkt->body);
    int size = *(int*)(_pkt->body + (GMSGR_MAX_ID+1));
    string name(_pkt->body + (GMSGR_MAX_ID+1) + sizeof(size));
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;
    
    // ask the buddy whether he or she wants
    if (!buddy->get_file_manager()->ask_file(clnt->usr->get_id(), name, size))
    {
	// make a file result packet, if it's failed
	gmsgr_packet pkt;
	pkt.head.type = PACKET_FILE_RSL;
	pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + sizeof(GMSGR_FILE_RSL) + (GMSGR_MAX_ID+1);
	*(P_GMSGR_FILE_RSL)pkt.body = FILE_RSL_FAIL_UNKNWN;
	strcpy(pkt.body + sizeof(GMSGR_FILE_RSL), buddy->get_id().c_str());
	
	// send the packet to the client
	if (!clnt->mgr_transmission->send(&pkt)) 
	    cerr << "[Error] Fail to send a packet!" << endl;
	
	return false;
    }
    
    return true;
}

/*
 * transacts the packet FILE_ASW.
 */
bool gmsgr_packet_mgr::transact_file_answer(p_gmsgr_packet _pkt)
{
    // get the answer and buddy id
    GMSGR_FILE_ASW asw = *(P_GMSGR_FILE_ASW)_pkt->body;
    string id(_pkt->body + sizeof(GMSGR_FILE_ASW));
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;

    // answer the question
    if (!buddy->get_file_manager()->answer_file(clnt->usr->get_id(), asw))
	return false;
	
    return true;
}

/*
 * transacts the packet FILE_BIN.
 */
bool gmsgr_packet_mgr::transact_file_binary(p_gmsgr_packet _pkt)
{
    // get the id, whole and current size, binary
    string id(_pkt->body);
    int whole = *(int*)(_pkt->body + (GMSGR_MAX_ID+1));
    int cur = *(int*)(_pkt->body + (GMSGR_MAX_ID+1) + sizeof(whole));
    char* bin = _pkt->body + (GMSGR_MAX_ID+1) + sizeof(whole) + sizeof(cur);
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;

    // pass the binary to the buddy
    if (!buddy->get_file_manager()->pass_binary(clnt->usr->get_id(), whole, cur, bin))
	return false;
	
    return true;
}

/*
 * transacts the packet FILE_ACK.
 */
bool gmsgr_packet_mgr::transact_file_acknowledge(p_gmsgr_packet _pkt)
{
    // get the id, whole and current size, binary
    string id(_pkt->body);
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;

    // pass the binary to the buddy
    if (!buddy->get_file_manager()->pass_acknowledge(clnt->usr->get_id()))
	return false;
	
    return true;
}

/*
 * transacts the packet FILE_ESC.
 */
bool gmsgr_packet_mgr::transact_file_escape(p_gmsgr_packet _pkt)
{
    // get the id, whole and current size, binary
    string id(_pkt->body);
    
    // get the buddy
    p_gmsgr_user buddy = clnt->mgr_user->get_user_log(id);
    if (!buddy)
	return false;

    // pass the binary to the buddy
    if (!buddy->get_file_manager()->pass_escape(clnt->usr->get_id()))
	return false;
	
    return true;
}
