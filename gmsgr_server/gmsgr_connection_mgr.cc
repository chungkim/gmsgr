#include <iostream>
using namespace std;

#include "gmsgr_server.h"
#include "gmsgr_client.h"
#include "gmsgr_connection_mgr.h"
#include "gmsgr_user.h"

// -- construct, destruct --

// constructor
gmsgr_connection_mgr::gmsgr_connection_mgr(unsigned short _port, unsigned int _clnt_max, p_gmsgr_server _serv)
:   sock(_port),		// server socket
    clnt_idx_box(_clnt_max),	// client idx box
    serv(_serv)			// g messenger server
{
    if (!sock.is_valid()) 
    {
	cerr << "[Error] G Messenger Server is already opened!" << endl;
	exit(1);
    }
}

// destructor
gmsgr_connection_mgr::~gmsgr_connection_mgr() 
{
    
}


// -- operations --

/*
 * accepts a g messenger clients infinitely.
 */
void gmsgr_connection_mgr::run()
{
    int fd;

    string ip;
    unsigned short port;
    if (!sock.get_addr(ip, port))
    {
	cerr << "[Error] Fail to get server address!" << endl;
	exit(1);
    }
    
    // notice connection manager is on 
    cout << "[Notice] Connection manager is running: (" << ip << "/" << port << ")" << endl;
    
    // loop for accepting clients
    while (true)
    {
	// accept a client socket
	fd = sock.accept();
	if (fd == -1)
	{
	    cout << "[Warning] Fail to accept a client!" << endl;
	    continue;
	}
	
	// add a g messenger client
	if (!add_client(fd))
	{
	    // close the client socket
	    close(fd);
	    
	    cout << "[Warning] Fail to add a client!" << endl;
	    continue;
	}
    }
}

/*
 * deletes a g messenger client with the client index
 */
void gmsgr_connection_mgr::del_client(unsigned int _idx)
{
    // is there any client?
    if (clnt_idx_box.is_full())
    {
	cerr << "[Error] There is no client!" << endl;
	exit(1);
    }
    
    if (serv->clnts[_idx].get_index() == 0xFFFFFFFF)
	return;
	
    // logout the user if it's logined
    p_gmsgr_user usr = serv->clnts[_idx].get_user();
    if (usr)
	serv->mgr_user->logout(usr->get_id(), &serv->clnts[_idx]);
    
    // close the client
    serv->clnts[_idx].close();
    
    // box in the client index
    if (!clnt_idx_box.in(_idx))
    {
	cerr << "[Error] Fail to box in a client index!" << endl;
	exit(1);
    }
    
    // notice current client count
    unsigned int cur, max;
    get_client_count(cur, max);
    cout << "[Notice] Current client count: (" << cur << "/" << max << ")" << endl;
}

/*
 * returns a client by the index
 */
p_gmsgr_client gmsgr_connection_mgr::get_client(unsigned int _idx)
{
    return (&serv->clnts[_idx]);
}
    
/*
 * notices current client count
 */
void gmsgr_connection_mgr::get_client_count(unsigned int& cur, unsigned int& max)
{
    cur = clnt_idx_box.max() - clnt_idx_box.count();
    max = clnt_idx_box.max();
}


// -- methods --

/*
 * adds a g messenger client with a new socket file descriptor
 */
bool gmsgr_connection_mgr::add_client(int _fd)
{
    // are clients full?
    if (clnt_idx_box.is_empty())
    {
	cout << "[Warning] Clients are full!" << endl;
	return false;
    }
    
    // box out an client index
    unsigned int idx = clnt_idx_box.out();
    
    // open a new client
    if (!serv->clnts[idx].open(idx, _fd, serv->mgr_user,this))
    {
	// box in the client index
	clnt_idx_box.in(idx);
	
	cerr << "[Error] Fail to open a client!" << endl;
	return false;
    }
    
    // notice current client count
    unsigned int cur, max;
    get_client_count(cur, max);
    cout << "[Notice] Current client count: (" << cur << "/" << max << ")" << endl;
    
    return true;
}

