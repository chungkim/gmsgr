#include <iostream>
using namespace std;

#include "gmsgr_server.h"

// -- fields --

gmsgr_server*	gmsgr_server::instance;


// -- construct, destruct --

// constructor
gmsgr_server::gmsgr_server(unsigned short _port, unsigned int _clnt_max)
:   mgr_user(NULL),  	    // user data mananger
    mgr_connection(NULL)    // connection manager
{
    // allocate g messenger client instances
    clnts = new gmsgr_client[_clnt_max];
    if (!clnts)
    {
	cerr << "[Error] Not enough memory for client instances!" << endl;
	exit(1);
    }

    // allocate user manager
    mgr_user = new gmsgr_user_mgr(this);
    if (!mgr_user)
    {
	cerr << "[Error] Not enough memory for user manager!" << endl;
	exit(1);
    }
    
    // allocate connection manager
    mgr_connection = new gmsgr_connection_mgr(_port, _clnt_max, this);
    if (!mgr_connection)
    {
	cerr << "[Error] Not enough memory for connection manager!" << endl;
	exit(1);
    }        
}

// destructor
gmsgr_server::~gmsgr_server()
{
    // free connection manager
    if (mgr_connection)
	delete mgr_connection;
    
    // free user manager
    if (mgr_user)
	delete mgr_user;
    
    // free g messenger client instances
    if (clnts)
	delete []clnts;        
}


// -- operations --

/*
 * creates the g messenger instance. (singleton pattern)
 */
gmsgr_server* gmsgr_server::create_instance(unsigned short _port, unsigned int _clnt_max)
{
    if (instance) return NULL;
    
    // allocate the only instance
    instance = new gmsgr_server(_port, _clnt_max);
    if (!instance) return NULL;
    
    return instance;
}

/*
 * runs g messenger server.
 */
void gmsgr_server::run()
{
    cout << endl << "<< G Messenger Server v1.0 >>" << endl << endl;
    
    // run user data manager
    mgr_user->run();
    
    // run connection manager (never returns until the end)
    mgr_connection->run();
}
