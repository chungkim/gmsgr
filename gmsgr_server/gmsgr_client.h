// 
// File:   gmsgr_client.h
// Author: William Kim
//
// Created on May 5, 2007, 1:09 AM
//

#ifndef _gmsgr_client_H
#define	_gmsgr_client_H

#include "client_socket.h"
#include "gmsgr_user_mgr.h"
#include "gmsgr_user.h"
#include "gmsgr_connection_mgr.h"
#include "gmsgr_transmission_mgr.h"
#include "gmsgr_packet_mgr.h"
#include "gmsgr_chat_mgr.h"
#include "gmsgr_file_mgr.h"

/*
 * g messenger client
 */
class gmsgr_client
{
private:
    // -- public fields --
   
    unsigned int		idx;		    // client index
    client_socket		sock;		    // client socket
   
    p_gmsgr_user_mgr		mgr_user;	    // user manager (passed from gmsgr_server)
    p_gmsgr_connection_mgr	mgr_connection;	    // connection manager (passed from gmsgr_server)
    p_gmsgr_transmission_mgr    mgr_transmission;   // transmission manager
    p_gmsgr_packet_mgr	        mgr_packet;	    // packet manager

    p_gmsgr_user		usr;		    // g messenger user
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_client();

    // destructor
    ~gmsgr_client();
    
    
    // -- properties
    
    /*
     * returns the client index.
     */
    unsigned int get_index();
    
    /*
     * sets g mesenger user.
     */
    void set_user(p_gmsgr_user _usr);
    
    /*
     * returns g messenger user.
     */
    p_gmsgr_user get_user();
    
    
    // -- operations --
    
    /*
     * opens g messenger client.
     */
    bool open(unsigned int _idx, int _fd, p_gmsgr_user_mgr _mgr_user, p_gmsgr_connection_mgr _mgr_connection);

    /*
     * closes g messenger client.
     */
    void close();
    
    
private:    
    // -- methods --        
    
    
    // -- aggregation --
    
    friend class gmsgr_transmission_mgr;    // transmission manager
    friend class gmsgr_packet_mgr;	    // packet manager
    friend class gmsgr_user_mgr;	    // user manager
    friend class gmsgr_chat_mgr;	    // chat manager
    friend class gmsgr_file_mgr;	    // file manager
    friend class gmsgr_user;		    // user
};

typedef gmsgr_client* p_gmsgr_client;

#endif	/* _gmsgr_client_H */

