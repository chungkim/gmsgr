#include <iostream>
using namespace std;

#include "gmsgr_client.h"
#include "gmsgr_user.h"

// -- construct, destruct

// constructor
gmsgr_user::gmsgr_user(string _id, p_gmsgr_client _clnt)
:   id(_id),	    // id
    cnt_buddies(0), // count of buddies
    clnt(_clnt),    // g messenger client
    mgr_chat(NULL), // chat manager
    mgr_file(NULL)  // file manager
{
    // set no buddy first
    memset(buddies, 0, sizeof(gmsgr_user*) * GMSGR_MAX_BUDDY);
    
    // allocate chat manager
    mgr_chat = new gmsgr_chat_mgr(this);
    if (!mgr_chat)
    {
	cerr << "[Error] Not enough memory for chat manager!" << endl;
	exit(1);
    }    
    
    // allocate file manager
    mgr_file = new gmsgr_file_mgr(this);
    if (!mgr_file)
    {
	cerr << "[Error] Not enough memory for file manager!" << endl;
	exit(1);
    }    
}

// destructor
gmsgr_user::~gmsgr_user()
{
    // free file manager
    if (mgr_file)
	delete mgr_file;
    
    // free chat manager
    if (mgr_chat)
	delete mgr_chat;
}


// -- properties --

/*
 * returns client.
 */
p_gmsgr_client gmsgr_user::get_clnt()
{
    return clnt;
}

/*
 * returns user id.
 */
string gmsgr_user::get_id()
{
    return id;
}

/*
 * returns current count of buddies logined.
 */
unsigned int gmsgr_user::get_buddy_count()
{
    return cnt_buddies;
}

/*
 * returns chat manager.
 */
p_gmsgr_chat_mgr gmsgr_user::get_chat_manager()
{
    return mgr_chat;
}

/*
 * returns file manager.
 */
p_gmsgr_file_mgr gmsgr_user::get_file_manager()
{
    return mgr_file;
}


// -- operations --

/*
 * adds a buddy.
 * returns false, if buddies are full.
 */
bool gmsgr_user::add_buddy(gmsgr_user* _buddy)
{
    // set a new buddy and increase the count
    buddies[cnt_buddies++] = _buddy;
    
    // notice the user that the buddy logined
    if (!notice_login(clnt, _buddy->get_id()))
	return false;
    
    return true;
}

/*
 * deletes a buddy.
 */
bool gmsgr_user::del_buddy(gmsgr_user* _buddy)
{
    // find the same buddy
    unsigned int i;
    for (i = 0; i < cnt_buddies; i++)
    {
	if (buddies[i] == _buddy)
	    break;
    }    
    if (i >= cnt_buddies)
	return false;
    
    // pull the other buddies next
    for (unsigned int j = i+1; j < cnt_buddies; j++)
	buddies[j-1] = buddies[j];
    
    // decrease the count
    cnt_buddies--;
    
    // notice the user that the buddy logouted
    if (!notice_logout(clnt, _buddy->get_id()))
	return false;
    
    return true;
}

/*
 * clears all buddies.
 */
void gmsgr_user::clear_buddies()
{
    // delete the user in the buddies
    for (unsigned int i = 0; i < cnt_buddies; i++) 
	buddies[i]->del_buddy(this);
    
    // set no buddy
    memset(buddies, 0, sizeof(gmsgr_user*) * GMSGR_MAX_BUDDY);
    
    // set the count zero
    cnt_buddies = 0;
}

    
// -- methods --

/*
 * sends the packet BUDDY_LOGIN.
 */
bool gmsgr_user::notice_login(p_gmsgr_client _clnt, string _id)
{
    // make a buddy login packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_LOGIN;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
    
    // send the packet to the client
    if (!_clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}

/*
 * sends the packet BUDDY_LOGOUT.
 */
bool gmsgr_user::notice_logout(p_gmsgr_client _clnt, string _id)
{
    // make a buddy logout packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_BUDDY_LOGOUT;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1);
    strncpy(pkt.body, _id.c_str(), GMSGR_MAX_ID);
    
    // send the packet to the client
    if (!_clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}
