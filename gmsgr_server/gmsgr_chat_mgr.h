// 
// File:   gmsgr_chat_mgr.h
// Author: William Kim
//
// Created on May 23, 2007, 8:40 PM
//

#ifndef _gmsgr_chat_mgr_H
#define	_gmsgr_chat_mgr_H

#include <string>

typedef class gmsgr_user* p_gmsgr_user;

/*
 * g messenger chat manager
 */
class gmsgr_chat_mgr
{
private:
    // -- fields --
    
    const p_gmsgr_user	usr;	// g meesenger user (passed from gmsgr_user)

    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_chat_mgr(p_gmsgr_user _usr);

    // destructor
    ~gmsgr_chat_mgr();
    
    
    // -- operations --
    
    /*
     * passes the message to the user.
     */
    bool pass_message(std::string _buddy_id, std::string _msg);
    
    
private:    
    // -- methods --    
    
};

typedef gmsgr_chat_mgr* p_gmsgr_chat_mgr;

#endif	/* _gmsgr_chat_mgr_H */

