#include <iostream>
using namespace std;

#include "gmsgr_connection_mgr.h"
#include "gmsgr_transmission_mgr.h"
#include "gmsgr_client.h"

// -- construct, destruct --

// constructor
gmsgr_transmission_mgr::gmsgr_transmission_mgr(p_gmsgr_client _clnt) 
:   clnt(_clnt),    // managed g messenger client
    thrd_recv(0),   // receiving thread (posix)
    thrd_on(false)  // whether receining thread is on or off
{
    
}

// destructor
gmsgr_transmission_mgr::~gmsgr_transmission_mgr() 
{
    // stop transmission manager
    stop();
}


// -- operations --

/*
 * runs transmission manager to receive packets from the client.
 */
bool gmsgr_transmission_mgr::run() 
{
    // set thread on
    thrd_on = true;
    
    // create the receiving thread
    if (pthread_create(&thrd_recv, NULL, thread_recv, this))
    {
	cerr << "[Error] Fail to create receiving thread!" << endl;
	return false;
    }
    
    return true;
}

/*
 * stops transmission manager.
 */
void gmsgr_transmission_mgr::stop()
{
    // stop receiving thread
    if (thrd_recv)
    {
	// set thread off
	thrd_on = false;
	
	// wait for finishing the thread
	pthread_join(thrd_recv, NULL);
	
	// set thread invalid
	thrd_recv = 0;
    }
}

/*
 * receives a packet from the client.
 */
bool gmsgr_transmission_mgr::recv(gmsgr_packet& _pkt)
{
    // receive the head of a packet
    if (clnt->sock.recv(&_pkt.head, GMSGR_SIZEOF_PACKET_HEAD) != GMSGR_SIZEOF_PACKET_HEAD) 
	return false;
    
    // receive the body of the packet
    if (_pkt.head.size - GMSGR_SIZEOF_PACKET_HEAD > 0)
    {
	if (clnt->sock.recv(&_pkt.body, _pkt.head.size - GMSGR_SIZEOF_PACKET_HEAD) != _pkt.head.size - GMSGR_SIZEOF_PACKET_HEAD) 
	    return false;
    }
    
#ifdef _DEBUG	
    // notice a packet is received
    cout << "[Notice] A packet is received[" << clnt->idx << "]: type(" << _pkt.head.type << ")" << endl;
#endif // _DEBUG

    return true;
}

/*
 * sends a packet to the client.
 */
bool gmsgr_transmission_mgr::send(p_gmsgr_packet _pkt)
{
    // send the packet
    if (clnt->sock.send(_pkt, _pkt->head.size) != _pkt->head.size)
	return false;
    
#ifdef _DEBUG	
    // notice a packet is sent
    cout << "[Notice] A packet is sent[" << clnt->idx << "]: type(" << _pkt->head.type << ")" << endl;
#endif // _DEBUG

    return true;
}

    
// -- methods --

/*
 * waits and receives packets from the client.
 */
#define this p
void* gmsgr_transmission_mgr::thread_recv(void* _arg)
{
    p_gmsgr_transmission_mgr p = (p_gmsgr_transmission_mgr)_arg;

    gmsgr_packet pkt;
    
    // loop for receiving packets
    while (this->thrd_on)
    {
	// receive a packet from the client
	if (!this->recv(pkt))	
	{
	    // delete the client disconnected
	    if (this->clnt->sock.is_valid())
		this->clnt->mgr_connection->del_client(this->clnt->idx);
	    
	    continue;
	}

	// pass the packet to the packet manager
	if (!this->clnt->mgr_packet->transact(&pkt))
	    cerr << "[Error] Fail to transact a packet[" << this->clnt->idx << "]: type(" << pkt.head.type << ")" << endl;
    }
    
    return NULL;
}
#undef this
