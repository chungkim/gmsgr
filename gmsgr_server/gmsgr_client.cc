#include <iostream>
using namespace std;

#include "gmsgr_client.h"

// -- construct, destruct --

// constructor
gmsgr_client::gmsgr_client()
:   idx(0xFFFFFFFF),	    // client index
    mgr_user(NULL),	    // user manager
    mgr_connection(NULL),   // connection manager
    mgr_transmission(NULL), // transmission manager
    mgr_packet(NULL),	    // packet manager
    usr(NULL)		    // g messenger user
{
    // allocate transmission manager
    mgr_transmission = new gmsgr_transmission_mgr(this);
    if (!mgr_transmission)
    {
	cerr << "[Error] Not enough memory for transmission manager!" << endl;
	exit(1);
    }
    
    // allocate packet manager
    mgr_packet = new gmsgr_packet_mgr(this);
    if (!mgr_packet)
    {
	cerr << "[Error] Not enough memory for packet manager!" << endl;
	exit(1);
    }
}

// destructor
gmsgr_client::~gmsgr_client()
{   
    // free packet manager
    if (mgr_packet)
	delete mgr_packet;
    
    // free transmission manager
    if (mgr_transmission)
	delete mgr_transmission;
}


// -- properties

/*
 * returns the client index.
 */
unsigned int gmsgr_client::get_index()
{
    return idx;
}

/*
 * sets g mesenger user.
 */
void gmsgr_client::set_user(p_gmsgr_user _usr)
{
    // set the user
    usr = _usr;
}

/*
 * returns g messenger user.
 */
p_gmsgr_user gmsgr_client::get_user()
{
    return usr;
}
    
    
// -- operations --

/*
 * opens g messenger client., p_gmsgr_user _mgr_user, 
 */
bool gmsgr_client::open(unsigned int _idx, int _fd, p_gmsgr_user_mgr _mgr_user, p_gmsgr_connection_mgr _mgr_connection)
{
    // set client index
    idx = _idx;

    // set file descriptor
    if (!sock.set_fd(_fd))
	return false;
    
    // set user manager
    mgr_user = _mgr_user;
    
    // set connection manager
    mgr_connection = _mgr_connection;
    
    // get client address
    string ip;
    unsigned short port;
    if (!sock.get_addr(ip, port))
    {
	cerr << "[Error] Fail to get client address!" << endl;
	return false;
    }     

    // run transmission manager
    if (!mgr_transmission->run())
    {
	cerr << "[Error] Fail to run transmission manager!" << endl;
	return false;	
    }
    
    // notice client is opened
    cout << "[Notice] Client[" << _idx << "] connected: (" << ip << "/" << port << ")" << endl;
    
    return true;
}

/*
 * closes g messenger client.
 */
void gmsgr_client::close()
{
    // get client address
    string ip;
    unsigned short port = 0;
    if (!sock.get_addr(ip, port))
	cout << "[Warning] Fail to get client address!" << endl;
    
    // close client socket
    sock.close();
    
    // stop transmission manager
    mgr_transmission->stop();
    
    // notice client is opened
    cout << "[Notice] Client[" << idx << "] disconnected";    
    if (!ip.empty() && port)
	cout << ": (" << ip << "/" << port << ")" << endl;
    else
	cout << "." << endl;

    // set g messenger user invalid
    usr = NULL;
    
    // set connection manager invalid
    mgr_connection = NULL;
    
    // set user manager invalid
    mgr_user = NULL;
    
    // set client index invalid
    idx = 0xFFFFFFFF;
}


// -- methods --

