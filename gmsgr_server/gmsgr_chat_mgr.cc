#include <iostream>
using namespace std;

#include "gmsgr_packet.h"
#include "gmsgr_client.h"
#include "gmsgr_user.h"
#include "gmsgr_chat_mgr.h"

// -- construct, destruct --

// constructor
gmsgr_chat_mgr::gmsgr_chat_mgr(p_gmsgr_user _usr)
:   usr(_usr)	// g meesenger user
{
    
}

// destructor
gmsgr_chat_mgr::~gmsgr_chat_mgr()
{
    
}


// -- operations --

/*
 * passes the message to the user.
 */
bool gmsgr_chat_mgr::pass_message(string _buddy_id, string _msg)
{
#ifdef _DEBUG    
    cout << "[Notice] User[" << _buddy_id << "] sent a chat message to buddy[" << usr->id << "]: " << _msg << endl;
#endif // _DEBUG

    // make a chat message packet
    gmsgr_packet pkt;
    pkt.head.type = PACKET_CHAT_MSG;
    pkt.head.size = GMSGR_SIZEOF_PACKET_HEAD + (GMSGR_MAX_ID+1) + (_msg.length()+1);
    strncpy(pkt.body, _buddy_id.c_str(), GMSGR_MAX_ID);
    strncpy(pkt.body + (GMSGR_MAX_ID+1), _msg.c_str(), (GMSGR_MAX_PACKET_BODY - (GMSGR_MAX_ID+1)));
    
    // send the packet to the client
    if (!usr->clnt->mgr_transmission->send(&pkt)) 
    {
	cerr << "[Error] Fail to send a packet!" << endl;
	return false;
    }
    
    return true;
}


// -- methods --

