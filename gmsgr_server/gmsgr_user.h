// 
// File:   gmsgr_user.h
// Author: William Kim
//
// Created on May 9, 2007, 8:54 PM
//

#ifndef _gmsgr_user_H
#define	_gmsgr_user_H

#include <string>

#include "gmsgr_protocol.h"
#include "gmsgr_chat_mgr.h"
#include "gmsgr_file_mgr.h"

typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger user
 */
class gmsgr_user
{
private:
    // -- fields --
    
    const p_gmsgr_client    clnt;			// g messenger client (passed from gmsgr_user_mgr)

    std::string		    id;				// id
    gmsgr_user*		    buddies[GMSGR_MAX_BUDDY];	// buddies
    unsigned int	    cnt_buddies;		// count of buddies
    
    p_gmsgr_chat_mgr	    mgr_chat;			// chat manager
    p_gmsgr_file_mgr	    mgr_file;			// file manager

    
public:
    // -- construct, destruct 
    
    // constructor
    gmsgr_user(std::string _id, p_gmsgr_client _clnt);

    // destructor
    ~gmsgr_user();
    
    
    // -- properties --
    
    /*
     * returns g messenger client.
     */
    p_gmsgr_client get_clnt();
    
    /*
     * returns user id.
     */
    std::string get_id();
    
    /*
     * returns current count of buddies logined.
     */
    unsigned int get_buddy_count();
    
    /*
     * returns chat manager.
     */
    p_gmsgr_chat_mgr get_chat_manager();
    
    /*
     * returns file manager.
     */
    p_gmsgr_file_mgr get_file_manager();
    
    
    // -- operations --
    
    /*
     * adds a buddy.
     * returns false, if buddies are full.
     */
    bool add_buddy(gmsgr_user* _buddy);

    /*
     * deletes a buddy.
     */
    bool del_buddy(gmsgr_user* _buddy);

    /*
     * clears all buddies.
     */
    void clear_buddies();
    
    
private:    
    // -- methods --     
    
    /*
     * sends the packet BUDDY_LOGIN.
     */
    bool notice_login(p_gmsgr_client _clnt, std::string _id);

    /*
     * sends the packet BUDDY_LOGOUT.
     */
    bool notice_logout(p_gmsgr_client _clnt, std::string _id);
    
    
    // -- aggregation -- 

    friend class gmsgr_chat_mgr;    // chat manager
    friend class gmsgr_file_mgr;    // file manager
};

typedef gmsgr_user* p_gmsgr_user;

#endif	/* _gmsgr_user_H */

