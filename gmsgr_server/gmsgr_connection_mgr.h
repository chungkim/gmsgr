// 
// File:   gmsgr_connection_mgr.h
// Author: William Kim
//
// Created on May 5, 2007, 12:10 AM
//

#ifndef _gmsgr_connection_mgr_H
#define	_gmsgr_connection_mgr_H

#include "server_socket.h"
#include "index_box.h"

typedef class gmsgr_server* p_gmsgr_server;
typedef class gmsgr_client* p_gmsgr_client;

/*
 * g messenger connection manager
 */
class gmsgr_connection_mgr
{
private:
    // -- fields --
   
    const p_gmsgr_server    serv;	    // g meesenger server (passed from gmsgr_server)
    
    server_socket	    sock;	    // server socket
   
    index_box		    clnt_idx_box;   // client index box
    
    
public:
    // -- construct, destruct --

    // constructor
    gmsgr_connection_mgr(unsigned short _port, unsigned int _clnt_max, p_gmsgr_server _serv);

    // destructor
    ~gmsgr_connection_mgr();
    

    // -- operations --
    
    /*
     * runs connection manager to accept g messenger clients infinitely.
     */
    void run();
    
    /*
     * deletes a g messenger client with the client index
     */
    void del_client(unsigned int _idx);
    
    /*
     * returns a client by the index
     */
    p_gmsgr_client get_client(unsigned int _idx);
    
    /*
     * notices current client count
     */
    void get_client_count(unsigned int& cur, unsigned int& max);

    
private:    
    // -- methods --    
    
    /*
     * adds a g messenger client with a new socket file descriptor
     */
    bool add_client(int _fd);    
};

typedef gmsgr_connection_mgr* p_gmsgr_connection_mgr;

#endif	/* _gmsgr_connection_mgr_H */

